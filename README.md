# VSI-Branch of simOpenPASS

This branch, based on the `hlrs` branch of the original **simopenpass**-repository ([link](https://gitlab.eclipse.org/eclipse/simopenpass/simopenpass/-/tree/hlrs)), is designated as the "working" branch for the team at the Vehicle Safety Institute at the Graz University of Technology in their ongoing effort to contribute to the OpenPass project by implementing Pedestrian Models (PM) for test scenarios based on the OpenScenario standard.

For information on Pedestrian Models, see:
* [Citation](link-to-some-paper)
