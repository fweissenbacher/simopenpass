/****************************************************************************** 
* Copyright (c) 2020 HLRS, University of Stuttgart.
* 
* This program and the accompanying materials are made 
* available under the terms of the Eclipse Public License 2.0 
* which is available at https://www.eclipse.org/legal/epl-2.0/ 
* 
* SPDX-License-Identifier: EPL-2.0 
******************************************************************************/ 

#include "AboutPlugin.h"

#include "openPASS-Window/WindowInterface.h"

#include "Views/AboutView.h"

AboutPlugin::AboutPlugin(QObject *const parent)
    : QObject(parent)
    , aboutView(nullptr)
{
}

bool AboutPlugin::initialize()
{
    WindowInterface *const window = WindowInterface::instance();
    if (window)
    {
        aboutView = new AboutView(window);

        return true;
    }
    return false;
}

bool AboutPlugin::deinitialize()
{
    delete aboutView;
    return true;
}
