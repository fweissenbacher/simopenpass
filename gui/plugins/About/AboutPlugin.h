/****************************************************************************** 
* Copyright (c) 2020 HLRS, University of Stuttgart.
* 
* This program and the accompanying materials are made 
* available under the terms of the Eclipse Public License 2.0 
* which is available at https://www.eclipse.org/legal/epl-2.0/ 
* 
* SPDX-License-Identifier: EPL-2.0 
******************************************************************************/ 

#ifndef ABOUTPLUGIN_H
#define ABOUTPLUGIN_H

#include "openPASS/PluginInterface.h"
#include <QObject>

class AboutModel;
class AboutPresenter;
class AboutView;

class AboutPlugin : public QObject, public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "openPASS.About")
    Q_INTERFACES(PluginInterface)

public:
    explicit AboutPlugin(QObject * const parent = nullptr);
    virtual ~AboutPlugin() = default;

public:
    virtual bool initialize() override;
    virtual bool deinitialize() override;

protected:
    AboutView * aboutView;
};

#endif // ABOUTPLUGIN_H
