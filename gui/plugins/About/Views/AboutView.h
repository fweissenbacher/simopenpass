/****************************************************************************** 
* Copyright (c) 2020 HLRS, University of Stuttgart.
* 
* This program and the accompanying materials are made 
* available under the terms of the Eclipse Public License 2.0 
* which is available at https://www.eclipse.org/legal/epl-2.0/ 
* 
* SPDX-License-Identifier: EPL-2.0 
******************************************************************************/ 

#ifndef ABOUTVIEW_H
#define ABOUTVIEW_H

#include "openPASS-Window/WindowInterface.h"

#include <QString>
#include <QWidget>

namespace Ui {
class AboutView;
}

class AboutPresenter;

class AboutView : public QWidget
{
    Q_OBJECT

public:
    explicit AboutView(WindowInterface * const window,
                         QWidget *parent = 0);
    virtual ~AboutView();

private:
    static WindowInterface::ID const ViewID;

private:
    WindowInterface * const window;

private:
    Ui::AboutView *ui;


};

#endif // ABOUTVIEW_H
