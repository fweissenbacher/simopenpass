/****************************************************************************** 
* Copyright (c) 2020 HLRS, University of Stuttgart.
* 
* This program and the accompanying materials are made 
* available under the terms of the Eclipse Public License 2.0 
* which is available at https://www.eclipse.org/legal/epl-2.0/ 
* 
* SPDX-License-Identifier: EPL-2.0 
******************************************************************************/ 

#include "Views/AboutView.h"
#include "ui_AboutView.h"


#include <QCoreApplication>
#include <QDir>
#include <QFileDialog>
#include <QFile>
#include <QKeyEvent>
#include <QStandardPaths>
#include <QString>

const WindowInterface::ID AboutView::ViewID = QStringLiteral("openPASS.About");

AboutView::AboutView(WindowInterface *const window,
                         QWidget *parent)
    : QWidget(parent)
    , window(window)
    , ui(new Ui::AboutView)
{
    // Create UI
    ui->setupUi(this);


    QFile file(":/README-3rd-party.txt");
    if (file.open(QIODevice::ReadOnly))
    {
        QString data(file.readAll());
        ui->textEdit->setPlainText(data);
    }
    QFile fileOP(":/README-OpenPASS.txt");
    if (fileOP.open(QIODevice::ReadOnly))
    {
        QString data(fileOP.readAll());
        ui->textEditOpenPASS->setPlainText(data);
    }

    


    // Register view
    window->add(ViewID, WindowInterface::createButton(tr("About"), 0, 07000), this,
    {});
}

AboutView::~AboutView()
{
    // Deregister view
    window->remove(ViewID);

    // Destroy UI
    delete ui;
}


