/*******************************************************************************
* Copyright (c) 2019, 2020 in-tech GmbH
* Copyright (c) 2021 HLRS, University of Stuttgart.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

#pragma once

#include "common/opExport.h"
#include "include/profilesInterface.h"

class CORESLAVEEXPORT Profiles : public ProfilesInterface
{
public:
    virtual ~Profiles() override = default;

    virtual std::unordered_map<std::string, std::shared_ptr<AgentProfile>>& GetAgentProfiles() override;

    virtual std::unordered_map<std::string, std::shared_ptr<VehicleProfile>>& GetVehicleProfiles() override;

    virtual ProfileGroups& GetProfileGroups() override;

    virtual StringProbabilities& GetDriverProbabilities(const std::string &agentProfileName) override;

    virtual StringProbabilities& GetVehicleProfileProbabilities(const std::string &agentProfileName) override;

    virtual openpass::parameter::ParameterSetLevel1 GetProfile(const std::string &type, const std::string &name) override;

private:
    std::unordered_map<std::string, std::shared_ptr<AgentProfile>> agentProfiles {};
    std::unordered_map<std::string, std::shared_ptr<VehicleProfile>> vehicleProfiles {};
    ProfileGroups profileGroups;
};
