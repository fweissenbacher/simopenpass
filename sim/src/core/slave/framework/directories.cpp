/*******************************************************************************
* Copyright (c) 2019, 2020 in-tech GmbH
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

#include "directories.h"

#include <algorithm>
#include <string>

#include <QDir>
#include <QFileInfo>
#include <QString>

namespace openpass::core {

// TODO: Change from applicationDir to workingDir (=current workspace)
/*
Directories::Directories(const std::string& applicationDir,
                         const std::string& libraryDir,
                         const std::string& configurationDir,
                         const std::string& outputDir):
    baseDir{Directories::Resolve(applicationDir, ".")},
    configurationDir{Directories::Resolve(applicationDir, configurationDir)},
    libraryDir{Directories::Resolve(applicationDir, libraryDir)},
    outputDir{Directories::Resolve(applicationDir, outputDir)}
{}
*/
Directories::Directories(const std::string& applicationDir,
                         const std::string& libraryDir,
                         const std::string& configurationDir,
                         const std::string& outputDir):
    workingDir{QDir::currentPath().toStdString()},
    baseDir{Directories::Resolve(applicationDir, ".")},
    configurationDir{Directories::Resolve(this->workingDir, configurationDir)},
    libraryDir{Directories::Resolve(applicationDir, libraryDir)},
    outputDir{Directories::Resolve(this->workingDir, outputDir)}
{}

Directories::~Directories()
{
    // the descructor has not been exported to a DLL, thus we explicitly define one here even though it is empty
}

const std::string Directories::Resolve(const std::string& referencePath, const std::string& path)
{
    auto qpath = QString::fromStdString(path);

    return QDir::isRelativePath(qpath) ?
           QDir(QString::fromStdString(referencePath) + QDir::separator() + qpath).absolutePath().toStdString() :
           QDir(qpath).absolutePath().toStdString();
}

const std::string Directories::Concat(const std::string& path, const std::string& file)
{
    if (file.length() > 2 && (file[1] == ':' || file[0] == '\\' || file[0] == '/')) // if file is an absolute path, return it directly otherwise prepend it with the specified path
        return QDir(QString::fromStdString(file)).absolutePath().toStdString();
    return QDir(QString::fromStdString(path) +
                QDir::separator() +
                QString::fromStdString(file)).absolutePath().toStdString();
}

const std::vector<std::string> Directories::Concat(const std::string& path, const std::vector<std::string>& filenames)
{
    std::vector<std::string> result {};

    std::transform(filenames.cbegin(),
                   filenames.cend(),
                   std::back_inserter(result),
                   [&path] (const auto &extension) -> std::string
    {
        return Directories::Concat(path, extension);
    });

    return result;
}

const std::string Directories::StripFile(const std::string& path)
{
    QFileInfo fileInfo(QString::fromStdString(path));
    return fileInfo.path().toStdString();
}

bool Directories::IsRelative(const std::string& path)
{
    QFileInfo fileInfo(QString::fromStdString(path));
    return fileInfo.isRelative();
}

} // namespace openpass::core
