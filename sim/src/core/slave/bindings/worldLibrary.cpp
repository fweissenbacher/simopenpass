/*******************************************************************************
* Copyright (c) 2017, 2018, 2019 in-tech GmbH
*               2016, 2017, 2018 ITK Engineering GmbH
*               2020 HLRS, University of Stuttgart.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

#include "common/log.h"
#include "bindings/worldLibrary.h"

namespace SimulationSlave
{

bool WorldLibrary::Init()
{
    std::string suffix = DEBUG_POSTFIX;
    library = new (std::nothrow) QLibrary(QString::fromStdString(worldLibraryPath+suffix));
    if(!library)
    {
        return false;
    }

    LOG_INTERN(LogLevel::DebugCore) << "QLibrary::load() library '" << worldLibraryPath+suffix << "'" ;
    bool loadingSuccessful = library->load();

    // For some reason, loading a QLibrary causes LogOutputPolicy to lose track of the entries in
    // logStreamMap that store the ofstream that belongs to the current threadId. Running the line below
    // simply reopens the log file in 'append' mode.
    //LogOutputPolicy::ReopenLogFile();

    if(!loadingSuccessful)
    {
        LOG_INTERN(LogLevel::Error) << library->errorString().toStdString();
        // Line below is only here for debugging reasons
        std::string errorMsg = library->errorString().toStdString();
        delete library;
        library = nullptr;
        return false;
    }

    getVersionFunc = (WorldInterface_GetVersion)library->resolve(DllGetVersionId.c_str());
    if(!getVersionFunc)
    {
        LOG_INTERN(LogLevel::Error) << "could not retrieve version information from DLL";
        return false;
    }

    createInstanceFunc = (WorldInterface_CreateInstanceType)library->resolve(DllCreateInstanceId.c_str());
    if(!createInstanceFunc)
    {
        LOG_INTERN(LogLevel::Error) << "could not create instance from DLL";
        return false;
    }

    destroyInstanceFunc = (WorldInterface_DestroyInstanceType)library->resolve(DllDestroyInstanceId.c_str());
    if(!destroyInstanceFunc)
    {
        LOG_INTERN(LogLevel::Warning) << "world could not be released";
        return false;
    }

    try
    {
        LOG_INTERN(LogLevel::DebugCore) << "loaded world library " << library->fileName().toStdString()
                                        << ", version " << getVersionFunc();
    }
    catch(std::runtime_error const &ex)
    {
        LOG_INTERN(LogLevel::Error) << "could not retrieve version information from DLL: " << ex.what();
        return false;
    }
    catch(...)
    {
        LOG_INTERN(LogLevel::Error) << "could not retrieve version information from DLL";
        return false;
    }

    return true;
}

WorldLibrary::~WorldLibrary()
{
    if(worldInterface)
    {
        LOG_INTERN(LogLevel::Warning) << "unloading library which is still in use";
    }

    if(library)
    {
        if(library->isLoaded())
        {
            LOG_INTERN(LogLevel::DebugCore) << "unloading world library ";
            if(library->unload())
            {
                LOG_INTERN(LogLevel::DebugCore) << "Successfully unloaded library World_OSI";
            }
            else
            {
                LOG_INTERN(LogLevel::DebugCore) << "Failed to unload library World_OSI";
            }
        }

        delete library;
        library = nullptr;
    }
}

bool WorldLibrary::ReleaseWorld()
{
    if(!worldInterface)
    {
        return true;
    }

    if(!library)
    {
        return false;
    }

    try
    {
        destroyInstanceFunc(worldInterface);
    }
    catch(std::runtime_error const &ex)
    {
        LOG_INTERN(LogLevel::Error) << "world could not be released: " << ex.what();
        return false;
    }
    catch(...)
    {
        LOG_INTERN(LogLevel::Error) << "world could not be released";
        return false;
    }

    worldInterface = nullptr;

    return true;
}

WorldInterface *WorldLibrary::CreateWorld()
{
    if(!library)
    {
        return nullptr;
    }

    if(!library->isLoaded())
    {
        if(!library->load())
        {
            return nullptr;
        }
    }

    worldInterface = nullptr;
    try
    {
        worldInterface = createInstanceFunc(callbacks, stochastics, dataStore);
    }
    catch(std::runtime_error const &ex)
    {
        LOG_INTERN(LogLevel::Error) << "could not create stochastics instance: " << ex.what();
        return nullptr;
    }
    catch(...)
    {
        LOG_INTERN(LogLevel::Error) << "could not create stochastics instance";
        return nullptr;
    }

    if(!worldInterface)
    {
        return nullptr;
    }


    return worldInterface;
}

} // namespace SimulationSlave


