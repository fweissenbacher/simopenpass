/*******************************************************************************
* Copyright (c) 2017, 2018, 2019 in-tech GmbH
*               2016, 2017, 2018 ITK Engineering GmbH
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

//-----------------------------------------------------------------------------
//! @file  log.h
//! @brief This file contains the implementation of the debug logging.
//-----------------------------------------------------------------------------

#ifndef LOG_H
#define LOG_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <iostream>
#include <QMutex>
#include <QThread>
#include <QFile>
#include <map>

#if defined(LOG_TIME_ENABLED)
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>

inline std::string Log_NowTime()
{
    const int MAX_LEN = 200;
    char buffer[MAX_LEN];
    if (0 == GetTimeFormatA(LOCALE_USER_DEFAULT,
                            0,
                            0,
                            "HH':'mm':'ss",
                            buffer,
                            MAX_LEN))
    {
        return "Error in Log_NowTime()";
    }

    char result[100] = { 0 };
    static DWORD first = GetTickCount();
    std::sprintf(result,
                 "%s.%03ld",
                 buffer,
                 (long)(GetTickCount() - first) % 1000);
    return result;
}

#else // WIN32
#include <sys/time.h>

inline std::string Log_NowTime()
{
    char buffer[11];
    time_t t;
    time(&t);
    tm r;
    strftime(buffer, sizeof(buffer), "%X", localtime_r(&t, &r));
    struct timeval tv;
    gettimeofday(&tv, nullptr);
    char result[100] = { 0 };
    std::sprintf(result,
                 "%s.%03ld",
                 buffer,
                 (long)tv.tv_usec / 1000);
    return result;
}

#endif // WIN32
#endif // LOG_TIME_ENABLED

//! Custom Deleter class for use with unique_ptr. Prints a message to stderr when the corresponding
//! object is deleted
struct LogStreamDeleter
{
    void operator()(std::ofstream* ofs) const
    {
        //std::fprintf(stderr, "DEBUG: Deleting unique_ptr<ofstream> at address %p \n", ofs);
        delete ofs;
        ofs = nullptr;
    }
};

//! Severity of log
enum class LogLevel : int
{
    Error = 0, //!< Indicates abortion of program flow
    Warning, //!< Indicates change of intended program flow (doesn't necessarily lead to an abortion)
    Info, //!< Used for diagnostic information (must not be used for periodic events to prevent overflow of log)
    DebugUser, //!< Used for debugging information on model level (might be used for periodic events)
    DebugAPI, //!< Used for debugging information on framework level (functionality related to model interface)
    DebugCore, //!< Used for debugging information on framework level (internal functionality)
    Count
};

//! Coordinates logging
template<typename T>
class Log
{
public:
    Log() = default;
    Log(const Log &) = delete;
    Log(Log &&) = delete;
    Log &operator=(const Log &) = delete;
    Log &operator=(Log &&) = delete;
    virtual ~Log();

    //-----------------------------------------------------------------------------
    //! Retrieves output stream.
    //!
    //! Additional diagnostic information might be logged dependent on severity of
    //! log.
    //!
    //! @param[in]     file      File name where log is issued
    //! @param[in]     line      Line number where log is issued
    //! @param[in]     level     Severity of log.
    //! @return                  Output stringstream for the log message
    //-----------------------------------------------------------------------------
    std::ostringstream &Get(const char *file,
                            int line,
                            LogLevel level = LogLevel::Warning);

    //-----------------------------------------------------------------------------
    //! Retrieves current severity limit.
    //!
    //! Only log messages with a severity smaller or equal than this level will be
    //! printed.
    //!
    //! @return                  Current severity limit
    //-----------------------------------------------------------------------------
    static LogLevel &ReportingLevel();

    //-----------------------------------------------------------------------------
    //! Converts severity value to a string for printing purposes.
    //!
    //! @param[in]     level     Severity level
    //! @return                  Converted severity level
    //-----------------------------------------------------------------------------
    static std::string ToString(LogLevel level);

protected:
    std::ostringstream oss; //!< output string-stream of current log session

private:
    static QMutex logGuard; //!< protects output stream from concurrent access
    static LogLevel reportingLevel; //!< controls logging output. Messages with log level <= reporting level are logged
};

template<typename T> QMutex Log<T>::logGuard;

template<typename T> LogLevel Log<T>::reportingLevel = LogLevel::Warning;

template<typename T>
std::ostringstream &Log<T>::Get(const char *file, int line, LogLevel level)
{
#if defined(LOG_TIME_ENABLED)
    oss << Log_NowTime();
#endif // LOG_TIME_ENABLED
    oss << " " << ToString(level) << ": ";

    if (static_cast<int>(LogLevel::DebugUser) <= static_cast<int>(level))
    {
        oss << "(" << file << ":" << line << ") ";
    }

    oss << "ThreadID: " << QThread::currentThreadId() << " ";  // DEBUG: remove (long long) once done with debugging

    return oss;
}

template <typename T>
Log<T>::~Log()
{
    logGuard.lock();
    oss << std::endl;
    T::Output(oss.str());
    logGuard.unlock();
}

template <typename T>
LogLevel &Log<T>::ReportingLevel()
{
    return reportingLevel;
}

template <typename T>
std::string Log<T>::ToString(LogLevel level)
{
    static const char *const buffer[] =
        {
            "Error",
            "Warning",
            "Info",
            "DebugUser",
            "DebugApi",
            "DebugCore"
        };

    return buffer[static_cast<int>(level)];
}


//! Handles access of file
class LogOutputPolicy
{
public:
    ~LogOutputPolicy ()
    {
        std::cerr << "Deconstructor for LogOutputPolicy has been called";
        for (const auto &[id, stream] : logStreamMap)
        {
            std::cerr << "Closing stream for treadId " << id;
            stream->close();
        }
    }

    //-----------------------------------------------------------------------------
    //! Initializes output file.
    //!
    //! @param[in]     fileName      Name of file where logs are stored
    //-----------------------------------------------------------------------------
    static void SetFile(const std::string &fileName);

    //-----------------------------------------------------------------------------
    //! Verifies if output file has already been opened.
    //!
    //! @return      True if file is open
    //-----------------------------------------------------------------------------
    static bool IsOpen();

    //-----------------------------------------------------------------------------
    //! Logs message into file and standard output.
    //!
    //! @param[in]     message      Message to be logged.
    //-----------------------------------------------------------------------------
    static void Output(const std::string &message);

    //-----------------------------------------------------------------------------
    //!
    //! @return     True if log file is now open, False if an exception occured
    //-----------------------------------------------------------------------------
    static bool ReopenLogFile();

//private:  // NOTE: The variable below should be private -----v
    static std::map<long long, std::unique_ptr<std::ofstream, LogStreamDeleter>> logStreamMap;
    static std::string logFileName;
};

inline void LogOutputPolicy::SetFile(const std::string &fileName)
{
    logFileName = fileName;
    long long threadId = (long long)QThread::currentThreadId();
    std::unique_ptr<std::ofstream, LogStreamDeleter> logStream =
        std::unique_ptr<std::ofstream, LogStreamDeleter>(new std::ofstream(fileName));

    logStreamMap.emplace(std::make_pair(threadId, std::move(logStream)));

    /*
    std::fprintf(stderr,
                 "DEBUG: Creating unique_ptr for ofstream and moving it to address %p \n",
                 logStreamMap.at(threadId).get() );
    */
}

inline bool LogOutputPolicy::IsOpen()
{
    long long threadId = (long long)QThread::currentThreadId();
    if (logStreamMap.find(threadId) != logStreamMap.end())
    {
        bool is_open =  logStreamMap[threadId]->is_open();
        return is_open;
    }
    else
    {
        return false;
    }
}

inline void LogOutputPolicy::Output(const std::string &message)
{
    // print to standard output
//    std::cout << message.c_str();

    // print to file
    long long threadId = (long long)QThread::currentThreadId();
    if (logStreamMap.find(threadId) != logStreamMap.end())
    {
        *logStreamMap[threadId] << message;
        logStreamMap[threadId]->flush();
    }
}

inline bool LogOutputPolicy::ReopenLogFile()
{
    if(IsOpen())
    {
        Output("Log file '" + std::string(logFileName) + "' is already open\n");
        return true;
    }

    long long threadId = (long long)QThread::currentThreadId();
    std::unique_ptr<std::ofstream, LogStreamDeleter> logStream =
        std::unique_ptr<std::ofstream, LogStreamDeleter>(new std::ofstream());

    if(QFile::exists(QString::fromStdString(std::string(logFileName))))
    {
        logStream->open(logFileName, std::ofstream::app);
        logStreamMap.emplace(std::make_pair(threadId, std::move(logStream)));
        std::fprintf(stderr,
                     "DEBUG: Creating another unique_ptr for ofstream and moving it to address %p \n",
                     logStreamMap.at(threadId).get());
        return true;
    }
    else
    {
        std::fprintf(stderr,
                     "DEBUG: Unable to reload logfile '%s'\n",
                     logFileName.c_str());
        return false;
    }

}

//! Bind logging mechanism to file
typedef Log<LogOutputPolicy> LogFile;

//! Macro used for printing log of framework external components
#define LOG_EXTERN(level, file, line) \
    if(LogLevel::Count <= level) ; \
    else if(static_cast<int>(level) > static_cast<int>(LogFile::ReportingLevel()) || !LogOutputPolicy::IsOpen()) ; \
    else LogFile().Get(file, line, level)

//! Macro used for printing log of framework internal components
#define LOG_INTERN(level) \
    if(LogLevel::Count <= level) std::cerr << "Count <= level" << std::endl; \
    else if(static_cast<int>(level) > static_cast<int>(LogFile::ReportingLevel())); \
    else if (!LogOutputPolicy::IsOpen()) std::cerr << "(" << __FILE__ << ":" << __LINE__ << ") Log file not open" << std::endl; \
    else LogFile().Get(__FILE__, __LINE__, level)


[[noreturn]] static void LogErrorAndThrow(const std::string &message)
{
    LOG_INTERN(LogLevel::Error) << message;
    throw std::runtime_error(message);
}

[[maybe_unused]] static void ThrowIfFalse(bool success, const std::string &message)
{
    if (!success)
    {
        LogErrorAndThrow(message);
    }
}


#endif // LOG_H
