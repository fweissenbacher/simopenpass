/*********************************************************************
* Copyright (c) 2017 ITK Engineering GmbH
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

#include "common/log.h"

//template class std::map<long long, std::unique_ptr<std::ofstream, LogStreamDeleter>>;

std::map<long long, std::unique_ptr<std::ofstream, LogStreamDeleter>> LogOutputPolicy::logStreamMap;
std::string LogOutputPolicy::logFileName = "not-found";