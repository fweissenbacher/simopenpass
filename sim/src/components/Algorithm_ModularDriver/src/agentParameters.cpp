/******************************************************************************
* Copyright (c) 2021 HLRS, Universitaet Stuttgart
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

#include "agentParameters.h"


agentParameters &agentParameters::operator = (const VehicleModelParameters &vp)
{
    vehicleType = vp.vehicleType;
    width = vp.width;
    length = vp.length;
    height = vp.height;
    wheelbase = vp.wheelbase;
    trackwidth = vp.trackwidth;
    distanceReferencePointToLeadingEdge = vp.distanceReferencePointToLeadingEdge;
    distanceReferencePointToFrontAxle = vp.distanceReferencePointToFrontAxle;
    maxVelocity = vp.maxVelocity;
    maxAcceleration = vp.maxAcceleration;
    maxDeceleration = vp.maxDeceleration;
    weight = vp.weight;
    heightCOG = vp.heightCOG;
    momentInertiaRoll = vp.momentInertiaRoll;
    momentInertiaPitch = vp.momentInertiaPitch;
    momentInertiaYaw = vp.momentInertiaYaw;
    frontSurface = vp.frontSurface;
    airDragCoefficient = vp.airDragCoefficient;
    minimumEngineSpeed = vp.minimumEngineSpeed;
    maximumEngineSpeed = vp.maximumEngineSpeed;
    minimumEngineTorque = vp.minimumEngineTorque;
    maximumEngineTorque = vp.maximumEngineTorque;
    numberOfGears = vp.numberOfGears;
    gearRatios = vp.gearRatios;
    axleRatio = vp.axleRatio;
    decelerationFromPowertrainDrag = vp.decelerationFromPowertrainDrag;
    steeringRatio = vp.steeringRatio;
    maximumSteeringWheelAngleAmplitude = vp.maximumSteeringWheelAngleAmplitude;
    maxCurvature = vp.maxCurvature;
    staticWheelRadius = vp.staticWheelRadius;
    frictionCoeff = vp.frictionCoeff;
    return *this;
}
