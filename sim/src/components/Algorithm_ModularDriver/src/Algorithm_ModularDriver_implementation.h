/******************************************************************************
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
* Copyright (c) 2021 HLRS, Universitaet Stuttgart
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  Algorithm_ModularDriver_implementation.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief Represents the cognitive processes of the driver from information acquisition to action excecution.
//!
//! This class contains the data calculations.
//! Singleton such that data calculations is generated only once and not seperately
//! for each instance.
//!
//-----------------------------------------------------------------------------

/*! \addtogroup Algorithm_Modular_Driver
 * @{
 *
 * \brief The calculation of all mental aspects of an agent from the acquisition of surrounding informations, their mental representation and the derivation of actions
 *
 * \section Algorithm_ModularDriver_Inputs Inputs
 * Input variables:
 * name | meaning
 * -----|---------
 * IA_I_BU.StaticEnvironment   | Container with the static elements in the environment inside the InformationAcquisition_Input_Bottom_Up
 * IA_I_BU.EgoData   | Container with the ego information inside the InformationAcquisition_Input_Bottom_Up
 * IA_I_BU.SurroundingMovingObjectsData   | Container with the moving elements in the environment inside the InformationAcquisition_Input_Bottom_Up
 *
 * Input channel IDs:
 * Input ID | signal class | contained variables
 * ------------|--------------|-------------
 * 0 | structSignal<StaticEnvironmentData> | IA_I_BU.StaticEnvironment
 * 1 | structSignal<egoData> | IA_I_BU.EgoData
 * 2 | structSignal<SurroundingMovingObjectsData> | IA_I_BU.SurroundingMovingObjectsData
 *
 *
 * \section Sensor_Modular_Driver_ConfigParameters Parameters to be specified in SystemConfiguration.xml
 *
 * name | id | unit | distribution | meaning | space
 * ----------        | -----------        | -----------        | -----------        | -----------        | -----------
 * AvertViewTimeMean	|	0	|	ms	|	Log-Normalverteilung	|	Mittlere Dauer der Blickabwendung bis zur nächsten Aktualisierung der Umgebungsinformationen (Agenten, Umwelt) à Informationen werden in diesem Zeitraum mental extrapoliert	|	0 ... 5 000
 * AvertViewTimeDeviation	|	1	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu AvertViewTimeMean 	|	0 … 1 000
 * ReactionTimeMean	|	2	|	ms	|	Log-Normalverteilung	|	Reaktionszeit bei Reaktion auf eine neue Verkehrssituation (aktuell neuer „Leader“, „Follower“ je auf allen Fahrstreifen)	|	0 … 1 000
 * ReactionTimeDeviation	|	3	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu ReactionTimeMean	|	0 … 100
 * MaxComfortAcceleration	|	4	|	m/s²	|	Normalverteilung	|	Maximale Beschleunigung bei Normalfahrt (Mittelwert)	|	1 ... 5
 * MinComfortAcceleration	|	5	|	m/s²	|	Normalverteilung	|	Minimale Verzögerung bei Normalfahrt (Mittelwert)	|	-5 … -0.5
 * ComfortAccelerationDeviation	|	6	|	m/s²	|	Normalverteilung	|	Standardabweichung zu MaxComfortAcceleration & MinComfortAcceleration	|	0 … 1
 * VelocityWish	|	7	|	km/h	|	Log-Normalverteilung	|	Mittlere Wunschgeschwindigkeit	|	0 … 300
 * VelocityWishDeviation	|	8	|	km/h	|	Log-Normalverteilung	|	Standardabweichung zu VelocityWish	|	0 … 60
 * TGapWish	|	9	|	s	|	Log-Normalverteilung	|	Wunschzeitlücke zu einem Vorderfahrzeug	|	0.2 … 3
 * TGapWishDeviation	|	10	|	s	|	Log-Normalverteilung	|	Standardabweichung zu TGapWish	|	0 … 1
 * MeanSpeedLimitViolation	|	11	|	km/h	|	Normalverteilung	|	Mittlerer Aufschlag auf den Geschwindigkeitswunsch bei vorhandenem Tempolimit 	|	0 …20
 * MeanSpeedLimitViolationDeviation	|	12	|	km/h	|	Normalverteilung	|	Standardabweichung zu MeanSpeedLimitViolation	|	0 … 10
 * MinDistance	|	13	|	m	|	-	|	Mindestabstand zum Vorderfahrzeug	|	0 … 2
 * SpeedGain	|	14	|	-	|	Normalverteilung	|	Der mittlere Drang, Spurwechsel durchzuführen, um Geschwindigkeit zu gewinnen. Höhere Werte führen zu mehr Spurwechseln	|	0 … 20
 * SpeedGainDeviation	|	15	|	-	|	Normalverteilung	|		|	0 … 5
 * KeepRight	|	16	|	-	|	Normalverteilung	|	Der mittlere Drang, dem Rechtsfahrgebot nachzukommen. Höhere Werte führen zu einem früheren Spurwechsel	|	0 … 1
 * KeepRightDeviation	|	17	|	-	|	Normalverteilung	|		|	0 … 1
 * Cooperative	|	18	|	-	|	Normalverteilung	|	Die Bereitschaft zum kooperativen Spurwechsel. Niedrigere Werte führen zu verminderter Kooperation	|	0 … 0.2
 * CooperativeDeviation	|	19	|	-	|	Normalverteilung	|		|	0 … 0.1
 * CriticalityLogging	|	20	|		|		|	Einschalten des Loggings von „ttc“, „thw“ bzw. „speeding“	| -
 * TtcThreshold_mean	|	21	|	s	|	Log-Normalverteilung	|	Grenze unterhalb derer der Agent vom Komfort-Verzögerungspotential in das Notbremspotential wechselt	|	0 … ∞
 * TtcThreshold_std	|	22	|	s	|	Log-Normalverteilung	|	-	| -
 * ViewTimeMean	|	23	|	ms	|	Log-Normalverteilung	|	Mittlere Dauer der Blickzuwendung bis zur nächsten Blickabwendung	|	0 … ∞
 * ViewTimeDeviation	|	24	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu ViewTimeMean 	|	0 … ∞
 * ForwardViewTimeMean  |   25  |   ms  |   Log-Normalverteilung    |   Mittlere Blickzuwendung in Fahrtrichtung    |   0 ... ∞
 * ForwardViewTimeDeviation	|	26	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu ForwardViewTimeMean 	|	0 … ∞
 * BackwardViewTimeMean  |   27  |   ms  |   Log-Normalverteilung    |   Mittlere Blickzuwendung entgegen der Fahrtrichtung    |   0 ... ∞
 * BackwardViewTimeDeviation	|	28	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu BackwardViewTimeMean 	|	0 … ∞
 * RelativePositionError	|	29	|	ms	|	Normalverteilung	|	Standardabweichung der relativen Abweichung der erkannten Position eines anderen Agenten bezogen auf die aktuelle Geschwindigkeit 	|	0 … 0.2
 * RelativeVelocityError	|	30	|	ms	|	Normalverteilung	|	Standardabweichung der relativen Abweichung der erkannten Geschwindigkeit eines anderen Agenten bezogen auf die aktuelle Geschwindigkeit 	|	0 … 0.2
 * RelativeAccelerationError	|	31	|	ms	|	Normalverteilung	|	Standardabweichung der relativen Abweichung der erkannten Beschleunigung eines anderen Agenten bezogen auf die aktuelle Beschleunigung 	|	0 … 0.2
 */

#pragma once

#include <string>
#include <iostream>
//#include <map>
#include <include/modelInterface.h>
#include "common/primitiveSignals.h"
#include "AlgorithmInformationAcquisition.h"
#include "AlgorithmMentalModel.h"
#include "AlgorithmSituationAssessment.h"
#include "AlgorithmActionDeduction.h"
#include "AlgorithmActionExecution.h"
#include "agentParameters.h"


/**
 * \ingroup Algorithm_Modular_Driver
*/
class AlgorithmModularDriverImplementation : public AlgorithmInterface
{
public:
    //! Name of the current component
    const std::string COMPONENTNAME = "AlgorithmModularDriver";

    //! \brief Constructor.
    //!
    //! \param [in] componentId   Component ID
    //! \param [in] isInit        Component's init state
    //! \param [in] priority      Task priority level
    //! \param [in] offsetTime    Start time offset
    //! \param [in] responseTime  Update response time
    //! \param [in] cycleTime     Cycle time
    //! \param [in] stochastics   Stochastics instance
    //! \param [in] world         World interface
    //! \param [in] parameters    Paramaters
    //! \param [in] observations  Observation instance
    //! \param [in] callbacks     Callbacks
    //! \param [in] agent         Agent
    AlgorithmModularDriverImplementation(
            std::string componentName,
            bool isInit,
            int priority,
            int offsetTime,
            int responseTime,
            int cycleTime,
            StochasticsInterface *stochastics,
            WorldInterface *world,
            const ParameterInterface *parameters,
            PublisherInterface *const publisher,
            const CallbackInterface *callbacks,
            AgentInterface *agent);

    AlgorithmModularDriverImplementation(const AlgorithmModularDriverImplementation&) = delete;
    AlgorithmModularDriverImplementation(AlgorithmModularDriverImplementation&&) = delete;
    AlgorithmModularDriverImplementation& operator=(const AlgorithmModularDriverImplementation&) = delete;
    AlgorithmModularDriverImplementation& operator=(AlgorithmModularDriverImplementation&&) = delete;
    virtual ~AlgorithmModularDriverImplementation();

    /*!
    * \brief Update Inputs
    *
    * Function is called by framework when another component delivers a signal over
    * a channel to this component (scheduler calls update taks of other component).
    *
    * Refer to module description for input channels and input ids.
    *
    * @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
    * @param[in]     data           Referenced signal (copied by sending component)
    * @param[in]     time           Current scheduling time
    */
    void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time);

    /*!
    * \brief Update outputs.
    *
    * Function is called by framework when this Component.has to deliver a signal over
    * a channel to another component (scheduler calls update task of this component).
    *
    * Refer to module description for output channels and output ids.
    *
    * @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
    * @param[out]    data           Referenced signal (copied by this component)
    * @param[in]     time           Current scheduling time
    */
    void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time);

    /*!
    * \brief Process data within component.
    *
    * Function is called by framework when the scheduler calls the trigger task
    * of this component.
    *
    * @param[in]     time           Current scheduling time
    */
    void Trigger(int time);


    /*! \callergraph
     * \brief Information_Acquisition
     *
     * Information from the sensor is collcted and the real vision of the agent is defined.
     * Call of functions of the object of class #InformationAcquisition
     *
     * \param IA_I_TD   Information_Acquisition_Input_Top_Down Container
     * \param IA_I_BU   Information_Acquisition_Input_Bottom_Up Container
     * \param IA_O      Information_Acquisition_Output Container
     * \param time      Current scheduling time
     */
    void Information_Acquisition (InformationAcquisition_Input_TD *IA_I_TD, InformationAcquisition_Input_BU *IA_I_BU, InformationAcquisition_Output *IA_O, int time);

    /*!
     * \brief Mental_Model
     *
     * Mental representation of the saved informations from the Information-Acquisition.
     * Call of functions of the object of class #MentalModel
     *
     * \param MM_I_TD       Mental_Model_Input_Top_Down Container
     * \param MM_I_BU       Mental_Model_Input_Bottom_up Container
     * \param MM_O          Mental_Model_Output Container
     */
    void Mental_Model (MentalModel_Input_TD *MM_I_TD, MentalModel_Input_BU *MM_I_BU, MentalModel_Output *MM_O);

    /*!
     * \brief Situation_Assessment
     *
     * Evaluation of the current Situation based on the available information from the Mental-Model
     * e.g. current sourrunding vehicles are set to descriptions (leader, follower...).
     * Call of functions of the object of class #SituationAssessment
     *
     * \param SA_I              Situation_Assessment_Input Container
     * \param SA_O_BU           Situation_Assessment_Output_Bottom_Up Container
     * \param SA_O_TD           Situation_Assessment_Output_Top_Down Container
     * \param vehicleParameters Parameters of the ego-vehicle
     * \param time              Current scheduling time
     */
    void Situation_Assessment (SitationAssessment_Input *SA_I, SituationAssessment_Output_BU *SA_O_BU, SituationAssessment_Output_TD *&SA_O_TD, const agentParameters *vehicleParameters, int time);

    /*!
     * \brief Action_Deduction
     *
     * Derivation of actions of the agent like following behaviour or lane-change.
     * Call of functions of the object of class #ActionDeduction
     *
     * \param AD_I              Action_Deduction_Input Container
     * \param AD_O_BU           Action_Deduction_Output_Bottom_Up Container
     * \param AD_O_TD           Action_Deduction_Output_Top_Down Container
     * \param vehicleParameters Parameters of the ego-vehicle
     * \param time              Current scheduling time
     */
    void Action_Deduction(ActionDeduction_Input *AD_I, ActionDeduction_Output_BU *AD_O_BU, ActionDeduction_Output_TD *AD_O_TD, const agentParameters *vehicleParameters, int time);

    /*!
     * \brief Action_Execution
     *
     * Setting of the actions based on the selected wishes in the action-deduction.
     * Call of functions of the object of class #ActionDeduction
     *
     * \param AE_I              Action_Execution_Input Container
     * \param AE_O_BU           Action_Execution_Output_Bottom_Up Container
     * \param AE_O_TD           Action_Execution_Output_Top_Down Container
     * \param vehicleParameters Parameters of the ego-vehicle
     * \param time              Current scheduling time
     */
    void Action_Execution(ActionExecution_Input *AE_I, ActionExecution_Output_BU *AE_O_BU, ActionExecution_Output_TD *&AE_O_TD, const agentParameters *vehicleParameters, int time);

    //----------------------------------------------------------------

private:

    bool initializedVehicleModelParameters = false;

    //! The state of realignment to queue [-].
    bool out_realignToQueue = false;   

    //!
    StaticEnvironmentData StaticEnvironment; //! Internal object for the static environment, Input parameter
    std::vector<SurroundingMovingObjectsData> SurroundingMovingObjects; //! Internal container for the surrounding moving objects, Input parameter
    egoData Ego; //! Internal object for the ego information, Input parameter

    InformationAcquisition informationacquisition;  //! Object of class InformationAcquisition
    MentalModel mentalmodel;                        //! Object of class MentalModel
    SituationAssessment situationassessment;        //! Object of class SituationAssessment
    ActionDeduction actiondeduction;                //! Object of class ActionDeduction
    ActionExecution actionexecution;                //! Object of class ActionExecution

    InformationAcquisition_Input_BU IA_I_BU;
    InformationAcquisition_Input_TD IA_I_TD;
    MentalModel_Input_TD MM_I_TD;
    SituationAssessment_Output_TD *SA_O_TD;
    ActionDeduction_Output_TD *AD_O_TD;
    ActionExecution_Output_TD *AE_O_TD;

    //! The longitudinal acceleration of the vehicle [m/s^2].
    double out_longitudinal_acc =0;

    //! The state of the turning indicator [-].
    int out_indicatorState = static_cast<int>(IndicatorState::IndicatorState_Off);
    //! Activation of HornSwitch [-].
    bool out_hornSwitch = false;
    //! Activation of Headlight [-].
    bool out_headLight = false;
    //! Activation of Highbeam Light [-].
    bool out_highBeamLight = false;
    //! Activation of Flasher [-].
    bool out_flasher = false;

    double out_desiredSteeringWheelAngle;

    //  --- Outputs
    //! Position of the accecaleration pedal position in percent.
    double out_accPedalPos = 0;
    //! Position of the brake pedal position in percent.
    double out_brakePedalPos = 0;
    //! Number of gears and the currently choosen gear.
    int out_gear = {0};

    std::vector<double> out_DoubleSignalVector = {0,0,0};

    //! component state for finely granulated evaluation of signal
    ComponentState componentState = ComponentState::Acting;


    //! Flag that indicates the need to initialize the InformationAcquisition - e.g parsing input informations
    bool initialisationIA = false;
    //! Flag that indicates the need to initialize the MentalModel - e.g parsing input informations
    bool initialisationMM = false;
    //! Flag that indicates the need to initialize the SituationAssessment - e.g parsing input informations
    bool initialisationSA = false;
    //! Flag that indicates the need to initialize the ActionDeduction - e.g parsing input informations
    bool initialisationAD = false;
    //! Flag that indicates the need to initialize the ActionExcecution - e.g parsing input informations
    bool initialisationAE = false;

    agentParameters vehicleParameters;

    StochasticsInterface *stochastic;

    ObservationInterface* observer = nullptr; ///!< Observer containing the eventnetwork into which (de-)activation events are inserted

};
