/******************************************************************************
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  SituationLogging.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief provide data to the observer for logging of criticality values
//!
//-----------------------------------------------------------------------------

#pragma once
#include "common/globalDefinitions.h"
#include <boost/geometry/geometries/adapted/c_array.hpp>
#include "common/boostGeometryCommon.h"
#include "osi3/osi_sensordata.pb.h"
#include "SituationCalculation.h"
#include "include/observationInterface.h"
#include "include/modelInterface.h"


//! \ingroup Algorithm_Situation_Assessment
//! \defgroup SituationLogging

//! \ingroup SituationLogging
class SituationLogging
{
public:
    SituationLogging(int cycleTime, SituationCalculation *situationCalculation);

    //!
    //! \brief Initialize
    //!
    //! Initializes if there are values to log and set each status
    //!
    //! \param LoggingGroups vector with the logging group strings
    //! \param observer
    //! \return
    //!
    bool Initialize(std::vector<std::string> LoggingGroups, ObservationInterface *observer);
    //!
    //! \brief CheckForLoggingStatesAndLog
    //!
    //! Calls for each possible logging value the function LoggRelationValues oder LoggDoubleValue after each value is calculated
    //!
    //! \param time
    //! \param pov
    //!
    void CheckForLoggingStatesAndLog(int time, std::string pov);
    //!
    //! \brief LoggDoubleValue
    //! \param keylog
    //! \param value
    //! \param time
    //!
    void LoggDoubleValue(std::string keylog, double value, int time);
    //!
    //! \brief LoggRelationValues
    //! \param Agents
    //! \param values
    //! \param key
    //! \param time
    //!
    void LoggRelationValues(const std::vector<int> *Agents, std::vector<double> values, std::string key, int time);

private:
    //!
    bool log_ttc = false;
    //!
    bool log_ettc = false;
    //!
    bool log_thw = false;
    //!
    bool log_speeding = false;
    //!
    ObservationInterface *observer;

    //!
    SituationCalculation *situationCalculation;
};
