/******************************************************************************
* Copyright (c) 2019 AMFD GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  AlgorithmInformationAcquisition.cpp
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief This file contains the calculations
//!
//! This class contains the acquisition of informations like visual and auditive.
//!
//-----------------------------------------------------------------------------

/**
 * @ingroup al_mod_driver
 * @defgroup alg_inf_acq Algorithm Information Acquisition
 * - This module contains the acquisition of informations like visual and auditive.
 * Singleton such that data calculations is generated only once and not seperately
 * for each instance.
*/

#include <iostream>
#include "AlgorithmInformationAcquisition.h"

InformationAcquisition::InformationAcquisition(StochasticsInterface *stochastics, ObservationInterface *observation):
    _stochastic(stochastics),
    _observation(observation)
{
    v_y_Max = _stochastic->GetNormalDistributed(1,0.2); //TODO
}

void InformationAcquisition::SetInformationAcquisition_Input(InformationAcquisition_Input_TD *Input_TD, InformationAcquisition_Input_BU *Input_BU)
{ 
    IA_Input_BU = Input_BU;
    IA_Input_TD = Input_TD;
}

void InformationAcquisition::SetEgoData(InformationAcquisition_Output *IA_Output)
{
    IA_Output->Ego = &IA_Input_BU->EgoData;
}

void InformationAcquisition::SetEnvironmentInformation(InformationAcquisition_Output *IA_Output)
{
   IA_Output->EnvironmentInfo= &IA_Input_BU->StaticEnvironment;
}

void InformationAcquisition::SetSurroundingMovingObjects(InformationAcquisition_Output *IA_Output)
{
    if (IsReactive())
    {
        if (IsSwitchingView())
        {
            if (IsLookingForward())
            {
                LookingForward();
                SelectAgentsForward(IA_Input_BU);
            }
            else
            {
                LookingBackward();
                SelectAgentsBackward(IA_Input_BU);
            }
        }

        IA_Output->SurroundingMovingObjects = &IA_Input_BU->SurroundingMovingObjects;

        //std::cout << IA_Input_BU->EgoData.GetState()->id << ", " << 1 << std::endl;
    }
    else
    {
        IA_Input_BU->SurroundingMovingObjects.clear();
        IA_Output->SurroundingMovingObjects = &IA_Input_BU->SurroundingMovingObjects;
        //std::cout << IA_Input_BU->EgoData.GetState()->id << ", " << 0 << std::endl;
    }
}


bool InformationAcquisition::IsReactive()
{
    if (!init)
    {
        viewTimeTrigger = 0;
        currentViewTime = std::max(0,(int) std::round(_stochastic->GetLogNormalDistributed(meanViewTime,ViewTimeDeviation)/10)*10);
        reactionTimeTrigger = 0;
        currentReactionTime = std::max(minAvertViewTime,(int) std::round(_stochastic->GetLogNormalDistributed(meanAvertViewTime,AvertViewTimeDeviation)/10)*10);
        isreactive = true;
        init = true;
    }

    if (currentReactionTime==0 && currentViewTime==0)
        return true;
    if (currentReactionTime==0)
        return true;

    if(!isreactive)
    {
        if (reactionTimeTrigger>=currentReactionTime)
        {
            reactionTimeTrigger = 0;
            currentReactionTime = std::max(minAvertViewTime,(int) std::round(_stochastic->GetLogNormalDistributed(meanAvertViewTime,AvertViewTimeDeviation)/10)*10);
            isreactive = true;
        }
        else
        {
            reactionTimeTrigger += 100;
            isreactive = false;
        }
    }

    if(isreactive)
    {
        if (viewTimeTrigger>=currentViewTime)
        {
            viewTimeTrigger = 0;
            currentViewTime = std::max(0,(int) std::round(_stochastic->GetLogNormalDistributed(meanViewTime,ViewTimeDeviation)/10)*10);
            isreactive = false;
            reactionTimeTrigger += 100;

            if(currentViewTime == 0) // even if the viewTime is set to 0, the driver hast to look one timestep to update the mental-model after each avert-view
                return true;
        }
        else
        {
            viewTimeTrigger += 100;
            isreactive = true;
        }
    }
    return isreactive;
}

void InformationAcquisition::LogSetValues(int time)
{
      //!!!
//    _observation->Insert(time,
//                        IA_Input_BU->EgoData.GetState()->id,
//                        LoggingGroup::Driver,
//                        "CurrentAvertViewTime",
//                        std::to_string(currentReactionTime));

//    if (IA_Input_BU->EgoData.GetState()->collisionpartners.size()>0)
//    {
//        std::string partners;

//        auto collisionPartners = IA_Input_BU->EgoData.GetState()->collisionpartners;
//        for (const auto &partner : collisionPartners)
//        {
//            partners += std::to_string(partner.second) + ", ";
//        }

//        std::string out =   "CollisionAgentId: " + std::to_string(IA_Input_BU->EgoData.GetState()->id) +
//                            ", CollisionPartners: " + partners +
//                            "CurrAvertView: " + std::to_string(currentReactionTime) +
//                            ", CurrAvertTrigger: " + std::to_string(reactionTimeTrigger);

//        std::shared_ptr<BasicEvent> event = std::make_shared<BasicEvent>(time,
//                                                                         out,
//                                                                         "sequenceName",
//                                                                         EventDefinitions::EventType::ComponentStateChange);

//        _observation->InsertEvent(event);
//    }
      //!!!
}

