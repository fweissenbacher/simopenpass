/******************************************************************************
* Copyright (c) 2021 HLRS, Universitaet Stuttgart
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

#pragma once
#include <common/globalDefinitions.h>

class agentParameters : public VehicleModelParameters
{
public:
    agentParameters &operator=(const VehicleModelParameters &);
    //double maxDeceleration;
    //double maxAcceleration;
};
