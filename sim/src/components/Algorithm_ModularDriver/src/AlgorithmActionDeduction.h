/******************************************************************************
* Copyright (c) 2019 AMFD GmbH
* Copyright (C) 2001-2018 German Aerospace Center (DLR) and others.
* Copyright (c) 2021 HLRS, Universitaet Stuttgart
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  AlgorithmActionDeduction.h
//! @author  Konstantin Blenz
//! @author  Daniel Krajzewicz
//! @author  Jakob Erdmann
//! @author  Friedemann Wesner
//! @author  Sascha Krieg
//! @author  Michael Behrisch
//! @date    Tue, 03.12.2019
//! @brief This file contains the calculations of possible actions
//!
//! This class contains the data calculations.
//! Singleton such that data calculations is generated only once and not seperately
//! for each instance.
//!
//-----------------------------------------------------------------------------

#pragma once

//#include <vector>
#include <include/stochasticsInterface.h>
#include <include/observationInterface.h>
#include "../../Sensor_Modular_Driver/src/Container/ContainerStructures.h"
#include "ActionDeductionMethods/CFModel.h"
#include "ActionDeductionMethods/CFModel_Daniel1.h"
#include "ActionDeductionMethods/AbstractLaneChangeModel.h"
#include "ActionDeductionMethods/LCM_LC2013.h"
#include "ActionDeductionMethods/TargetBraking.h"
#include "agentParameters.h"

//! \brief This class contains the data calculations of possible actions
//!
//! \ingroup Algorithm_Modular_Driver
//! \defgroup Algorithm_Action_Deduction
//!

//! \ingroup Algorithm_Action_Deduction
class ActionDeduction
{
public:
    ActionDeduction(int cycletime, StochasticsInterface *stochastics, ObservationInterface *observer); // avoid access of constructor of singleton outside class

    //!
    //! \brief Initialize
    //!
    //! initializes the static values from the input-parameters for the class:
    //!
    //! \param maxComfortAcceleration   maximum comfortable Accleration, normal-distributed with config-input "maxComfotAccleration" and "comfortAccelDeviation", while < 1.0 -> repeated
    //! \param minComfortDeceleration   minimum comfortable Deceleration, normal-distributed with config-input "maxComfotAccleration" and "minComfortDeceleration", while > -0.5 -> repeated
    //! \param CarFollowingModel        Car-following-model is with the previously set values initialized
    //! \param SpeedGain                The eagerness for performing lane changing to gain speed, normal-distributed with config-input "SpeedGain" and "SpeedGainDeviation", while < 0 -> repeated
    //! \param KeepRight                The eagerness for following the obligation to keep right, normal-distributed with config-input "KeepRight" and "KeepRightDeviation", while < 0 -> repeated
    //! \param Cooperative              willingness for performing cooperative lane changing, normal-distributed with config-input "Cooperative" and "CooperativeDeviation", while < 0 and > 1 -> repeated
    //! \param LaneChangeModel          Lane-change-model is with the previously set values initialized
    //! \param targetBraking            Car-following-model is updated inside the targetBraking-module
    //!
    void Initialize();

    //!
    //! \brief SetActionDeduction_Input
    //!
    //! sets the input of the class
    //! \param Input Container of type ActionDeduction_Input
    //!
    void SetActionDeduction_Input(ActionDeduction_Input &Input);

    //!
    //! \brief LogSetValues
    //!
    //! Log the initial values of the stochastic set values to the observer, when they're set
    //! \param time
    //!
    void LogSetValues(int time);

    //!
    //! \brief GetActionDecution_Output_BU
    //! \param AD_Output_BU
    //!
    void GetActionDecution_Output_BU(ActionDeduction_Output_BU &AD_Output_BU);

    //!
    //! \brief GetActionDecution_Output_TD
    //! \return
    //!
    ActionDeduction_Output_TD* GetActionDecution_Output_TD();

    //!
    //! \brief CalcAccelerationWish
    //!
    //! Calculates the ActionDeduction-output-values for the velocity-wish and acceleration-wish for the next time-step.
    //! - Depending on an existing leading vehicle on the current lane or a existing stop sign in sight-distance.
    //! - If leading vehicle -> Call of CalcFollowVelocityDiffWish
    //! - If time-to-collision is under the in the input-configs set "ttc_threshold", then the deceleration potential is set to maxDeceleration
    //! - If no leader, then free-speed is calculated from the car-following-model
    //! \param AD_Output_BU
    //!
    void CalcAccelerationWish(ActionDeduction_Output_BU &AD_Output_BU);

    //!
    //! \brief CalcFollowVelocityDiffWish
    //!
    //! Call of the car-following-model to calculate the desired speed (safe-velocity) in the next-timestep.
    //! Minimum value between the common velocity-wish and the calculated safe-velocity is set.
    //! Calculation of the speed-difference for the next-timestep.
    //!
    //! \param v_long           current longitudinal velocity
    //! \param delta_s_front    distance to the leading vehicle
    //! \param v_long_Front     longitudinal velocity of the leading vehicle
    //! \return                 delta to the current longitudinal velocity for the next time-step
    //!
    double CalcFollowVelocityDiffWish(const double &v_long, const double &delta_s_front, const double &v_long_Front);

    //!
    //! \brief CalcFreeVelocityDiffWish
    //!
    //! Calculates the delta between current longitudinal velocity and the minimum value between common velocity-wish or maximum-velocity
    //! \param v_long   current longitudinal velocity
    //! \return         delta to the current longitudinal velocity for the next time-step
    //!
    double CalcFreeVelocityDiffWish(const double &v_long);

    //!
    //! \brief CheckLaneChange
    //!
    //! - Prepares the check for a lane change for the next time-step and calls the function CheckforLaneChange() with the changeable pointers v_long_next and a_long_next.
    //! - Sets the connection to the output for velocity-wish and acceleration-wish
    //!
    //! \param time             current time-step [ms]
    //! \param AD_Output_BU     Output container of the whole module
    //!
    void CheckLaneChange(const int *time, ActionDeduction_Output_BU *AD_Output_BU);

    //!
    //! \brief CheckforLaneChange
    //!
    //! - Checks for lane-change 500 ms after the spawn-time-step with the chall of the function CheckChangeDirection()
    //! - Finalizes the speed-wish for the next time-step with the call of the car-following-model
    //! - computes the next
    //!
    //! \param Environment
    //! \param vNext [in/out]
    //! \param aNext [in/out]
    //! \param time
    //! \param spawnTime
    //! \return
    //!
    LaneChangeState CheckforLaneChange(const StaticEnvironmentData *Environment,
                                       double *vNext,
                                       double *aNext,
                                       const int *time,
                                       const int *spawnTime);

    //!
    //! \brief CheckChangeDirection
    //!
    //! Checks the lane-change-status for the next time-step by calling the function checkCangeWithinEdge() with each of the possible directions,
    //! depending on the lanemarking (!= solid) and the existance of a neighbor lane.
    //! If decision is on both directions and urgent, agent has to go to the right.
    //!
    //! \param Environment
    //! \param time         current time-step [ms]
    //! \param spawnTime    spawn time-step of the current agent [ms]
    //! \return
    //!
    LaneChangeState CheckChangeDirection(const StaticEnvironmentData *Environment,
                                         const int *time,
                                         const int *spawnTime);

    //!
    //! \brief checkChangeWithinEdge
    //!
    //! Checks the lane-change for a special direction (Adaption from SUMO)
    //! - Prepares the surrounding moving objects for the direction to check a lane-change for
    //! - Checks, if neighboring vehicle is overlapping or within a secure-gap and blocks lane-change
    //! - Calls the lane-change-model function LCM_LC2013::wantsChange()
    //! - Lots of unused statements
    //!
    //! \param laneOffset   direction of lane-change demand
    //! \param Environment
    //! \param preb
    //! \return LaneChangeState for viewed the direction (laneOffset)
    //!
    int checkChangeWithinEdge(int laneOffset,
                            const StaticEnvironmentData *Environment,
                            const std::vector<int>* preb) const;

    //!
    //! \brief ComputeAcceleration
    //!
    //! Calculates the Acceleration from a velocity-difference depending on a current possible acceleration or deceleration
    //!
    //! \param vDelta   velocity-difference
    //! \param decel    current minimum deceleration
    //! \return         Acceleration [m/s²]
    //!
    double ComputeAcceleration(double *vDelta, double decel);

    //!
    //! \brief prepareLanechange
    //!
    //! Prepares the lane-change and sets the output lateral displacement depending on the lane-change decision
    //!
    //! \param AD_Output_BU
    //!
    void prepareLanechange(ActionDeduction_Output_BU *AD_Output_BU);

    //!
    //! \brief checkForChanged
    //!
    //! Checks if current lane-id is different from the lane-id in the previous time-step.
    //! If so, then LaneChangeModel status is set to "changed"
    //!
    void checkForChanged();

    //!
    //! \brief SetMaxComfortAccel
    //! \param mcacc
    //!
    void SetMaxComfortAccel(double mcacc)
    {
        maxComfortAcceleration = mcacc;
    }
    //!
    //! \brief SetMinComfortDecel
    //! \param mcdec
    //!
    void SetMinComfortDecel(double mcdec)
    {
        minComfortDeceleration = mcdec;
    }
    //!
    //! \brief SetComfortAccelDev
    //! \param caccDev
    //!
    void SetComfortAccelDev(double caccDev)
    {
        comfortAccelDeviation = caccDev;
    }
    //!
    //! \brief SetTGapWish
    //! \param tGapWish
    //!
    void SetTGapWish(double tGapWish)
    {
        this->tGapWish = tGapWish;
    }
    //!
    //! \brief SetSpeedGain
    //! \param SpeedGain
    //!
    void SetSpeedGain(double SpeedGain)
    {
        this->SpeedGain = SpeedGain;
    }
    //!
    //! \brief SetSpeedGainDeviation
    //! \param SpeedGainDev
    //!
    void SetSpeedGainDeviation(double SpeedGainDev)
    {
        this->SpeedGainDeviation = SpeedGainDev;
    }
    //!
    //! \brief SetKeepRight
    //! \param KeepRight
    //!
    void SetKeepRight(double KeepRight)
    {
        this->KeepRight = KeepRight;
    }
    //!
    //! \brief SetKeepRightDeviation
    //! \param KeepRightDev
    //!
    void SetKeepRightDeviation(double KeepRightDev)
    {
        this->KeepRightDeviation = KeepRightDev;
    }
    //!
    //! \brief SetCooperative
    //! \param Cooperative
    //!
    void SetCooperative(double Cooperative)
    {
        this->Cooperative = Cooperative;
    }
    //!
    //! \brief SetCooperativeDeviation
    //! \param CooperativeDev
    //!
    void SetCooperativeDeviation(double CooperativeDev)
    {
        this->CooperativeDeviation = CooperativeDev;
    }
    //!
    //! \brief SetVehicleParameters
    //! \param vehicleParameters
    //!
    void SetVehicleParameters (const agentParameters *vehicleParameters)
    {
        this->vehicleParameters = vehicleParameters;
    }
    //!
    //! \brief InitialValuesLogged
    //!
    //! \return If initial values are logged
    //!
    bool InitialValuesLogged()
    {
        return Logged;
    }

    //!
    //! \brief AccTargetBrake
    //!
    //! brake constant to stop in given distance from given velocity
    //!
    //! \param v    start velocity
    //! \param ds   target distance
    //! \return     acceleration to brake in distance to vtarget=0
    //!
    double AccTargetBrake(double v, double ds)
    {
        return (- v*v / (2*ds));
    }

    //!
    //! \brief NextRealBrakeVelocity
    //! \param a
    //! \param b
    //! \param c
    //! \param t
    //! \return
    //!
    double NextRealBrakeVelocity(double a, double b, double c, double t)
    {
        return a*pow(t,4) - b*t*t + c;
    }

private:

    int CycleTime = 100;
    StochasticsInterface *stochastics;
    TargetBraking targetBraking;
    ObservationInterface *observation;

    //!
    ActionDeduction_Input *AD_Input;
    //!
    std::map<RelationType,SurroundingMovingObjectsData> *NearTraffic;
    //!
    state *State;
    //!
    state_Ego *State_Ego;
    //!
    driverInformation *DriverInformation;

    //!
    bool Logged = false;

    ActionDeduction_Output_TD AD_Output_TD;
    //!mean of lognormal-distribution for the wish-netto-timegap between leader and ego in s
    double tGapWish;
    //! minimal comfortable deceleration
    double minComfortDeceleration = -4;
    //! maximal comfortable acceleration
    double maxComfortAcceleration = 3;
    //! deviation value for the stochastical setting
    double comfortAccelDeviation = 0.5;

    //! Lanechangemodel Parameters from SUMO

    //! The eagerness for performing lane changing to gain speed, normal-distributed with config-input "SpeedGain" and "SpeedGainDeviation", while < 0 -> repeated
    double SpeedGain = 1;
    //! The eagerness for following the obligation to keep right, normal-distributed with config-input "KeepRight" and "KeepRightDeviation", while < 0 -> repeated
    double KeepRight = 1;
    //! Willingness for performing cooperative lane changing, normal-distributed with config-input "Cooperative" and "CooperativeDeviation", while < 0 and > 1 -> repeated
    double Cooperative = 0.1;
    //!
    double SpeedGainDeviation = 0;
    //!
    double KeepRightDeviation = 0;
    //!
    double CooperativeDeviation = 0;

    //!
    bool targetbrakestarted = false;
    //!
    double t_brk = 0;
    //!
    double t_a = 0;
    //!
    double a = 0;
    //!
    double b = 0;
    //!
    double c = 0;

    //! Current Lane-change wish status
    LaneChangeState LaneChangeWish = LaneChangeState::NoLaneChange;
    //! The agents Car-following-model
    CFModel *CarFollowingModel;
    //! The agents lane-change-model
    AbstractLaneChangeModel *LaneChangeModel;

    //! Lane changing status
    bool IsChangingLanes;
    //! Lane changing status of the last time-step
    LaneChangeState LastSavedChangeStatus;
    //! Last lane-change status in the right direction
    int savedstateright;
    //! Last lane-change status in the right direction
    int savedstateleft;
    //! Last time-steps lateral displacement
    double last_lat_displacement_target = 0;
    //! lane-change distance-trigger for smooth lane-change
    double trigger = 0;
    //!
    int lastStepLaneId;
    //! Current lane-change state
    LaneChangeState LCState;

    //! Calulaction parameters for the lateral acceleration wish [m/s].
    struct LateralDynamicConstants
    {
        //! Typical acceleration for a lane change [m/s].
        const double lateralAcceleration = 1.5;
        //! Typical lateral damping for a lane change [m/s].
        const double zeta = 1.0;
        //! aggressiveness of the controller for heading errors
        const double gainHeadingError = 7.5;
    };

    //! parameters for the lateral acceleration wish [m/s]
    const LateralDynamicConstants lateralDynamicConstants;
    //!
    const agentParameters *vehicleParameters;

};


