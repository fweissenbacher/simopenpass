/******************************************************************************
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
* Copyright (c) 2021 HLRS, Universitaet Stuttgart
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  AlgorithmMentalModel.h
//! @author  Christian Siebke
//! @author  Konstantin Blenz
//! @author  Christian Gärber
//! @author  Vincent   Adam
//! @date    Tue, 03.12.2019
//! @brief provide data and calculate driver's extrapolation of the environment.
//!
//! This class contains the data calculations.
//! Singleton such that data calculations is generated only once and not seperately
//! for each instance.
//!
//-----------------------------------------------------------------------------

#pragma once

//#include <vector>
#include "../../Sensor_Modular_Driver/src/Container/ContainerStructures.h"
#include <include/stochasticsInterface.h>

//! \brief This class conains the mental representation and extrapolation of the environment
//!
//! \ingroup Algorithm_Modular_Driver
//! \defgroup Algorithm_Mental_Model


//! \ingroup Algorithm_Mental_Model
class MentalModel
{
public:

    MentalModel(StochasticsInterface *stochastics);

    //!
    //! \brief SetMentalModel_Input
    //!
    //! Update the Input Containers
    //!
    //! \param Input_BU Input_Bottom_Up Container
    //! \param Input_TD Input_Top_Down Container
    //!
    void SetMentalModel_Input(MentalModel_Input_BU *Input_BU, MentalModel_Input_TD *Input_TD);

    //!
    //! \brief GetMentalModel_Output
    //!
    //! Update the Output Container
    //!
    //! \param MM_Output
    //!
    void GetMentalModel_Output(MentalModel_Output *MM_Output);

    //!
    //! \brief Update
    //!
    //! Update the recognized surrounding agents
    //! If agent wasn't recognized before, he is added
    //! If agent was recognized and is still, he is updated
    //! If agent was recognized and now he isn't, he is extrapolated for a memorytime
    //!
    //! \param _inperceptionData    Input Container
    //! \param _roadPerceptionData  Input Container with road information
    //! \param in_cycletime         Current schedule time
    //!
    void Update(const std::list<SurroundingMovingObjectsData> &_inperceptionData, const StaticEnvironmentData &_roadPerceptionData,int in_cycletime);

    void Misjudge();

    //!
    //! \brief MisjudgeDistance
    //!
    //! Misjudge the current position of the agent by a relative value to the current velocity depending on the parameter relative_position_error.
    //! Sets the relative error-value to the agent-representation of the agent.
    //! Calls the function "MisjudgePosition()".
    //!
    //!  \param currAgent
    //!
    void MisjudgeDistance(std::list<std::unique_ptr<AgentRepresentation>>::const_iterator &currAgent);

    //!
    //! \brief MisjudgeVelocity
    //!
    //! Misjudge the current velocity of the agent by a relative value to the current velocity depending on the parameter relative_velocity_error.
    //! Sets the relative error-value to the agent-representation of the agent.
    //! Calls the function "MisjudgeVelocity()".
    //!
    //!  \param currAgent
    //!
    void MisjudgeVelocity(std::list<std::unique_ptr<AgentRepresentation>>::const_iterator &currAgent);

    //!
    //! \brief MisjudgeAcceleration
    //!
    //! Misjudge the current acceleration of the agent by a relative value to the current acceleration depending on the parameter relative_acceleration_error.
    //! Sets the relative error-value to the agent-representation of the agent.
    //! Calls the function "MisjudgeAcceleration()".
    //!
    //!  \param currAgent
    //!
    void MisjudgeAcceleration(std::list<std::unique_ptr<AgentRepresentation>>::const_iterator &currAgent);

    //!
    //! \brief GetSurroundingAgents
    //!
    //! Returns the recognized and remembered surrounding agents
    //!
    //! \return the recognized and remembered surrounding agents
    //!
    const std::list <std::unique_ptr <AgentRepresentation>> &GetSurroundingAgents() const
    {
         return   _surrounding_Agents;
    }

    //!
    //! \brief SetRelativeVelocityError
    //!
    //! Sets the relative-error-value to the current velocity, which will set an offset to the detected velocity relatively
    //!
    //!  \param rel_vel_error
    //!
    void SetRelativeVelocityError(double rel_vel_error)
    {
        relative_velocity_error = rel_vel_error;
    }
    //!
    //! \brief SetRelativePositionError
    //!
    //! Sets the relative-error-value to the current velocity, which will set an offset to the detected road-position (s) relatively to the velocity
    //!
    //!  \param rel_pos_error
    //!
    void SetRelativePositionError(double rel_pos_error)
    {
        relative_position_error = rel_pos_error;
    }

    //!
    //! \brief SetRelativeAccelerationError
    //!
    //! Sets the relative-error-value to the current acceleration, which will set an offset to the detected acceleration relatively
    //!  \param rel_acc_error
    //!
    void SetRelativeAccelerationError(double rel_acc_error)
    {
        relative_acceleration_error = rel_acc_error;
    }

private:

    MentalModel_Input_BU *MM_Input_BU;
    MentalModel_Input_TD *MM_Input_TD;
    StochasticsInterface *_stochastic;

    /*!
    \brief "forget-process of the human"
     *
     * agents that have not been fixated since a long time will be deleted (forgotten)
     * memory time is exceeded
     *
     * @param[in] mentalmodel_agent  iterator of the MentalModal_Agents
     * @param[in] in_perceptionData   perceived information of fixed agent
     */
    bool Forget(std::list<std::unique_ptr<AgentRepresentation>>::const_iterator &mentalmodel_agent_iter, const std::list<SurroundingMovingObjectsData> &in_perceptionData);

     /*!
     * \brief "human memory capacity "
     * memory capacity is exeded
     *
     * If the agent capacity is exeeded the oldest agents are deleted.
     *
     */
     void Memory_capacity_exceeded ();

     //! standard deviation of the relative error of the velocity recognition depending on the current velocity
     double relative_velocity_error = 0;
     //! standard deviation of the relative error of the position recognition depending on the current velocity
     double relative_position_error = 0;
     //! standard deviation of the relative error of the acceleration recognition depending on the current acceleration
     double relative_acceleration_error = 0;

 // mental model memory

     //! contains all  surrouded representation agents of the MentalModel
     std::list <std::unique_ptr<AgentRepresentation>> _surrounding_Agents;


 // memory variables
     //! maximum memory capacity of agent representations
     unsigned int number_of_agent_representations = 10;

     //! time, till agent representation will be forgotten, not yet representative [ in ms]
     int memorytime  = 10000;  //ms



};


