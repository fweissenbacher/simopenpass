/*******************************************************************************
* Copyright (c) 2019 AMFD GmbH
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

//-----------------------------------------------------------------------------
//! @file  AlgorithmActionExecution.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief This file contains the calculations for the action-execution
//!
//! This class contains the data calculations for the execution of actions from the previous cognition.
//! Singleton such that data calculations is generated only once and not seperately
//! for each instance.
//!
//-----------------------------------------------------------------------------

#pragma once

//#include <vector>
#include "../../Sensor_Modular_Driver/src/Container/ContainerStructures.h"
#include "../../Algorithm_Longitudinal/src/longCalcs.h"
#include "include/stochasticsInterface.h"
#include "agentParameters.h"

//! \brief This class contains the data calculations for the execution of actions from the previous cognition.
//! Singleton such that data calculations is generated only once and not seperately
//! for each instance.
//!
//! \ingroup Algorithm_Modular_Driver
//! \defgroup Algorithm_Action_Execution

//!
//! \ingroup Algorithm_Action_Execution
//!
class ActionExecution
{
public:

    ActionExecution(StochasticsInterface *stochastics); // avoid access of constructor of singleton outside class
    //!
    //! \brief SetActionExecution_Input
    //! \param Input
    //!
    void SetActionExecution_Input (ActionExecution_Input *Input);
    //!
    //! \brief GetActionExecution_Output_TD
    //! \return
    //!
    ActionExecution_Output_TD * GetActionExecution_Output_TD();

    //!
    //! \brief CalculatePedalPosAndGear
    //! \param AE_Output_BU
    //!
    void CalculatePedalPosAndGear(ActionExecution_Output_BU *AE_Output_BU);
    //!
    //! \brief GetPedalChangeTime
    //! \return
    //!
    double GetPedalChangeTime();
    //!
    //! \brief IsPedalChangeFinished
    //! \param pedalChangeTime
    //! \param cycleTime
    //! \return
    //!
    bool IsPedalChangeFinished(double pedalChangeTime, double cycleTime);

    //-----------------------------------------------------------------------------
    /*!
    * \brief Returns the lateral acceleration wish.
    *
    * \details Returns the lateral acceleration wish.
    *
    * @param[in]    DeltaY  Lateral displacement.
    *
    * @return       lateral acceleration wish.
    */
    //-----------------------------------------------------------------------------
    double ALatWish(double DeltaW);

    //-----------------------------------------------------------------------------
    /*!
    * \brief Returns the desired curvature.
    *
    * \details Returns the desired curvature.
    *
    * @param[in]    accLatTarget         Desired acceleration.
    *
    * @return       Desired curvature.
    */
    //-----------------------------------------------------------------------------
    double CurvatureTarget(double accLatTarget);

    //!
    //! \brief CalculateSteeringWheelAngle
    //! \param AE_Output_BU
    //! \param time
    //!
    void CalculateSteeringWheelAngle(ActionExecution_Output_BU *AE_Output_BU, int time);
    //!
    //! \brief SetIndicatorStateDirection
    //! \param AE_Output_BU
    //!
    void SetIndicatorStateDirection(ActionExecution_Output_BU *AE_Output_BU);
    //!
    //! \brief SetVehicleParameters
    //! \param vehicleParameters
    //!
    void SetVehicleParameters(const agentParameters *vehicleParameters)
    {
        this->vehicleParameters = vehicleParameters;
    }

private:

    ActionExecution_Input *AE_Input;
    ActionExecution_Output_TD AE_Output_TD;

    // --- module internal variables
    ObservationInterface* _observer = nullptr;

    // Stochastics
    StochasticsInterface* _stochastic;

    // --- module internal variables
    //  --- Inputs
    //! Current lateral deviation regarding trajectory [m].
    double in_lateralDeviation = 0;
    //! Gain for lateral deviation controller [-].
    double in_gainLateralDeviation = 20.0;
    //! Current heading error regarding trajectory [rad].
    double in_headingError = 0;
    //! Gain for heading error controller [-].
    double in_gainHeadingError = 7.5;
    //! Set value for trajectory curvature [1/m].
    double in_kappaSet = 0;
    //! current velocity
    double velocity = 0.0;
    //! current angle of the steering wheel
    double steeringWheelAngle = 0.0;

    //  --- Outputs
    //! The steering wheel angle wish of the driver in degree.
    double out_desiredSteeringWheelAngle{0};
    /** @} @} */

    //  --- Internal Parameters

    //! Helper constant to convert radiant into degree.
    const double RadiantToDegree = 57.295779513082320876798154814105;
    /** @} @} */

    //!
    bool isActive{false};

    //! Previous scheduling time (for calculation of cycle time lenght).
    int timeLast{-100};

    //!
    double pedalchangetime;

    const VehicleModelParameters *vehicleParameters;

};


