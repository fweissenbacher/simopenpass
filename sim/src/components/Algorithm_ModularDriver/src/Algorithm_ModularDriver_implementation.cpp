/******************************************************************************
* Copyright (c) 2017, 2018, 2019 in-tech GmbH
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  Algorithm_ModularDriver_implementation.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief Represents the cognitive processes of the driver from information acquisition to action excecution.
//!
//! This class contains the data calculations.
//! Singleton such that data calculations is generated only once and not seperately
//! for each instance.
//!
//-----------------------------------------------------------------------------

/**
 * @defgroup al_mod_driver Algorithm Modular Driver
 *
 * The calculation of all mental aspects of an agent from the acquisition of surrounding informations, their mental representation and the derivation of actions
 *
 * \image html ModularDriver.png "Structure of the modular driver"
 *
 */

#include <QtGlobal>

#include "Algorithm_ModularDriver_implementation.h"
#include "common/lateralSignal.h"
#include "common/secondaryDriverTasksSignal.h"
#include "../../Sensor_Modular_Driver/src/Signals/complexsignals.cpp"
#include "common/steeringSignal.h"
#include "common/vectorSignals.h"
#include "common/longitudinalSignal.h"
#include "common/accelerationSignal.h"
#include "common/parametersVehicleSignal.h"
#include <list>
#include "include/parameterInterface.h"

#include <cassert>
#include <memory>
#include <qglobal.h>


AlgorithmModularDriverImplementation::AlgorithmModularDriverImplementation(
        std::string componentName,
        bool isInit,
        int priority,
        int offsetTime,
        int responseTime,
        int cycleTime,
        StochasticsInterface *stochastics,
        WorldInterface *world,
        const ParameterInterface *parameters,
        PublisherInterface *const publisher,
        const CallbackInterface *callbacks,
        AgentInterface *agent) :
        AlgorithmInterface(
            componentName,
            isInit,
            priority,
            offsetTime,
            responseTime,
            cycleTime,
            stochastics,
            parameters,
            publisher,
            callbacks,
            agent),
        informationacquisition(stochastics, /*GetObservations()->at(0)*/nullptr),
        mentalmodel(stochastics),
        situationassessment(stochastics, cycleTime, /*GetObservations()->at(0)*/nullptr),
        actiondeduction(cycleTime, stochastics, /*GetObservations()->at(0)*/nullptr),
        actionexecution(stochastics)
{
    //!!!
//    try
//    {
//        observer = GetObservations()->at(0);
//        if (observer == nullptr)
//        {
//            throw std::runtime_error("");
//        }
//    }
//    catch (...)
//    {
//        const std::string msg = COMPONENTNAME + " invalid observation module setup";
//        LOG(CbkLogLevel::Error, msg);
//        throw std::runtime_error(msg);
//    }
    //!!!

     /**
     * @ingroup al_mod_driver
     * @defgroup params Parameters
     * * Parameters from the SystemConfiguration-XML are set to the driver model
     *
     *  *
     * \section Sensor_Modular_Driver_ConfigParameters Parameters to be specified in SystemConfiguration.xml
     *
     * name | id | unit | distribution | meaning | space
     * ----------        | -----------        | -----------        | -----------        | -----------        | -----------
     * AvertViewTimeMean	|	0	|	ms	|	Log-Normalverteilung	|	Mittlere Dauer der Blickabwendung bis zur nächsten Aktualisierung der Umgebungsinformationen (Agenten, Umwelt) à Informationen werden in diesem Zeitraum mental extrapoliert	|	0 ... 5 000
     * AvertViewTimeDeviation	|	1	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu AvertViewTimeMean 	|	0 … 1 000
     * ReactionTimeMean	|	2	|	ms	|	Log-Normalverteilung	|	Reaktionszeit bei Reaktion auf eine neue Verkehrssituation (aktuell neuer „Leader“, „Follower“ je auf allen Fahrstreifen)	|	0 … 1 000
     * ReactionTimeDeviation	|	3	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu ReactionTimeMean	|	0 … 100
     * MaxComfortAcceleration	|	4	|	m/s²	|	Normalverteilung	|	Maximale Beschleunigung bei Normalfahrt (Mittelwert)	|	1 ... 5
     * MinComfortAcceleration	|	5	|	m/s²	|	Normalverteilung	|	Minimale Verzögerung bei Normalfahrt (Mittelwert)	|	-5 … -0.5
     * ComfortAccelerationDeviation	|	6	|	m/s²	|	Normalverteilung	|	Standardabweichung zu MaxComfortAcceleration & MinComfortAcceleration	|	0 … 1
     * VelocityWish	|	7	|	km/h	|	Log-Normalverteilung	|	Mittlere Wunschgeschwindigkeit	|	0 … 300
     * VelocityWishDeviation	|	8	|	km/h	|	Log-Normalverteilung	|	Standardabweichung zu VelocityWish	|	0 … 60
     * TGapWish	|	9	|	s	|	Log-Normalverteilung	|	Wunschzeitlücke zu einem Vorderfahrzeug	|	0.2 … 3
     * TGapWishDeviation	|	10	|	s	|	Log-Normalverteilung	|	Standardabweichung zu TGapWish	|	0 … 1
     * MeanSpeedLimitViolation	|	11	|	km/h	|	Normalverteilung	|	Mittlerer Aufschlag auf den Geschwindigkeitswunsch bei vorhandenem Tempolimit 	|	0 …20
     * MeanSpeedLimitViolationDeviation	|	12	|	km/h	|	Normalverteilung	|	Standardabweichung zu MeanSpeedLimitViolation	|	0 … 10
     * MinDistance	|	13	|	m	|	-	|	Mindestabstand zum Vorderfahrzeug	|	0 … 2
     * SpeedGain	|	14	|	-	|	Normalverteilung	|	Der mittlere Drang, Spurwechsel durchzuführen, um Geschwindigkeit zu gewinnen. Höhere Werte führen zu mehr Spurwechseln	|	0 … ∞
     * SpeedGainDeviation	|	15	|	-	|	Normalverteilung	|		|	0 … ∞
     * KeepRight	|	16	|	-	|	Normalverteilung	|	Der mittlere Drang, dem Rechtsfahrgebot nachzukommen. Höhere Werte führen zu einem früheren Spurwechsel	|	0 … ∞
     * KeepRightDeviation	|	17	|	-	|	Normalverteilung	|		|	0 … ∞
     * Cooperative	|	18	|	-	|	Normalverteilung	|	Die Bereitschaft zum kooperativen Spurwechsel. Niedrigere Werte führen zu verminderter Kooperation	|	0 … 1
     * CooperativeDeviation	|	19	|	-	|	Normalverteilung	|		|	0 … 1
     * CriticalityLogging	|	20	|		|		|	Einschalten des Loggings von „ttc“, „thw“ bzw. „speeding“	| -
     * TtcThreshold_mean	|	21	|	s	|	Log-Normalverteilung	|	Grenze unterhalb derer der Agent vom Komfort-Verzögerungspotential in das Notbremspotential wechselt	|	0 … ∞
     * TtcThreshold_std	|	22	|	s	|	Log-Normalverteilung	|	-	| -
     * ViewTimeMean	|	23	|	ms	|	Log-Normalverteilung	|	Mittlere Dauer der Blickzuwendung bis zur nächsten Blickabwendung	|	0 … ∞
     * ViewTimeDeviation	|	24	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu ViewTimeMean 	|	0 … ∞
     * ForwardViewTimeMean  |   25  |   ms  |   Log-Normalverteilung    |   Mittlere Blickzuwendung in Fahrtrichtung    |   0 ... ∞
     * ForwardViewTimeDeviation	|	26	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu ForwardViewTimeMean 	|	0 … ∞
     * BackwardViewTimeMean  |   27  |   ms  |   Log-Normalverteilung    |   Mittlere Blickzuwendung entgegen der Fahrtrichtung    |   0 ... ∞
     * BackwardViewTimeDeviation	|	28	|	ms	|	Log-Normalverteilung	|	Standardabweichung zu BackwardViewTimeMean 	|	0 … ∞
     * RelativePositionError	|	29	|	ms	|	Normalverteilung	|	Standardabweichung der relativen Abweichung der erkannten Position eines anderen Agenten bezogen auf die aktuelle Geschwindigkeit 	|	0 … 0.2
     * RelativeVelocityError	|	30	|	ms	|	Normalverteilung	|	Standardabweichung der relativen Abweichung der erkannten Geschwindigkeit eines anderen Agenten bezogen auf die aktuelle Geschwindigkeit 	|	0 … 0.2
     * RelativeAcclerationError	|	31	|	ms	|	Normalverteilung	|	Standardabweichung der relativen Abweichung der erkannten Beschleunigung eines anderen Agenten bezogen auf die aktuelle Beschleunigung 	|	0 … 0.2
     *
     */

    if (parameters->GetParametersDouble().count("VelocityWish") > 0 || parameters->GetParametersDouble().count("7") > 0)
    {
        double VelocityWish = (parameters->GetParametersDouble().count("VelocityWish") > 0 ? parameters->GetParametersDouble().at("VelocityWish") : parameters->GetParametersDouble().at("7"));
        situationassessment.SetVWish(1/3.6 * VelocityWish);
    }

    if (parameters->GetParametersDouble().count("VelocityWishDeviation") > 0 || parameters->GetParametersDouble().count("8"))
    {
        double VelocityWishDeviation = (parameters->GetParametersDouble().count("VelocityWishDeviation") > 0 ? parameters->GetParametersDouble().at("VelocityWishDeviation") : parameters->GetParametersDouble().at("8"));
        situationassessment.SetVWishDeviation(1/3.6 * VelocityWishDeviation);
    }
    if (parameters->GetParametersDouble().count("TGapWish") > 0 || parameters->GetParametersDouble().count("9") > 0)
    {
        double TGapWish = (parameters->GetParametersDouble().count("TGapWish") > 0 ? parameters->GetParametersDouble().at("TGapWish") : parameters->GetParametersDouble().at("9"));
        situationassessment.SetTGapWish(TGapWish);
        actiondeduction.SetTGapWish(TGapWish);
    }
    if (parameters->GetParametersDouble().count("TGapWishDeviation") > 0 || parameters->GetParametersDouble().count("10") > 0)
    {
        double TGapWishDeviation = (parameters->GetParametersDouble().count("TGapWishDeviation") > 0 ? parameters->GetParametersDouble().at("TGapWishDeviation") : parameters->GetParametersDouble().at("10"));
        situationassessment.SetTGapWishDeviation(TGapWishDeviation);
    }
    if (parameters->GetParametersDouble().count("MaxComfortAcceleration") > 0 || parameters->GetParametersDouble().count("4") > 0)
    {
        double MaxComfortAcceleration = (parameters->GetParametersDouble().count("MaxComfortAcceleration") > 0 ? parameters->GetParametersDouble().at("MaxComfortAcceleration") : parameters->GetParametersDouble().at("4"));
        actiondeduction.SetMaxComfortAccel(MaxComfortAcceleration);
    }
    if (parameters->GetParametersDouble().count("MinComfortDeceleration") > 0 || parameters->GetParametersDouble().count("5") > 0)
    {
        double MinComfortDeceleration = (parameters->GetParametersDouble().count("MinComfortDeceleration") > 0 ? parameters->GetParametersDouble().at("MinComfortDeceleration") : parameters->GetParametersDouble().at("5"));
        MinComfortDeceleration = MinComfortDeceleration <= 0 ? MinComfortDeceleration : MinComfortDeceleration * -1.;  // TODO Adjust to deceleration as absolute value
        actiondeduction.SetMinComfortDecel(MinComfortDeceleration);
    }
    if (parameters->GetParametersDouble().count("ComfortAccelerationDeviation") > 0 || parameters->GetParametersDouble().count("6") > 0)
    {
        double ComfortAccelerationDeviation = (parameters->GetParametersDouble().count("ComfortAccelerationDeviation") > 0 ? parameters->GetParametersDouble().at("ComfortAccelerationDeviation") : parameters->GetParametersDouble().at("6"));
        actiondeduction.SetComfortAccelDev(ComfortAccelerationDeviation);
    }
    if (parameters->GetParametersDouble().count("MeanSpeedLimitViolation") > 0 || parameters->GetParametersDouble().count("11") > 0)
    {
        double MeanSpeedLimitViolation = (parameters->GetParametersDouble().count("MeanSpeedLimitViolation") > 0 ? parameters->GetParametersDouble().at("MeanSpeedLimitViolation") : parameters->GetParametersDouble().at("11"));
        situationassessment.SetSpeedLimit_Violation(1/3.6 * MeanSpeedLimitViolation);
    }
    if (parameters->GetParametersDouble().count("MeanSpeedLimitViolationDeviation") > 0 || parameters->GetParametersDouble().count("12") > 0)
    {
        double MeanSpeedLimitViolationDeviation = (parameters->GetParametersDouble().count("MeanSpeedLimitViolationDeviation") > 0 ? parameters->GetParametersDouble().at("MeanSpeedLimitViolationDeviation") : parameters->GetParametersDouble().at("12"));
        situationassessment.SetSpeedLimit_ViolationDeviation(1/3.6 * MeanSpeedLimitViolationDeviation);
    }
    if (parameters->GetParametersDouble().count("MinDistance") > 0 || parameters->GetParametersDouble().count("13") > 0)
    {
        double MinDistance = (parameters->GetParametersDouble().count("MinDistance") > 0 ? parameters->GetParametersDouble().at("MinDistance") : parameters->GetParametersDouble().at("13"));
        situationassessment.SetMinDistance(MinDistance);
    }
    if (parameters->GetParametersDouble().count("SpeedGain") > 0 || parameters->GetParametersDouble().count("14") > 0)
    {
        double SpeedGain = (parameters->GetParametersDouble().count("SpeedGain") > 0 ? parameters->GetParametersDouble().at("SpeedGain") : parameters->GetParametersDouble().at("14"));
        actiondeduction.SetSpeedGain(SpeedGain);
    }
    if (parameters->GetParametersDouble().count("SpeedGainDeviation") > 0 || parameters->GetParametersDouble().count("15") > 0)
    {
        double SpeedGainDeviation = (parameters->GetParametersDouble().count("SpeedGainDeviation") > 0 ? parameters->GetParametersDouble().at("SpeedGainDeviation") : parameters->GetParametersDouble().at("15"));
        actiondeduction.SetSpeedGainDeviation(SpeedGainDeviation);
    }
    if (parameters->GetParametersDouble().count("KeepRight") > 0 || parameters->GetParametersDouble().count("16") > 0)
    {
        double KeepRight = (parameters->GetParametersDouble().count("KeepRight") > 0 ? parameters->GetParametersDouble().at("KeepRight") : parameters->GetParametersDouble().at("16"));
        actiondeduction.SetKeepRight(KeepRight);
    }
    if (parameters->GetParametersDouble().count("KeepRightDeviation") > 0 || parameters->GetParametersDouble().count("17") > 0)
    {
        double KeepRightDeviation = (parameters->GetParametersDouble().count("KeepRightDeviation") > 0 ? parameters->GetParametersDouble().at("KeepRightDeviation") : parameters->GetParametersDouble().at("17"));
        actiondeduction.SetKeepRightDeviation(KeepRightDeviation);
    }
    if (parameters->GetParametersDouble().count("Cooperative") > 0 || parameters->GetParametersDouble().count("18") > 0)
    {
        double Cooperative = (parameters->GetParametersDouble().count("Cooperative") > 0 ? parameters->GetParametersDouble().at("Cooperative") : parameters->GetParametersDouble().at("18"));
        actiondeduction.SetCooperative(Cooperative);
    }
    if (parameters->GetParametersDouble().count("CooperativeDeviation") > 0 || parameters->GetParametersDouble().count("19") > 0)
    {
        double CooperativeDeviation = (parameters->GetParametersDouble().count("CooperativeDeviation") > 0 ? parameters->GetParametersDouble().at("CooperativeDeviation") : parameters->GetParametersDouble().at("19"));
        actiondeduction.SetCooperativeDeviation(CooperativeDeviation);
    }
    if (parameters->GetParametersDouble().count("TtcThresholdMean") > 0 || parameters->GetParametersDouble().count("21") > 0)
    {
        double ttc_threshold_mean = (parameters->GetParametersDouble().count("TtcThresholdMean") > 0 ? parameters->GetParametersDouble().at("TtcThresholdMean") : parameters->GetParametersDouble().at("21"));
        situationassessment.SetTtcThresholdMean(ttc_threshold_mean);
    }
    if (parameters->GetParametersDouble().count("TtcThresholdStd") > 0 || parameters->GetParametersDouble().count("22") > 0)
    {
        double ttc_threshold_std = (parameters->GetParametersDouble().count("TtcThresholdStd") > 0 ? parameters->GetParametersDouble().at("TtcThresholdStd") : parameters->GetParametersDouble().at("22"));
        situationassessment.SetTtcThresholdStd(ttc_threshold_std);
    }
    if (parameters->GetParametersInt().count("AvertViewTimeMean") > 0 || parameters->GetParametersInt().count("0") > 0)
    {
        int AvertViewTimeMean = (parameters->GetParametersInt().count("AvertViewTimeMean") > 0 ? parameters->GetParametersInt().at("AvertViewTimeMean") : parameters->GetParametersInt().at("0"));
        informationacquisition.SetMeanAvertViewTime(AvertViewTimeMean);
    }
    if (parameters->GetParametersInt().count("AvertViewTimeDeviation") > 0 || parameters->GetParametersInt().count("1") > 0)
    {
        int AvertViewTimeDeviation = (parameters->GetParametersInt().count("AvertViewTimeDeviation") > 0 ? parameters->GetParametersInt().at("AvertViewTimeDeviation") : parameters->GetParametersInt().at("1"));
        informationacquisition.SetAvertViewTimeDeviation(AvertViewTimeDeviation);
    }
    if (parameters->GetParametersInt().count("ReactionTimeMean") > 0 || parameters->GetParametersInt().count("2") > 0)
    {
        int ReactionTimeMean = (parameters->GetParametersInt().count("ReactionTimeMean") > 0 ? parameters->GetParametersInt().at("ReactionTimeMean") : parameters->GetParametersInt().at("2"));
        situationassessment.SetMeanReactionTime(ReactionTimeMean);
    }
    if (parameters->GetParametersInt().count("ReactionTimeDeviation") > 0 || parameters->GetParametersDouble().count("3") > 0)
    {
        int ReactionTimeDeviation = (parameters->GetParametersInt().count("ReactionTimeDeviation") > 0 ? parameters->GetParametersInt().at("ReactionTimeDeviation") : parameters->GetParametersInt().at("3"));
        situationassessment.SetReactionTimeDeviation(ReactionTimeDeviation);
    }
    if (parameters->GetParametersInt().count("MinReactionTime") > 0)
    {
        int MinReactionTime = parameters->GetParametersInt().at("MinReactionTime");
        situationassessment.SetMinReactionTime(MinReactionTime);
    }
    if (parameters->GetParametersString().count("CriticalityLogging") > 0 || parameters->GetParametersString().count("20") > 0)
    {
        std::string CriticalityLogging = (parameters->GetParametersString().count("CriticalityLogging") > 0 ? parameters->GetParametersString().at("CriticalityLogging") : parameters->GetParametersString().at("20"));
        situationassessment.SetLoggingGroups(CriticalityLogging);
    }
    if (parameters->GetParametersInt().count("ViewTimeMean") > 0 || parameters->GetParametersInt().count("23") > 0)
    {
        double view_time_mean = (parameters->GetParametersInt().count("ViewTimeMean") > 0 ? parameters->GetParametersInt().at("ViewTimeMean") : parameters->GetParametersInt().at("23"));
        informationacquisition.SetViewTimeMean(view_time_mean);
    }
    if (parameters->GetParametersInt().count("ViewTimeDeviation") > 0 || parameters->GetParametersInt().count("24") > 0)
    {
        double view_time_dev = (parameters->GetParametersInt().count("ViewTimeDeviation") > 0 ? parameters->GetParametersInt().at("ViewTimeDeviation") : parameters->GetParametersInt().at("24"));
        informationacquisition.SetViewTimeDeviation(view_time_dev);
    }
    if (parameters->GetParametersInt().count("ForwardViewTimeMean") > 0 || parameters->GetParametersInt().count("25") > 0)
    {
        double forward_view_time_mean = (parameters->GetParametersInt().count("ForwardViewTimeMean") > 0 ? parameters->GetParametersInt().at("ForwardViewTimeMean") : parameters->GetParametersInt().at("25"));
        informationacquisition.SetForwardViewTime(forward_view_time_mean);
    }
    if (parameters->GetParametersInt().count("ForwardViewTimeDeviation") > 0 || parameters->GetParametersInt().count("26") > 0)
    {
        double forward_view_time_dev = (parameters->GetParametersInt().count("ForwardViewTimeDeviation") > 0 ? parameters->GetParametersInt().at("ForwardViewTimeDeviation") : parameters->GetParametersInt().at("26"));
        informationacquisition.SetForwardViewTimeDeviation(forward_view_time_dev);
    }
    if (parameters->GetParametersInt().count("BackwardViewTimeMean") > 0 || parameters->GetParametersInt().count("27") > 0)
    {
        double backward_view_time_mean = (parameters->GetParametersInt().count("BackwardViewTimeMean") > 0 ? parameters->GetParametersInt().at("BackwardViewTimeMean") : parameters->GetParametersInt().at("27"));
        informationacquisition.SetBackwardViewTime(backward_view_time_mean);
    }
    if (parameters->GetParametersInt().count("BackwardViewTimeDeviation") > 0 || parameters->GetParametersInt().count("28") > 0)
    {
        double backward_view_time_dev = (parameters->GetParametersInt().count("BackwardViewTimeDeviation") > 0 ? parameters->GetParametersInt().at("BackwardViewTimeDeviation") : parameters->GetParametersInt().at("28"));
        informationacquisition.SetBackwardViewTimeDeviation(backward_view_time_dev);
    }
    if (parameters->GetParametersDouble().count("RelativePositionError") > 0 || parameters->GetParametersDouble().count("29") > 0)
    {
        double rel_pos_error = (parameters->GetParametersDouble().count("RelativePositionError") > 0 ? parameters->GetParametersDouble().at("RelativePositionError") : parameters->GetParametersDouble().at("29"));
        mentalmodel.SetRelativePositionError(rel_pos_error);
    }
    if (parameters->GetParametersDouble().count("RelativeVelocityError") > 0 || parameters->GetParametersDouble().count("30") > 0)
    {
        double rel_vel_error = (parameters->GetParametersDouble().count("RelativeVelocityError") > 0 ? parameters->GetParametersDouble().at("RelativeVelocityError") : parameters->GetParametersDouble().at("30"));
        mentalmodel.SetRelativePositionError(rel_vel_error);
    }
    if (parameters->GetParametersDouble().count("RelativeAcclerationError") > 0 || parameters->GetParametersDouble().count("31") > 0)
    {
        double rel_acc_error = (parameters->GetParametersDouble().count("RelativeAcclerationError") > 0 ? parameters->GetParametersDouble().at("RelativeAcclerationError") : parameters->GetParametersDouble().at("31"));
        mentalmodel.SetRelativePositionError(rel_acc_error);
    }

    initialisationAD = true;
    initialisationAE = true;
    initialisationIA = true;
    initialisationMM = true;
    initialisationSA = true;
}

AlgorithmModularDriverImplementation::~AlgorithmModularDriverImplementation()
{}

void AlgorithmModularDriverImplementation::UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time)
{

    /**
    * @ingroup al_mod_driver
    * @defgroup al_mod_input Input
    * Input variables:
    * name | meaning
    * -----|------
    * StaticEnvironment            | Container with the information about the static environment
    * Ego                          | Container with the information about the ego-vehicle
    * SurroundingMovingObjects     | Container with the information about the surrounding-vehicles
    *
   */

    if (localLinkId == 0)
    {
        const std::shared_ptr<structSignal<StaticEnvironmentData> const> signal = std::dynamic_pointer_cast<structSignal<StaticEnvironmentData> const>(data);
        if (!signal)
        {
            const std::string msg = COMPONENTNAME + " invalid signaltype";
            LOG(CbkLogLevel::Debug, msg);
            throw std::runtime_error(msg);
        }
        IA_I_BU.StaticEnvironment = signal->value;
    }
    else if (localLinkId == 1)
    {
        const std::shared_ptr<structSignal<egoData> const> signal = std::dynamic_pointer_cast<structSignal<egoData> const>(data);
        if (!signal)
        {
            const std::string msg = COMPONENTNAME + " invalid signaltype";
            LOG(CbkLogLevel::Debug, msg);
            throw std::runtime_error(msg);
        }
        IA_I_BU.EgoData = signal->value;
    }
    else if (localLinkId == 2)
    {
        const std::shared_ptr<structSignal<std::list<SurroundingMovingObjectsData>> const> signal = std::dynamic_pointer_cast<structSignal<std::list<SurroundingMovingObjectsData>> const>(data);
        if (!signal)
        {
            const std::string msg = COMPONENTNAME + " invalid signaltype";
            LOG(CbkLogLevel::Debug, msg);
            throw std::runtime_error(msg);
        }
        IA_I_BU.SurroundingMovingObjects = signal->value;
    }
    else if (localLinkId == 3)
    {
        if (!initializedVehicleModelParameters)
        {
            // from ParametersAgent
            const std::shared_ptr<ParametersVehicleSignal const> signal = std::dynamic_pointer_cast<ParametersVehicleSignal const>(data);
            if (!signal)
            {
                const std::string msg = COMPONENTNAME + " invalid signaltype";
                LOG(CbkLogLevel::Debug, msg);
                throw std::runtime_error(msg);
            }
            vehicleParameters = signal->vehicleParameters;
            vehicleParameters.maxDeceleration = vehicleParameters.maxDeceleration > 0 ? vehicleParameters.maxDeceleration*-1 : vehicleParameters.maxDeceleration; //GetAgent()->GetMaxDeceleration() * -1; // TODO Adjust to deceleration as absolute value
            //vehicleParameters.maxAcceleration = GetAgent()->GetMaxAcceleration();
            situationassessment.SetVehicleParameters(&vehicleParameters);
            actiondeduction.SetVehicleParameters(&vehicleParameters);
            actionexecution.SetVehicleParameters(&vehicleParameters);
            initializedVehicleModelParameters = true;
        }
    }
    else
    {
        const std::string msg = COMPONENTNAME + " invalid link";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
    }
    Q_UNUSED(localLinkId);
    Q_UNUSED(data);
    Q_UNUSED(time);
}

void AlgorithmModularDriverImplementation::UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time)
{

    /**
    * @ingroup al_mod_driver
    * @defgroup al_mod_output Output
    *
    * Output variables:
    * name | meaning
    * -----|------
    * out_accPedalPos    | Acceleration pedal position
    * out_brakePedalPos  | Brake pedal position
    * out_gear           | Chosen gear
    * out_indicatorState | Indicator state
    * out_hornSwitch     | Horn switch status
    * out_headLight      | Head light status
    * out_highBeamLight  | High beam light status
    * out_flasher        | Flasher status
    * out_desiredSteeringWheelAngle | Desired steering wheel angle
    *
   */

    Q_UNUSED(time);

    if(localLinkId == 0)
    {
        try
        {
            data = std::make_shared<LongitudinalSignal const>(
                        componentState,
                        out_accPedalPos,
                        out_brakePedalPos,
                        out_gear);
        }
        catch(const std::bad_alloc&)
        {
            const std::string msg = COMPONENTNAME + " could not instantiate signal";
            LOG(CbkLogLevel::Debug, msg);
            throw std::runtime_error(msg);
        }
    }
    else if(localLinkId == 1)
    {
        try
        {
            data = std::make_shared<SecondaryDriverTasksSignal const>(
                                                      out_indicatorState,
                                                      out_hornSwitch,
                                                      out_headLight,
                                                      out_highBeamLight,
                                                      out_flasher,
                                                      componentState);
        }
        catch(const std::bad_alloc&)
        {
            const std::string msg = COMPONENTNAME + " could not instantiate signal";
            LOG(CbkLogLevel::Debug, msg);
            throw std::runtime_error(msg);
        }
    }

    else if (localLinkId == 2)
    {
        try
        {
            data = std::make_shared<SteeringSignal const>(ComponentState::Acting, out_desiredSteeringWheelAngle);
        }
        catch(const std::bad_alloc&)
        {
            const std::string msg = COMPONENTNAME + " could not instantiate signal";
            LOG(CbkLogLevel::Debug, msg);
            throw std::runtime_error(msg);
        }
    }
    else if(localLinkId == 3)
    {
        try
        {
            data = std::make_shared<AccelerationSignal const>(componentState, out_longitudinal_acc);
        }
        catch(const std::bad_alloc&)
        {
            const std::string msg = COMPONENTNAME + " could not instantiate signal";
            LOG(CbkLogLevel::Debug, msg);
            throw std::runtime_error(msg);
        }
    }
    else
    {
        const std::string msg = COMPONENTNAME + " invalid link";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
    }
}

/** \addtogroup al_mod_driver
 */
void AlgorithmModularDriverImplementation::Trigger(int time)
{
    InformationAcquisition_Output IA_O = {};

    /** @ingroup al_mod_driver
     * @defgroup in_acq Information Acquisition
     * - Call of the function Information_Acquisition() with the pointer to the containers:
     *   * InformationAcquisition_Input_Top_Down IA_I_TD (empty yet)
     *   * InformationAcquisition_Input_Bottom_Up IA_I_BU <- set from the module Input
     *   * InformationAcquisition_Output IA_O
     */
    Information_Acquisition(&IA_I_TD, &IA_I_BU, &IA_O, time);

    /** @ingroup al_mod_driver
     * @defgroup ment_mod Mental Model
     * - Call of the function Mental_Model() with the pointer to the containers:
     *   * MentalModel_Input_Top_Down MM_I_TD (empty yet)
     *   * MentalModel_Input_Bottom_Up MM_I_BU <- set from IA_O
     *   * MentalModel_Output MM_O
     */
    MentalModel_Input_BU MM_I_BU;
    MM_I_BU.IA_O = &IA_O;
    MentalModel_Output MM_O;
    Mental_Model(&MM_I_TD, &MM_I_BU, &MM_O);

    /** @ingroup al_mod_driver
     * @defgroup sit_ass Situation Assessment
     * - Call of the function Situation_Assessment() with the pointer to the containers:
     *   * Situation_Assessment_Input_Top_Down SA_I_TD (empty yet)
     *   * Situation_Assessment_Input_Bottom_Up SA_I_BU <- set from MM_O
     *   * Situation_Assessment_Output_Bottom_Up SA_O_BU
     *   * Situation_Assessment_Output_Top_Down SA_O_TD -> sets IA_I_TD.SA_O_TD
     */
    SitationAssessment_Input SA_I;
    SA_I.MM_O = &MM_O;
    SituationAssessment_Output_BU SA_O_BU;
    Situation_Assessment(&SA_I, &SA_O_BU, SA_O_TD, &vehicleParameters, time);
    IA_I_TD.SA_O_TD = SA_O_TD;

    /** @ingroup al_mod_driver
     * @defgroup ac_ded Action Deduction
     * - Call of the function Action_Deduction() with the pointer to the containers:
     *   * Action_Deduction_Input_Top_Down AD_I_TD (empty yet)
     *   * Action_Deduction_Input_Bottom_Up AD_I_BU <- set from SA_O_BU and MM_O
     *   * Action_Deduction_Output_Bottom_Up AD_O_BU
     *   * Action_Deduction_Output_Top_Down AD_O_TD -> sets IA_I_TD.AD_O_TD
     */
    ActionDeduction_Input AD_I;
    AD_I.SA_O_BU = &SA_O_BU;
    AD_I.MM_O = &MM_O;
    ActionDeduction_Output_BU AD_O_BU;
    Action_Deduction(&AD_I, &AD_O_BU, AD_O_TD, &vehicleParameters, time);
    IA_I_TD.AD_O_TD = AD_O_TD;

    /** @ingroup al_mod_driver
     * @defgroup ac_ex Action Execution
     * - Call of the function Action_Execution() with the pointer to the containers:
     *   * Action_Execution_Input_Top_Down AE_I_TD (empty yet)
     *   * Action_Execution_Input_Bottom_Up AE_I_BU <- set from AD_O
     *   * Action_Execution_Output_Bottom_Up AE_O_BU
     *   * Action_Execution_Output_Top_Down AE_O_TD -> sets IA_I_TD.AE_O_TD
     */
    ActionExecution_Input AE_I;
    AE_I.AD_O_BU = &AD_O_BU;
    AE_I.MM_O = &MM_O;
    ActionExecution_Output_BU AE_O_BU;
    Action_Execution(&AE_I, &AE_O_BU, AE_O_TD, &vehicleParameters, time);
    IA_I_TD.AE_O_TD = AE_O_TD;

    /** @addtogroup al_mod_output
     *   Setting of the output-parameters from AE_O_BU
     */

    out_desiredSteeringWheelAngle = AE_O_BU.out_desiredSteeringWheelAngle;
    out_accPedalPos = AE_O_BU.out_accPP;
    out_brakePedalPos = AE_O_BU.out_brkPP;
    out_gear = AE_O_BU.out_gear;
    out_indicatorState = AE_O_BU.out_indicatorState;

    IA_I_BU = {};
}

void AlgorithmModularDriverImplementation::Information_Acquisition (InformationAcquisition_Input_TD *IA_Input_TD,
                                                                    InformationAcquisition_Input_BU *IA_Input_BU,
                                                                    InformationAcquisition_Output *IA_Output,
                                                                    int time)
{
    /** @addtogroup in_acq
     *  - Information_Acquisition(): Call of the functions of the internal object "informationacquisition" described in "Algorithm Information Acquisition":
     *      -# SetInformationAcquisition_Input()
     *      -# SetEgoData()
     *      -# SetEnvironmentInformation()
     *      -# SetSurroundingMovingObjects()
     *      -# LogSetValues()
     */
    informationacquisition.SetInformationAcquisition_Input(IA_Input_TD, IA_Input_BU);
    informationacquisition.SetEgoData(IA_Output);
    informationacquisition.SetEnvironmentInformation(IA_Output);
    informationacquisition.SetSurroundingMovingObjects(IA_Output);
    informationacquisition.LogSetValues(time);
}

void AlgorithmModularDriverImplementation::Mental_Model (MentalModel_Input_TD *MM_Input_TD,
                                                         MentalModel_Input_BU *MM_Input_BU,
                                                         MentalModel_Output *MM_Output)
{
    /** @addtogroup ment_mod
     *  - Mental_Model(): Call of the functions of the internal object "mentalmodel" described in "Algorithm Mental Model":
     *      -# SetMentalModel_Input()
     *      -# GetMentalModel_Output()
     */
    mentalmodel.SetMentalModel_Input(MM_Input_BU, MM_Input_TD);
    mentalmodel.GetMentalModel_Output(MM_Output);
}

void AlgorithmModularDriverImplementation::Situation_Assessment (SitationAssessment_Input *SA_Input,
                                                                 SituationAssessment_Output_BU *SA_Output_BU,
                                                                 SituationAssessment_Output_TD *&SA_Output_TD,
                                                                 const agentParameters *vehicleParameters,
                                                                 int time)
{
    /** @addtogroup sit_ass
     *  - Situation_Assessment(): Call of the functions of the internal object "situationassessment" described in "Algorithm Situation Assessment":
     *      -# Initialisation: Initialize(), just in the first time-step
     *      -# SetSituationAssessment_Input()
     *      -# SetEnvironmentInformation()
     *      -# Pigeonhole_SurroundingMovingObjectsToEgo()
     *      -# UpdateSituationCalculations()
     *      -# CheckForInternalLogging()
     *      -# LogSetValues(), just in the first time-step
     *      -# AssessEgoSituationAndWish()
     *      -# GetSituationAssessment_Output_TD
     */
    if (initialisationSA)
    {
        initialisationSA = false;
        situationassessment.Initialize();
    }
    situationassessment.SetSituationAssessment_Input(SA_Input);
    situationassessment.SetEnvironmentInformation(SA_Output_BU);
    situationassessment.Pigeonhole_SurroundingMovingObjectsToEgo(SA_Output_BU, time);

    situationassessment.UpdateSituationCalculations();
    situationassessment.CheckForInternalLogging(time);

    if (!situationassessment.InitialValuesLogged())
    situationassessment.LogSetValues(time);

    situationassessment.AssessEgoSituationAndWish(SA_Output_BU);

    SA_Output_TD = situationassessment.GetSituationAssessment_Output_TD();
}

void AlgorithmModularDriverImplementation::Action_Deduction (ActionDeduction_Input *AD_Input,
                                                             ActionDeduction_Output_BU *AD_Output_BU,
                                                             ActionDeduction_Output_TD *AD_Output_TD,
                                                            const agentParameters *vehicleParameters,
                                                             int time)
{
    /** @addtogroup ac_ded
     *  - Action_Deduction: Call of the functions of the internal object "actiondeduction" described in "Algorithm Action Deduction":
     *      -# Initialisation: Initialize(), just in the first time-step
     *      -# SetActionDeduction_Input()
     *      -# CalcAccelerationWish()
     *      -# LogSetValues(), just in the first time-step
     *      -# CheckLaneChange()
     *      -# prepareLanechange()
     *      -# GetActionDecution_Output_BU()
     *      -# GetActionDecution_Output_TD()
     */
    if (initialisationAD)
    {
        initialisationAD = false;
        actiondeduction.Initialize();
    }

    actiondeduction.SetActionDeduction_Input(*AD_Input);
    actiondeduction.CalcAccelerationWish(*AD_Output_BU);

    if (!actiondeduction.InitialValuesLogged())
        actiondeduction.LogSetValues(time);

    actiondeduction.CheckLaneChange(&time, AD_Output_BU);
    actiondeduction.prepareLanechange(AD_Output_BU);

    actiondeduction.GetActionDecution_Output_BU(*AD_Output_BU);
    AD_Output_TD = actiondeduction.GetActionDecution_Output_TD();
}

void AlgorithmModularDriverImplementation::Action_Execution (ActionExecution_Input *AE_Input,
                                                             ActionExecution_Output_BU *AE_Output_BU,
                                                             ActionExecution_Output_TD *&AE_Output_TD,
                                                             const agentParameters *vehicleParameters,
                                                             int time)
{
    /** @addtogroup ac_ex
     *  - Action_Execution(): Call of the functions of the internal object "actionexecution" described in "Algorithm Action Execution":
     *      -# SetActionExecution_Input()
     *      -# CalculatePedalPosAndGear()
     *      -# CalculateSteeringWheelAngle()
     *      -# SetIndicatorStateDirection()
     *      -# GetActionExecution_Output_TD()
     */
    actionexecution.SetActionExecution_Input(AE_Input);
    actionexecution.CalculatePedalPosAndGear(AE_Output_BU);
    actionexecution.CalculateSteeringWheelAngle(AE_Output_BU, time);
    actionexecution.SetIndicatorStateDirection(AE_Output_BU);
    AE_Output_TD = actionexecution.GetActionExecution_Output_TD();
}
