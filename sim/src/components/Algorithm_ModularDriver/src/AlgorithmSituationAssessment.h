/******************************************************************************
* Copyright (c) 2019 AMFD GmbH
* Copyright (c) 2021 HLRS, Universitaet Stuttgart
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  AlgorithmSituationAssessment.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief provide data and calculate driver's situation assessment parameters.
//!
//!

#pragma once

//#include <vector>
#include <include/stochasticsInterface.h>
#include <include/observationInterface.h>
#include "agentParameters.h"
#include "../../Sensor_Modular_Driver/src/Container/ContainerStructures.h"
#include "SituationAssessmentMethods/SituationCalculation.h"
#include "SituationAssessmentMethods/SituationLogging.h"

//! \brief This class contains the assessment of the current situation, regarding the surrounding agents
//! and select the leader, follower, neighleader and neighfollower and checking the criticality of the situation
//!
//! \ingroup Algorithm_Modular_Driver
//! \defgroup Algorithm_Situation_Assessment

//! \ingroup Algorithm_Situation_Assessment
class SituationAssessment
{
public:

    //!
    //! \brief SituationAssessment
    //! \param stochastics          Stochastics module
    //! \param cycleTime            defined cycleTime of the current simulation run
    //!
    SituationAssessment(StochasticsInterface *stochastics, int cycleTime, ObservationInterface *observer); // avoid access of constructor of singleton outside class

    //!
    //! \brief Initialize
    //!
    //! initializes the static values for the class:
    //!
    //! \param vWish                            mean velocity-wish of the current agent
    //! \param vWish_deviation                  std deviation of the velocity wish (lognormal)
    //! \param tGapWish                         mean prefered time-gap to the leader agent
    //! \param tGapWish_deviation               std deviation of the prefered time-gap (lognormal)
    //! \param speedLimit_violation             mean violation difference to the current speed-limit (speedlimit 120 km/h, violation 10 km/h --> vwish 130 +- deviation)
    //! \param speedLimit_violation_deviation   std deviation of the violation difference
    //! \param minDistance                      minimal accepted distance between the leader and ego
    //! \param vehicleparameters                the static vehicle parameters of the current agent
    //! \param observer                         current observation instance
    //!
    void Initialize();

    //!
    //! \brief LogSetValues
    //!
    //! Log the initial values of the stochastic set values to the observer, when they're set
    //! \param time
    //!
    void LogSetValues(int time);

    //!
    //! \brief InitialValuesLogged
    //!
    //! \return If initial values are logged
    //!
    bool InitialValuesLogged()
    {
        return Logged;
    }

    //!
    //! \brief SetSituationAssessment_Input
    //!
    //! sets the input of the class
    //!
    //! \param Input Container of type SitationAssessment_Input
    //!
    void SetSituationAssessment_Input(SitationAssessment_Input *Input);

    //!
    //! \brief GetSituationAssessment_Output_TD
    //! \return
    //!
    SituationAssessment_Output_TD *GetSituationAssessment_Output_TD();

    //!
    //! \brief SetMeanReactionTime
    //! \param MeanReactionTime
    //!
    void SetMeanReactionTime(int MeanReactionTime)
    {
        meanReactionTime = MeanReactionTime;
    }
    //!
    //! \brief SetReactionTimeDeviation
    //! \param ReactionTimeDeviation
    //!
    void SetReactionTimeDeviation(int ReactionTimeDeviation)
    {
        reactionTimeDeviation = ReactionTimeDeviation;
    }
    //!
    //! \brief SetMinReactionTime
    //! \param MinReactionTime
    //!
    void SetMinReactionTime(int MinReactionTime)
    {
        minReactionTime = MinReactionTime;
    }

    //!
    //! \brief SetEnvironmentInformation
    //!
    //! Sorts the lane-markings by distance and sets the environment information for the output of the module
    //!
    //! \param SA_Output_BU
    //!
    void SetEnvironmentInformation(SituationAssessment_Output_BU *SA_Output_BU);
    //!
    //! \brief AssessEgoSituationAndWish
    //!
    //! Checks the current situation, including the current speed-limit, nearest stop-signs and sets the velocity-wish depending on these.
    //! Sets the speed-limit and stop-signs for the output of the assessed environment.
    //! Sets the output-container #driverInformation as part of the output-container #egoData
    //!
    //! \param SA_Output_BU
    //!
    void AssessEgoSituationAndWish(SituationAssessment_Output_BU *SA_Output_BU);
    //!
    //! \brief Pigeonhole_SurroundingMovingObjectsToEgo
    //!
    //! Iterates through the surrounding moving objects from the SA_Input->MM_O and checks their position in relation to the current agent.
    //! Therefore the lane-id of each moving object, their secondary-covered lanes and indicator states are checked.
    //! The Relations to the current agent are sorted, till there are agents set to the RelationTypes Leader, Follower on every lane.
    //!
    //! \param [in/out] SA_Output_BU
    //! \param time [in]
    //!
    void Pigeonhole_SurroundingMovingObjectsToEgo(SituationAssessment_Output_BU *SA_Output_BU, int time);

    //!
    //! \brief SetNearVehicle
    //!
    //! Sets the current viewed surrounding moving object to the output container with the assessed surrounding agents,
    //! if the current agent is reactive to detect e.g. a new vehicle as a leader
    //!
    //! \param RelationType current RelationType to be set to the output container
    //! \param it           currect surrounding moving object to be set to the output container
    //! \param ItState      current state of the surrounding moving object
    //! \param mindistance  currently nearest distance of a moving object to the current agent with this RelationType
    //! \param currdist     current distance of the viewed moving object to the current agent
    //! \param SA_Output_BU Bottom-up output container of the module Situation_Assessment
    //!
    void SetNearVehicle(RelationType RelationType,
                        const SurroundingMovingObjectsData *it,
                        state *ItState,
                        double *mindistance,
                        double currdist,
                        SituationAssessment_Output_BU *SA_Output_BU);

    //!
    //! \brief SortLaneMarkingsbyDistance
    //!
    //! sorts the lane markings by their distance and set them to the output of the module
    //!
    //! \param Environment
    //!
    void SortLaneMarkingsbyDistance(StaticEnvironmentData *Environment);

    //!
    //! \brief CompareLaneMarkings
    //!
    //! compares two lane markings by their relative distance to the start
    //!
    //! \param Mark1
    //! \param Mark2
    //! \return
    //!
    static bool CompareLaneMarkings(const LaneMarking::Entity &Mark1, const LaneMarking::Entity &Mark2)
    {
        double a = Mark1.relativeStartDistance;
        double b = Mark2.relativeStartDistance;
        // swap not needed
        if((a < 0 && b < 0)
            && (a > b))
            return true;

        if((a > 0 && b > 0)
            && (a < b))
            return true;

        // swap needed
        if (a < 0 && b > 0)
            return true;

        // swap needed
        if (a == 0 && b < 0)
            return true;

        // swap needed
        if (a == 0 && b > 0)
            return true;

        return false;
    }

    //!
    //! \brief SetRelationType
    //!
    //! Sets the RelationType and its distance to the current agent of the currently viewed moving object to the output of the module
    //!
    //! \param RelationType
    //! \param MinDistance  current distance to the agent
    //! \param it           currently viewed moving object
    //! \param SA_Output_BU output of the module
    //!
    void SetRelationType(RelationType RelationType,
                           double MinDistance,
                           const SurroundingMovingObjectsData *it,
                           SituationAssessment_Output_BU *SA_Output_BU);

    //!
    //! \brief CheckItOnViewedLane
    //!
    //! Checks if current viewed vehicle is eighter changing its lane to the viewed targetlane or covering it secondary
    //!
    //! \param it
    //! \param targetlaneid
    //! \return
    //!
    bool CheckItOnViewedLane(AgentRepresentation &it, int targetlaneid);

    //!
    //! \brief AdaptionOnSpeedLimit
    //!
    //! Adapts the velocity wish depending of the currently existing speed-limit, its distance to the ego and the commonSpeedLimit_Violation,
    //! if the free-velocity wish parameter is higher than the speedlimit plus the common violation
    //!
    //! \param SpeedLimit   current speed-limit
    //! \param Distance     current distance to the sign
    //! \return
    //!
    double AdaptionOnSpeedLimit(double SpeedLimit, double Distance);

    //!
    //! \brief UpdateSituationCalculations
    //!
    //! updates the member variables of the module situationCalculation
    //!
    void UpdateSituationCalculations();

    //!
    //! \brief CheckForInternalLogging
    //!
    //! Checks if logging of internal values is activated
    //!
    //! \param time
    //!
    void CheckForInternalLogging(int time);

    //!
    //! \brief SplitLoggingStates
    //!
    //! Splits the comma-separated input-parameters from the systemConfig.xml
    //!
    //! \param input
    //! \return
    //!
    std::vector<std::string> SplitLoggingStates(std::string input)
    {
        if (!input.empty())
        {
            std::vector<std::string> output;
            std::string token;
            std::istringstream tokenStream(input);
            while (std::getline(tokenStream, token, ','))
               {
                  output.push_back(token);
               }
            return output;
        }
        else
        {
            std::vector<std::string> output{};
            return output;
        }
    }

    //!
    //! \brief SetVWish
    //! \param vWish
    //!
    void SetVWish(double vWish)
    {
        this->vWish = vWish;
    }
    //!
    //! \brief SetVWishDeviation
    //! \param vWish_deviation
    //!
    void SetVWishDeviation(double vWish_deviation)
    {
        this->vWish_deviation = vWish_deviation;
    }
    //!
    //! \brief SetTGapWish
    //! \param tGapWish
    //!
    void SetTGapWish(double tGapWish)
    {
        this->tGapWish = tGapWish;
    }
    //!
    //! \brief SetTGapWishDeviation
    //! \param tGapWish_deviation
    //!
    void SetTGapWishDeviation(double tGapWish_deviation)
    {
        this->tGapWish_deviation = tGapWish_deviation;
    }
    //!
    //! \brief SetSpeedLimit_Violation
    //! \param speedLimit_Violation
    //!
    void SetSpeedLimit_Violation(double speedLimit_Violation)
    {
        this->speedLimit_Violation = speedLimit_Violation;
    }
    //!
    //! \brief SetSpeedLimit_ViolationDeviation
    //! \param speedLimit_Violation_deviation
    //!
    void SetSpeedLimit_ViolationDeviation(double speedLimit_Violation_deviation)
    {
        this->speedLimit_Violation_deviation = speedLimit_Violation_deviation;
    }
    //!
    //! \brief SetMinDistance
    //! \param minDistance
    //!
    void SetMinDistance(double minDistance)
    {
        this->minDistance = minDistance;
    }
    //!
    //! \brief SetLoggingGroups
    //! \param LoggingGroups
    //!
    void SetLoggingGroups(std::string LoggingGroups)
    {
        this->LoggingGroups = LoggingGroups;
    }
    //!
    //! \brief SetVehicleParameters
    //! \param vehicleParameters
    //!
    void SetVehicleParameters(const agentParameters *vehicleParameters)
    {
        this->vehicleParameters = vehicleParameters;
    }
    //!
    //! \brief SetTtcThresholdMean
    //! \param ttc_threshold_mean
    //!
    void SetTtcThresholdMean(double ttc_threshold_mean)
    {
        this->ttc_threshold_mean = ttc_threshold_mean;
    }
    //!
    //! \brief SetTtcThresholdStd
    //! \param ttc_threshold_std
    //!
    void SetTtcThresholdStd(double ttc_threshold_std)
    {
        this->ttc_threshold_std = ttc_threshold_std;
    }
    //!
    //! \brief ResetReactionTime
    //!
    //! Resets the values of the ReactionTrigger (=0) and ReactionTime threshold (stochastic, log-normal-distributed) at the current RelationType
    //!
    //! \param relationType relationType to react on
    //!
    void ResetReactionTime(RelationType relationType)
    {
        int time = std::max(minReactionTime,(int) std::round(_stochastic->GetLogNormalDistributed(meanReactionTime,reactionTimeDeviation)/10)*10);
        if (ReactionTrigger.find(relationType)!=ReactionTrigger.end())
        {
            ReactionTrigger.at(relationType) = 0;
            ReactionTime.at(relationType) = time;
        }
        else
        {
            ReactionTrigger.emplace(relationType, 0);
            ReactionTime.emplace(relationType, time);
        }
    }
    //!
    //! \brief IncrementReactionTime
    //!
    //! Increments the ReactionTrigger by one cycleTime
    //!
    //! \param relationType relationType to react on
    //!
    void IncrementReactionTime(RelationType relationType)
    {
        if (ReactionTrigger.find(relationType)!=ReactionTrigger.end())
        {
            ReactionTrigger.at(relationType) += cycleTime;
        }
        else
        {
            ReactionTrigger.emplace(relationType, cycleTime);
        }
    }
    //!
    //! \brief ActivateReaction
    //!
    //! Activates a reaction for a RelationType
    //!
    //! \param relationType relationType to activate the reaction for
    //!
    void ActivateReaction(RelationType relationType)
    {
        if (React.find(relationType)!=React.end())
        {
            React.at(relationType) = true;
        }
        else
        {
           React.emplace(relationType, true);
        }
    }
    //!
    //! \brief DeactivateReaction
    //!
    //! Deactivates a reaction for a RelationType
    //!
    //! \param relationType relationType to deactivate the reaction for
    //!
    void DeactivateReaction(RelationType relationType)
    {
        if (React.find(relationType)!=React.end())
        {
            React.at(relationType) = false;
        }
        else
        {
           React.emplace(relationType, false);
        }
    }
    //!
    //! \brief IsReactive
    //!
    //! Returns if the agent is reactive for a RelationType
    //!
    //! \param relationType RelationType to check a reaction for
    //! \return if the agent is reactive
    //!
    bool IsReactive(RelationType relationType)
    {
        if (React.find(relationType)!=React.end())
        {
            return React.at(relationType);
        }
        return false;
    }
    //!
    //! \brief GetCurrReactionTrigger
    //!
    //! \param relationType
    //! \return The current reaction trigger for a RelationType
    //!
    int GetCurrReactionTrigger(RelationType relationType)
    {
        if (ReactionTrigger.find(relationType)!=ReactionTrigger.end())
        {
            return ReactionTrigger.at(relationType);
        }
        return 0;
    }
    //!
    //! \brief GetCurrReactionTime
    //! \param relationType
    //! \return The current reaction time threshold for a RelationType
    //!
    int GetCurrReactionTime(RelationType relationType)
    {
        if (ReactionTime.find(relationType)!=ReactionTime.end())
        {
            return ReactionTime.at(relationType);
        }
        return 100;
    }
    //!
    //! \brief GetLastNearVehicleId
    //! \param relationType
    //! \return
    //!
    int GetLastNearVehicleId(RelationType relationType)
    {
        if (lastNearTraffic.find(relationType)!=lastNearTraffic.end())
        {
            return lastNearTraffic.at(relationType);
        }
        return -999;
    }
    //!
    //! \brief SetLastNearVehicleId
    //! \param relationType
    //! \param id
    //!
    void SetLastNearVehicleId(RelationType relationType, int id)
    {
        if (lastNearTraffic.find(relationType)!=lastNearTraffic.end())
        {
            lastNearTraffic.at(relationType) = id;
        }
        else
        {
            lastNearTraffic.emplace(relationType, id);
        }
    }
    //!
    //! \brief ClearLastNearVehicleId
    //! \param relationType
    //!
    void ClearLastNearVehicleId(RelationType relationType)
    {
        if (lastNearTraffic.find(relationType)!=lastNearTraffic.end())
        {
            lastNearTraffic.erase(relationType);
        }
    }
    //!
    //! \brief SetLastNearVehicles
    //! \param NearTraffic
    //!
    void SetLastNearVehicles(std::map<RelationType, SurroundingMovingObjectsData> *NearTraffic)
    {
        for (int reltypeint = Leader ; reltypeint != FollowerLeft ; reltypeint++)
        {
            RelationType relationType = static_cast<RelationType>(reltypeint);
            if (NearTraffic->find(relationType)!=NearTraffic->end())
                SetLastNearVehicleId(relationType, NearTraffic->at(relationType).GetState()->id);
            else
            {
                ClearLastNearVehicleId(relationType);
            }
        }
    }
    //!
    //! \brief CheckReaction
    //!
    //! Checks the reaction of the current agent for all RelationTypes and increments or resets the reaction-values
    //!
    void CheckReaction()
    {
        for (int reltypeint = Leader ; reltypeint != FollowerLeft ; reltypeint++)
        {
            RelationType relationType = static_cast<RelationType>(reltypeint);
            if (IsReactive(relationType))
            {
                IncrementReactionTime(relationType);
            }
            else
            {
                ResetReactionTime(relationType);
            }
        }
    }

protected:


private:


    SitationAssessment_Input *SA_Input;
    SituationAssessment_Output_TD SA_Output_TD;

    StochasticsInterface *_stochastic;
    int cycleTime;
    ObservationInterface *observation;

    //!
    bool Logged = false;

    const VehicleModelParameters *vehicleParameters;

    //! mean reaction time to react on new surrounding vehicles
    int meanReactionTime = 0;
    //! deviation of the reaction time to react on new surrounding vehicles
    int reactionTimeDeviation = 0;
    //! minimum reaction tim to react on new surrounding vehicles
    int minReactionTime = 0;

    //! logging groups to log internal values (comma seperated string) (currently: "ttc, thw, speeding, ettc")
    std::string LoggingGroups {};
    //! split logging groups from LogginGroups string
    std::vector<std::string> loggingGroups;

    //! object of class SituationCalculation
    SituationCalculation situationCalculation;
    //! object of class SituationLogging
    SituationLogging situationlogging;

    //! maximum lateral velocity of the ego
    double v_y_Max;

    //! mean of lognormal-distribution for the wish-velocity in km/h
    double vWish = 140 / 3.6;
    //! std deviation for the lognormal-distribution
    double vWish_deviation = 20 / 3.6;
    //! mean of lognormal-distribution for the wish-netto-timegap between leader and ego in s
    double tGapWish = 1.5;
    //! std deviation for the lognormal-distribution
    double tGapWish_deviation = 0.3;
    //! mean difference (normal-distribution) between the chosen speed and the current speed-limit
    double speedLimit_Violation = 5 / 3.6;
    //! std deviation of the speedLimit_Violation
    double speedLimit_Violation_deviation = 3 / 3.6; //
    double minDistance = 2;
    //! mean of lognormal-distribution for the ttc_threshold
    double ttc_threshold_mean = 3;
    //! std-deviation of the ttc_threshold
    double ttc_threshold_std = 0;
    //! current ttc threshold under which a emergency break is possible
    double ttc_threshold = 3;
    //! last known speed limit
    double lastSpeedLimit = 300/3.6;

    //!
    std::map<RelationType, int> lastNearTraffic;
    //!
    std::map<RelationType, int> ReactionTrigger;
    //!
    std::map<RelationType, bool> React;
    //!
    std::map<RelationType, int> ReactionTime;

    //!
    double commonSpeedLimit_Violation;
    //!
    double v_Wish_out;
    //!
    double thw_Wish_out;

    //!
    bool loggingActivated = false;
    //!
    bool initialisationSpeedLimit = false;

    //! Additional length added to the vehicle boundary when checking for collision detection
    double collisionDetectionLongitudinalBoundary {0.3};
    //! Additional width added to the vehicle boundary when checking for collision detection
    double collisionDetectionLateralBoundary {0.3};
    //! The minimum Time-To-Collision before the AEB component activates
    double ttcBrake{2};


};


