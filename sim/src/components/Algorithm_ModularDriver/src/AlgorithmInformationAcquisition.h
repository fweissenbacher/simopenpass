/******************************************************************************
* Copyright (c) 2019 AMFD GmbH
* Copyright (c) 2021 HLRS, Universitaet Stuttgart
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  AlgorithmInformationAcquisition.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief This file contains the calculations
//!
//! This class contains the acquisition of informations like visual and auditive.
//! Singleton such that data calculations is generated only once and not seperately
//! for each instance.
//!
//-----------------------------------------------------------------------------

#pragma once

//#include <vector>
#include <iostream>
#include "components/Sensor_Modular_Driver/src/Container/ContainerStructures.h"
#include <include/stochasticsInterface.h>
#include <include/observationInterface.h>

//! \brief This class contains the acquisition of informations like visual and auditive.
//! Singleton such that data calculations is generated only once and not seperately
//! for each instance.
//!
//! \ingroup Algorithm_Modular_Driver
//! \defgroup Algorithm_Information_Acquisition
//!

//! \ingroup Algorithm_Information_Acquisition
class InformationAcquisition
{
public:

    InformationAcquisition(StochasticsInterface *stochastics, ObservationInterface *observation); // avoid access of constructor of singleton outside class

    //!
    //! \brief LogSetValues
    //!
    //! Log the initial values of the stochastic set values to the observer, when they're set and when a collision occurs
    //! - CurrentAvertViewTime
    //! - CollisionAgentId, CollisionPartners, CurrAvertView, CurrAvertTrigger
    //!
    //! \param time     Current scheduling time
    //!
    void LogSetValues(int time);

    //!
    //! \brief SetInformationAcquisition_Input
    //!
    //! Update of the internal containers (pointers)
    //!
    //! \param Input_TD     pointer to the Input_Top_Down container
    //! \param Input_BU     pointer to the Input_Bottom_Up container
    //!
    void SetInformationAcquisition_Input(InformationAcquisition_Input_TD *Input_TD, InformationAcquisition_Input_BU *Input_BU);

    //!
    //! \brief SetEnvironmentInformation
    //!
    //! Update of the environment container part of the the IA_Output container (currently no change of values)
    //!
    //! \param IA_Output    pointer to the Output container
    //!
    void SetEnvironmentInformation(InformationAcquisition_Output *IA_Output);

    //!
    //! \brief SetEgoData
    //! Update of the ego container part of the IA_Output container (currently no change of values)
    //!
    //! \param IA_Output    pointer to the Output container
    //!
    void SetEgoData(InformationAcquisition_Output *IA_Output);

    //!
    //! \brief SetSurroundingMovingObjects
    //!
    //! Update of the surrounding moving objects container, if agent is "reactive",
    //! otherwise clear the IA_Output container part of the surrounding moving objects
    //!
    //! \param IA_Output
    //!
    void SetSurroundingMovingObjects(InformationAcquisition_Output *IA_Output);

    //!
    //! \brief IsReactive
    //!
    //! Check if agent is reactive (is averting his view or not) depending on the parameters:
    //! - meanViewTime, AvertViewTimeDeviation, meanViewTime, ViewTimeDeviation
    //! - Tresholds are set stochastically and during each of the alternating two states (viewing, avert-view)
    //! the trigger is increased by 100 ms every time-step, till it is bigger then the threshold of the current state
    //! - The state changes and with the new set values the trigger increases for the other state
    //!
    //!
    //! \return If the agent is reactive
    //!
    bool IsReactive();

    //!
    //! \brief SelectAgentsInFOV
    //!
    //! Selects all agents that are seen inside the sight cone and not hidden by another agent
    //!
    //! \param IA_Output
    //!
    void SelectAgentsInFOV(InformationAcquisition_Output *IA_Output);

    bool IsSwitchingView()
    {
        return viewIsSwitching;
    }

    //!
    //! \brief LookingForward
    //!
    //! Increments the forwardTrigger while the agent is looking forward till the threshold is exceeded
    //!
    //!  \return
    //!
    bool LookingForward()
    {
        if (forwardTrigger <= forwardThreshold)
        {
            forwardTrigger += 100;
            islookingforward = true;
        }
        else
        {
            forwardTrigger = 0;
            forwardThreshold = std::max(0, (int) std::round(_stochastic->GetLogNormalDistributed(800,100)/10)*10);
            islookingforward = false;
        }
        return islookingforward;
    }

    //!
    //! \brief IsLookingForward
    //! \return
    //!
    bool IsLookingForward()
    {
        return islookingforward;
    }

    //!
    //! \brief LookingBackward
    //!
    //!  Increments the backwardTrigger while the agent is looking backward till the threshold is exceeded
    //!
    //!  \return
    //!
    bool LookingBackward()
    {
        if (backwardTrigger <= backwardThreshold)
        {
            backwardTrigger += 100;
            islookingforward = false;
        }
        else
        {
            backwardTrigger = 0;
            backwardThreshold = std::max(0, (int) std::round(_stochastic->GetLogNormalDistributed(200,100)/10)*10);
            islookingforward = true;
        }
        return !islookingforward;
    }

    //!
    //! \brief SelectAgentsForward
    //!
    //! Checks if agents are in front of the current agent and deletes the others
    //!
    //!  \param IA_Input_BU
    //!
    void SelectAgentsForward(InformationAcquisition_Input_BU *IA_Input_BU)
    {
        double xpos = IA_Input_BU->EgoData.GetState()->pos.xPos;
        std::list<SurroundingMovingObjectsData> *v = &IA_Input_BU->SurroundingMovingObjects;
        v->erase(std::remove_if(
            v->begin(), v->end(),
            [xpos](SurroundingMovingObjectsData& x) {
                return x.GetState()->pos.xPos < xpos; // put your condition here
            }), v->end());
    }

    //!
    //! \brief SelectAgentsBackward
    //!
    //! Checks if agents are behind of the current agent and deletes the others
    //!
    //!  \param IA_Input_BU
    //!
    void SelectAgentsBackward(InformationAcquisition_Input_BU *IA_Input_BU)
    {
        double xpos = IA_Input_BU->EgoData.GetState()->pos.xPos;
        std::list<SurroundingMovingObjectsData> *v = &IA_Input_BU->SurroundingMovingObjects;
        v->erase(std::remove_if(
            v->begin(), v->end(),
            [xpos](SurroundingMovingObjectsData& x) {
                return x.GetState()->pos.xPos >= xpos; // put your condition here
            }), v->end());
    }

    //!
    //! \brief SetMeanAvertViewTime
    //!
    //! Sets the internal parameter meanAvertViewTime
    //!
    //! \param MeanReactionTime
    //!
    void SetMeanAvertViewTime(int MeanReactionTime)
    {
        meanAvertViewTime = MeanReactionTime;
    }

    //!
    //! \brief SetAvertViewTimeDeviation
    //!
    //! Sets the internal parameter AvertViewTimeDeviation
    //!
    //! \param ReactionTimeDeviation
    //!
    void SetAvertViewTimeDeviation(int ReactionTimeDeviation)
    {
        AvertViewTimeDeviation = ReactionTimeDeviation;
    }

    //!
    //! \brief SetViewTimeMean
    //!
    //! Sets the internal parameter meanViewTime
    //!
    //! \param view_time_mean
    //!
    void SetViewTimeMean(double view_time_mean)
    {
        this->meanViewTime = view_time_mean;
    }

    //!
    //! \brief SetViewTimeDeviation
    //!
    //! Sets the internal parameter ViewTimeDeviation
    //!
    //! \param view_time_dev
    //!
    void SetViewTimeDeviation(double view_time_dev)
    {
        this->ViewTimeDeviation = view_time_dev;
    }

    //!
    //! \brief SetForwardViewTime
    //!
    //! Sets the threshold for the time the agent is viewing forward and turns the switch between forward and backward view on
    //!
    //!  \param view_forward_mean
    //!
    void SetForwardViewTime(double view_forward_mean)
    {
        forwardThreshold = view_forward_mean;
        viewIsSwitching = true;
    }

    void SetForwardViewTimeDeviation(double view_forward_dev)
    {
        forwardViewDeviation = view_forward_dev;
    }

    void SetBackwardViewTime(double view_backward_mean)
    {
        backwardThreshold = view_backward_mean;
        viewIsSwitching = true;
    }

    void SetBackwardViewTimeDeviation(double view_backward_dev)
    {
        backwardViewDeviation = view_backward_dev;
    }

    //!
    //! \brief GetInformationAcquisition_Output
    //!
    //! Returns the internal output container with the in this module calculated values
    //!
    //! \return
    //!
    InformationAcquisition_Output * GetInformationAcquisition_Output();

private:


    bool Logged = false;
    //! time-headway-wish
    double thw_Wish;
    //! velocity wish
    double v_Wish;
    //! maximum y-velocity
    double v_y_Max;
    //! minimum Gap to leader
    double MinGap;

    //! Mean value of the time an agent averts the view
    int meanAvertViewTime = 0;
    //! Deviation of the avert-view-time
    int AvertViewTimeDeviation = 0;
    //! Minimum avert-view-time
    int minAvertViewTime = 0;
    //! Mean value of the time an agents views avert he averted the view
    int meanViewTime = 0;
    //! Deviation of the view-time
    int ViewTimeDeviation = 0;

    //! Incrementing value for triggering the view-time
    int viewTimeTrigger = 0;
    //! Current threshold of the view time, which is stochastically calculated by mean and deviation
    int currentViewTime = 0;
    //! Incrementing value for triggering the avert-view-time
    int reactionTimeTrigger = 0;
    //! Current threshold of the avert view time, which is stochastically calculated by mean and deviation
    int currentReactionTime = 0;
    //! Is reactive - Does agent view?
    bool isreactive = false;
    //! is initialized
    bool init = false;

    //! Deviation of the mean forward view-time
    int forwardViewDeviation;
    //! Deviation of the mean backward view-time
    int backwardViewDeviation;

    //! Shows if agent is switching his view between forward and backward
    bool viewIsSwitching = false;

    //! Is agent currently looking forward
    bool islookingforward = true;
    //! Incrementing value for triggering the forward view
    int forwardTrigger = 0;
    //! Current threshold of the forward-view-time
    int forwardThreshold = 800;

    //! Incrementing value for triggering the backward view
    int backwardTrigger = 0;
    //! Current threshold of the backward-view-time
    int backwardThreshold = 200;

    //! Information Acquisition Input Bottom Up Container
    InformationAcquisition_Input_BU *IA_Input_BU;
    //! Information Acquisition Input Bottom Up Container
    InformationAcquisition_Input_TD *IA_Input_TD;
    //! Link to StochasticsInterface
    StochasticsInterface *_stochastic;
    //! Link to ObservationInterface
    ObservationInterface *_observation;

};


