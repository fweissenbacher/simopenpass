#-----------------------------------------------------------------------------
# /file  AlgorithmModularDriver.pro
# /brief This file contains the information for the QtCreator-project of the
# module AlgorithmModularDriver
#
# Copyright (c) 2018 in-tech GmbH
#
#-----------------------------------------------------------------------------/
DEFINES += ALGORITHM_MODULARDRIVER_LIBRARY
CONFIG += OPENPASS_LIBRARY
include(../../../global.pri)

SUBDIRS +=  . \
            ../Sensor_Modular_Driver/src/Signals \
            ../Sensor_Modular_Driver/src/Container \
            src/ActionDeductionMethods \
            src/SituationAssessmentMethods \
            ../../Common \
            ../../Interfaces \
            ../Sensor_Driver/src/Signals \
            ..

INCLUDEPATH += $$SUBDIRS \
            ../Algorithm_Longitudinal \
            ..


SOURCES += \
    $$getFiles(SUBDIRS, cpp) \
    $$getFiles(SUBDIRS, cc) \
    $$getFiles(SUBDIRS, c) \
    ../Algorithm_Longitudinal/src/algorithm_longitudinalCalculations.cpp \

HEADERS += \
    $$getFiles(SUBDIRS, hpp) \
    $$getFiles(SUBDIRS, h) \
    ../Algorithm_Longitudinal/src/algorithm_longitudinalCalculations.h \
