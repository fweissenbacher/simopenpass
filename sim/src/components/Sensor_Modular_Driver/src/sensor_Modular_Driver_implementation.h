/******************************************************************************
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  sensor_Modular_Driver_implementation.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief provide all for the driver relevant information
//!

#ifndef SENSOR_MODULAR_DRIVER_IMPLEMENTATION_H
#define SENSOR_MODULAR_DRIVER_IMPLEMENTATION_H

#include <math.h>
#include "include/modelInterface.h"
#include "include/observationInterface.h"
#include "Signals/complexsignals.h"
#include "common/primitiveSignals.h"
#include "core/slave/modules/World_OSI/WorldData.h"
#include "Container/ContainerStructures.h"
#include "include/egoAgentInterface.h"

/*! \addtogroup Sensor_Modular_Driver
 * @{
 * \brief basic module to sense the current state of an agent
 *
 * This module provides information about the current ego-state, environment and surrounding agents
 *
 * \section Sensor_Modular_Driver_Inputs Inputs
 * none
 *
 * \section Sensor_Modular_Driver_InitInputs Init Inputs
 * none
 *
 * \section Sensor_Modular_Driver_Outputs Outputs
 * Output variables:
 * name | meaning
 * -----|------
 * StaticEnvironment            | Container with the information about the static environment
 * Ego                          | Container with the information about the ego-vehicle
 * SurroundingMovingObjects     | Container with the information about the surrounding-vehicles
 *
 * Output channel IDs:
 * Output Id | signal class | contained variables
 * ------------|--------------|-------------
 * 0 | structSignal<StaticEnvironmentData> | StaticEnvironment
 * 1 | structSignal<egoData> | Ego
 * 2 | structSignal<SurroundingMovingObjectsData> | SurroundingMovingObjects
 *
 * \section Sensor_Modular_Driver_ExternalParameters External parameters
 * none
 *
 * \section Sensor_Modular_Driver_ConfigParameters Parameters to be specified in agentConfiguration.xml
 * none
 *
 * \section Sensor_Modular_Driver_InternalParameters Internal paramters
 * none
 */

template<class T>
class structSignal;

/**
* \ingroup Sensor_Modular_Driver
*/
class Sensor_Modular_Driver_Implementation : public SensorInterface
{
public:
    const std::string COMPONENTNAME = "Sensor_Modular_Driver";

    Sensor_Modular_Driver_Implementation(const std::string &componentName,
                                         bool isInit,
                                         int priority,
                                         int offsetTime,
                                         int responseTime,
                                         int cycleTime,
                                         StochasticsInterface *stochastics,
                                         WorldInterface *world,
                                         const ParameterInterface *parameters,
                                         PublisherInterface *const publisher,
                                         const CallbackInterface *callbacks,
                                         AgentInterface *agent);
    virtual ~Sensor_Modular_Driver_Implementation() = default;

    /*!
     * \brief Update Inputs
     *
     * Function is called by framework when another component delivers a signal over
     * a channel to this component (scheduler calls update taks of other component).
     *
     * Refer to module description for input channels and input ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
     * \param[in]     data           Referenced signal (copied by sending component)
     * \param[in]     time           Current scheduling time
     */
    virtual void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data,
                             int time);

    /*!
     * \brief Update outputs.
     *
     * Function is called by framework when this component has to deliver a signal over
     * a channel to another component (scheduler calls update task of this component).
     *
     * Refer to module description for output channels and output ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
     * \param[out]    data           Referenced signal (copied by this component)
     * \param[in]     time           Current scheduling time
     */
    virtual void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time);

    /*!
     * \brief Process data within component.
     *
     * Function is called by framework when the scheduler calls the trigger task
     * of this component.
     *
     * Refer to module description for information about the module's task.
     *
     * \param[in]     time           Current scheduling time
     */
    virtual void Trigger(int time);

    //! \brief Calculates a new route for the agent and sets it in the EgoAgent
    void GetNewRoute();

    //! \brief Updates the current position in the routing graph if neccessary
    void UpdateGraphPosition();

    /*!
     * \brief SetEgoData
     * Collects the ego-information of the current agent
     * consisting of the AgentVehicleType, the #state and the #state_Ego
     */
    void SetEgoData();

    /*!
     * \brief SetMovingObjects
     * Collect Data from the World-Interface about all surrounding agents within a specific range and sort them by their distance to the current agent.
     * Information about the AgentVehicleType, #properties and #State_Moving are collected for each surrounding agent
     *
     * \param Agents [in] Map of all existing agents
     */
    void SetMovingObjects(const std::map<int, AgentInterface*> *Agents);

    /*!
     * \brief SetEnvironment
     * \param agent
     */
    void SetEnvironment(AgentInterface *Agent);

    //! \brief Get sensor data containing traffic rule information.
    virtual void GetTrafficRuleInformation();

    //! \brief Get traffic rule sensor data from the ego lane.
    LaneInformationTrafficRules GetTrafficRuleLaneInformationEgo();

    //! \brief Get traffic rule sensor data from the lane left of ego.
    LaneInformationTrafficRules GetTrafficRuleLaneInformationLeft();

    //! \brief Get traffic rule sensor data from the lane right of ego.
    LaneInformationTrafficRules GetTrafficRuleLaneInformationRight();

    //! \brief Get lane geometry sensor data.
    virtual void GetRoadGeometry();

    //! \brief Get lane geometry sensor data from the ego lane.
    void GetGeometryLaneInformationEgo(LaneInformation &laneInformation);

    //! \brief Get lane geometry sensor data from the lane left of ego.
    void GetGeometryLaneInformationLeft(LaneInformation &laneInformation);

    //! \brief Get lane geometry sensor data from the lane right of ego.
    void GetGeometryLaneInformationRight(LaneInformation &laneInformation);

    //! \brief Iterates through every road section of the current road (currently only id "1") to call #ConvertRoadToMMLane() for
    //! a mental description of each lane
    void SetMentalModelGeometryLaneInformation();

    //!
    //! \brief ConvertRoadToMMLane
    //! Checks special lane to save it with its geometrical description
    //! \param lane     OWL-Lane with all of its information
    //! \param OdId     openDriveId
    //!
    void ConvertRoadToMMLane(OWL::Lane* lane, int64_t OdId);

    //!
    //! \brief GetCurrentOsiLaneId
    //! Transforms the laneID from openDrive2OWL
    //! \param odRoadId     Id of Road in openDrive-Format
    //! \return             Id of Road in OWL-Format
    //!
 //   uint64_t GetCurrentOsiLaneId(const std::string &odRoadId)
//    {
        // TODO Adjust to 0.7 changes
//        std::unordered_map<uint64_t, std::string> RoadMap = static_cast<OWL::WorldData*>(GetWorld()->GetWorldData())->GetRoadIdMapping();
//        for (auto it = RoadMap.begin(); it!=RoadMap.end(); ++it)
//        {
//            if (it->second == odRoadId)
//                return it->first;
//        }
//        return 0;
//   }

    /*!
     * \brief The DistanceComparator struct
     * compares the distance between two objects of type #SurroundingMovingObjectsData
     */
    struct DistanceComparator
    {
        bool operator ()(SurroundingMovingObjectsData &Obj1, SurroundingMovingObjectsData &Obj2)
        {
            return *Obj1.GetDistanceToEgo() < *Obj2.GetDistanceToEgo();
        }
    };

private:

    AgentInterface* Agent {nullptr};
    EgoAgentInterface& EgoAgent;
    StaticEnvironmentData StaticEnvironment; //! Internal object for the static environment, filled and updated every timestep for output
    egoData Ego; //! Internal object for the ego information, filled and updated every timestep for output
    std::list<SurroundingMovingObjectsData> SurroundingMovingObjects; //! Internal container for the surrounding moving objects, filled and updated every timestep for output
    VehicleModelParameters vehicleParameters;
    bool initializedVehicleModelParameters {false};

    std::map<int, std::vector<MentalModelLane>> lanes; //! map with all mental lane sections with their id
    OWL::Road* _currentRoad = nullptr;
    int SpawnTime {-999};
};

#endif // SENSOR_MODULAR_DRIVER_IMPLEMENTATION_H
