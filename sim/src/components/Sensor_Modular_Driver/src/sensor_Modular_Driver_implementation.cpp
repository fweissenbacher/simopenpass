/******************************************************************************
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
* Copyright (c) 2021 HLRS, Universitaet Stuttgart
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  sensor_Modular_Driver_implementation.cpp
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief provide all for the driver relevant information
//!
//-----------------------------------------------------------------------------

/** @defgroup sens_mod_driver Sensor Modular Driver
  *
  * This module does the selection of all needed information from the environment and the grouping into the corresponding containers
  *
  */


/**
 * @ingroup sens_mod_driver
 * @defgroup sensing_process Process of perception
*/


#include <memory>
#include <qglobal.h>
#include "sensor_Modular_Driver_implementation.h"
//#include "Container/MentalModelLane.h"
#include "core/slave/modules/World_OSI/RoutePlanning/RouteCalculation.h"
//#include "include/egoAgentInterface.h"
// #include "complexsignals.cpp"
#include "common/parametersVehicleSignal.h"


Sensor_Modular_Driver_Implementation::Sensor_Modular_Driver_Implementation(const std::string &componentName,
                                                               bool isInit,
                                                               int priority,
                                                               int offsetTime,
                                                               int responseTime,
                                                               int cycleTime,
                                                               StochasticsInterface *stochastics,
                                                               WorldInterface *world,
                                                               const ParameterInterface *parameters,
                                                               PublisherInterface *const publisher,
                                                               const CallbackInterface *callbacks,
                                                               AgentInterface *agent) :
    SensorInterface(componentName,
                    isInit,
                    priority,
                    offsetTime,
                    responseTime,
                    cycleTime,
                    stochastics,
                    world,
                    parameters,
                    publisher,
                    callbacks,
                    agent),
    Agent(agent),
    EgoAgent(agent->GetEgoAgent())
{
    UpdateGraphPosition();
}

void Sensor_Modular_Driver_Implementation::UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time)
{
    Q_UNUSED(time);

    if (localLinkId == 1)
    {
        if (!initializedVehicleModelParameters)
        {
            // from ParametersAgent
            const std::shared_ptr<ParametersVehicleSignal const> signal = std::dynamic_pointer_cast<ParametersVehicleSignal const>(data);
            if (!signal)
            {
                const std::string msg = COMPONENTNAME + " invalid signaltype";
                LOG(CbkLogLevel::Debug, msg);
                throw std::runtime_error(msg);
            }
            vehicleParameters = signal->vehicleParameters;
            initializedVehicleModelParameters = true;
        }
    }
}

void Sensor_Modular_Driver_Implementation::UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time)
{
    Q_UNUSED(time);

    try
    {
        switch (localLinkId)
        {
        case 0:
            data = std::make_shared<structSignal<StaticEnvironmentData> const>(StaticEnvironment);
            break;
        case 1:
            data = std::make_shared<structSignal<egoData> const>(Ego);
            break;
        case 2:
            data = std::make_shared<structSignal<std::list<SurroundingMovingObjectsData>> const>(SurroundingMovingObjects);
            break;
        default:
            const std::string msg = COMPONENTNAME + " invalid link";
            LOG(CbkLogLevel::Debug, msg);
            throw std::runtime_error(msg);
        }
    }
    catch(const std::bad_alloc&)
    {
        const std::string msgDefault = COMPONENTNAME + " could not instantiate signal";
        LOG(CbkLogLevel::Debug, msgDefault);
        throw std::runtime_error(msgDefault);
    }

}

void Sensor_Modular_Driver_Implementation::Trigger(int time)
{
    if (SpawnTime < 0)
    {
        SpawnTime = time;
    }

    UpdateGraphPosition();

    /** @ingroup sensing_process
     * @defgroup ego_sensing Ego-Vehicle
     * - Collect Data from the Agent-Interface
     *
     */
    SetEgoData();

    /** @ingroup sensing_process
     * @defgroup surr_sensing Surrounding-Vehicles
     * - Collect Data from the World-Interface about all surrounding agents (GetWorld->GetAgents())
     */
    std::map<int, AgentInterface*> Agents = GetWorld()->GetAgents();
    SetMovingObjects(&Agents);

    /** @ingroup sensing_process
     * @defgroup env_sensing Environment
     * - Sense information about the static environment
     */
    StaticEnvironment = {};
    SetMentalModelGeometryLaneInformation();
    GetTrafficRuleInformation();
    GetRoadGeometry();

}

void Sensor_Modular_Driver_Implementation::GetNewRoute()
{
    constexpr int maxDepth = 10;
    const auto& roadIds = GetAgent()->GetRoads(MeasurementPoint::Front);
    if (roadIds.empty())
    {
        return;
    }
    auto [roadGraph, root] = GetWorld()->GetRoadGraph({roadIds.front(), GetAgent()->GetObjectPosition().mainLocatePoint.at(roadIds.front()).laneId < 0}, maxDepth);
    std::map<RoadGraph::edge_descriptor, double> weights = GetWorld()->GetEdgeWeights(roadGraph);
    auto target = RouteCalculation::SampleRoute(roadGraph, root, weights, *GetStochastics());
    EgoAgent.SetRoadGraph(std::move(roadGraph), root, target);
}

void Sensor_Modular_Driver_Implementation::UpdateGraphPosition()
{
    if(!EgoAgent.HasValidRoute())
    {
        GetNewRoute();
    }
}

void Sensor_Modular_Driver_Implementation::SetMentalModelGeometryLaneInformation()
{

    /** @addtogroup env_sensing
     * - Select neccessary information about the Environment from the World-Interface
     * - (currently only road id "1")
     */
    // TODO Reintegrate previous code
//    std::unordered_map<std::string, OWL::Interfaces::Road*> currentRoads = static_cast<OWL::Interfaces::WorldData*>(world->GetWorldData())->GetRoads();//->GetRoadById(GetCurrentOsiLaneId("1"));
//    OWL::Interfaces::Road  *currentRoad = currentRoads.at(GetCurrentOsiLaneId("1"));

    std::unordered_map<std::string, OWL::Road*> currentRoads {static_cast<OWL::WorldData*>(world->GetWorldData())->GetRoads()};
    if (currentRoads.empty() || currentRoads.size() > 1)
    {
        throw std::runtime_error("Current implementation does not support more than one road");
    }

    OWL::Road* currentRoad {currentRoads.at("1")};
    if (currentRoad != _currentRoad)
    {
        _currentRoad = const_cast<OWL::Road*>(currentRoad);
        lanes.clear();
        /** @addtogroup env_sensing
         * - Select road sections
         */
        for(auto section : currentRoad->GetSections())
        {
            /** @addtogroup env_sensing
             * - Select lanes in each section
             */
            for (auto lane : section->GetLanes())
            {
                ConvertRoadToMMLane(const_cast<OWL::Lane*>(lane), static_cast<OWL::WorldData*>(GetWorld()->GetWorldData())->GetLaneIdMapping().at(lane->GetId()));
            }
        }
    }
}

void Sensor_Modular_Driver_Implementation::ConvertRoadToMMLane(OWL::Lane* lane, int64_t odId)
{
    MentalModelLane mmLane(lane->GetId(), odId, lane->GetLength(), lane->GetWidth(0));

    //TODO Lanes posses only one succsessor!
    // change lane assignment
    //------------------------------------------------------
    OWL::CRoad* currentRoad = &lane->GetRoad();

    //TODO implement invalide lane (catch case: no further road exist)
    auto nextlane = lane->GetNext();
    if (!(nextlane.empty())) {
        mmLane.AddSuccessor(nextlane.front());
    } else {
        mmLane.AddSuccessor(OWL::InvalidId);
    }

    auto previouslane = lane->GetPrevious();
    if (!(previouslane.empty())) {
        mmLane.AddPredecessor(previouslane.front());
    } else {
        mmLane.AddPredecessor(OWL::InvalidId);
    }

    /** @addtogroup env_sensing
     * - Get for each lane on each section information about the geometrical characteristics (e.g. x,y,hdg tuples)
     */
    for(auto geometry : lane->GetLaneGeometryElements()) {
        double x = geometry->joints.current.points.reference.x;
        double y = geometry->joints.current.points.reference.y;
        double hdg = geometry->joints.current.sHdg;
        double s = geometry->joints.current.sOffset;

        mmLane.AddPoint(x, y, hdg, s);
    }

    /** @addtogroup env_sensing
     * - Add tuples to a mental description of the lane
     */
    if (lanes.find(odId)==lanes.end())
    {
        lanes.emplace(std::make_pair(odId, std::vector{mmLane}));
    }
    else
    {
        lanes[odId].push_back(mmLane);
    }
}

void Sensor_Modular_Driver_Implementation::SetEgoData()
{

    /** @addtogroup ego_sensing
     * - Collect the vehicle-type of the ego-vehicle
     * @code{.cpp}
     */
    AgentVehicleType VehicleType = vehicleParameters.vehicleType;
    /** @endcode */

    /** @addtogroup ego_sensing
     * - Collect the state-information of the ego vehicle for a container of type "state"
     * @code{.cpp}
     */
    state State;
    State.id                          = Agent->GetId();
    State.pos.xPos                    = Agent->GetPositionX();
    State.pos.yPos                    = Agent->GetPositionY();
    State.roadid                      = 0;
    State.drivingdirection            = 1;
    State.laneid                      = EgoAgent.GetLaneIdFromRelative(0);
    State.roadPos                     = EgoAgent.GetMainLocatePosition().roadPosition;
    State.velocity_x                  = Agent->GetVelocity(VelocityScope::DirectionX);
    State.velocity_y                  = Agent->GetVelocity(VelocityScope::DirectionY);
    State.acceleration_abs_inertial   = Agent->GetAcceleration();
    State.hornSwitch                  = Agent->GetHorn();
    State.headLightSwitch             = Agent->GetHeadLight();
    State.highBeamLightSwitch         = Agent->GetHighBeamLight();
    State.flasherSwitch               = Agent->GetFlasher();
    State.indicatorstate              = Agent->GetIndicatorState();
    State.brakeLight                  = Agent->GetBrakeLight();
    State.spawntime                   = SpawnTime;
    State.pos.yawAngle                = Agent->GetYaw();
    State.yaw_velocity                = Agent->GetYawRate();
    State.yaw_acceleration            = 0; //not implemented: agent->GetYawAcceleration();
    /** @endcode */

    State.collisionpartners           = Agent->GetCollisionPartners();

    /** @addtogroup ego_sensing
     * - Collect the state-information of the ego vehicle for a container of type "state_Ego" relative to the agent
     * @code{.cpp}
     */
    state_Ego State_Ego;
    State_Ego.velocity_long           = Agent->GetVelocity(VelocityScope::Longitudinal);
    State_Ego.velocity_lat            = Agent->GetVelocity(VelocityScope::Lateral);
    State_Ego.acceleration_long       = Agent->GetAcceleration(); //not implemented: agent->GetAccelerationX();
    State_Ego.acceleration_lat        = 0; //not implemented: agent->GetAccelerationY();
    State_Ego.currentGear             = Agent->GetGear();
    State_Ego.accelPedal              = Agent->GetEffAccelPedal();
    State_Ego.brakePedal              = Agent->GetEffBrakePedal();
    State_Ego.engineSpeed             = Agent->GetEngineSpeed();
    State_Ego.steeringWheelAngle      = Agent->GetSteeringWheelAngle();
    const LaneTypes acceptableLaneTypes {{LaneType::Driving, LaneType::OnRamp, LaneType::OffRamp, LaneType::Entry,
                                          LaneType::Exit}};
    State_Ego.distance_to_end_of_lane = EgoAgent.GetDistanceToEndOfLane(GetWorld()->GetVisibilityDistance(), 0,
                                                                        acceptableLaneTypes);
    /** @endcode */

    /** @addtogroup ego_sensing
     * - Set the collected information to the output-variable "Ego"
     * @code{.cpp}
     */
    Ego.SetState(State);
    Ego.SetState_Ego(State_Ego);
    Ego.SetVehicleType(VehicleType);
    /** @endcode */
}

void Sensor_Modular_Driver_Implementation::SetMovingObjects(const std::map<int, AgentInterface*> *Agents)
{
    /** @addtogroup surr_sensing
     * - Clear the Output Container
     */
    SurroundingMovingObjects.clear();

    /** @addtogroup surr_sensing
     * - Get the visibility distance (not so many agents will overload the memory)
     * - Sort agents by their distance to the egos road-position
     * - Select the 10 nearest agents (memory)
     */

    double sightdistance = GetWorld()->GetVisibilityDistance();
    std::list<std::pair<int,int>> AgentAndDist;

    double roadPos_s = GetAgent()->GetEgoAgent().GetMainLocatePosition().roadPosition.s;
    for (std::map<int, AgentInterface *>::const_iterator it = Agents->begin(); it != Agents->end(); it++)
    {
        double absdist2ego = abs(roadPos_s - it->second->GetEgoAgent().GetMainLocatePosition().roadPosition.s);
        AgentAndDist.push_back(std::make_pair(absdist2ego, it->first));
    }

    AgentAndDist.sort();
    auto it1 = AgentAndDist.begin();
    auto it2 = AgentAndDist.end();
    //size_t disttoend = AgentAndDist.size();
    //size_t maxsize = disttoend - 10;
    //if (maxsize > 0 )
    // size_t is unsigned --> this does not work
    if(AgentAndDist.size() > 10)
    {
        advance(it1, 10);
        AgentAndDist.erase(it1,it2);
    }

    /** @addtogroup surr_sensing
     * - Iterate through every remaining agent
     */
    for (std::list<std::pair<int,int>>::const_iterator i=AgentAndDist.begin(); i!=AgentAndDist.end(); i++)
    {
        AgentInterface* it = Agents->at(i->second);
        double absdist2ego = abs(EgoAgent.GetMainLocatePosition().roadPosition.s-it->GetEgoAgent().GetMainLocatePosition().roadPosition.s);
        if (it!=Agent &&
           (absdist2ego<sightdistance))
        {
            AgentInterface* surroundingAgent {it};
            EgoAgentInterface& surroundingEgoAgent {it->GetEgoAgent()};
            /** @addtogroup surr_sensing
             * - Collect the vehicle-type of the current-vehicle in the iterator
             * - Collect the state-information of the vehicle for a container of type "state"
             * - Collect the vehicle properties
             *
             * @code{.cpp}
             */
            AgentVehicleType VehicleType                      = vehicleParameters.vehicleType;

            State_Moving State;
            State.id                      = surroundingAgent->GetId();
            State.velocity_x              = surroundingAgent->GetVelocity(VelocityScope::DirectionX);
            State.velocity_y              = surroundingAgent->GetVelocity(VelocityScope::DirectionY);
            State.velocity_long           = surroundingAgent->GetVelocity(VelocityScope::Longitudinal);
            State.velocity_lat            = surroundingAgent->GetVelocity(VelocityScope::Lateral);
            State.pos.xPos                = surroundingAgent->GetPositionX();
            State.pos.yPos                = surroundingAgent->GetPositionY();
            State.pos.yawAngle            = surroundingAgent->GetYaw();
            State.yaw_velocity            = surroundingAgent->GetYawRate();
            State.roadPos                 = surroundingEgoAgent.GetMainLocatePosition().roadPosition;
            State.acceleration_long       = surroundingAgent->GetAcceleration();
            State.acceleration_lat        = 0;
            State.brakeLight              = surroundingAgent->GetBrakeLight();
            State.hornSwitch              = surroundingAgent->GetHorn();
            State.flasherSwitch           = surroundingAgent->GetFlasher();
            State.headLightSwitch         = surroundingAgent->GetHeadLight();
            State.highBeamLightSwitch     = surroundingAgent->GetHighBeamLight();
            State.indicatorstate          = surroundingAgent->GetIndicatorState();
            State.drivingdirection        = 1;
            State.laneid                  = surroundingEgoAgent.GetLaneIdFromRelative(0);
            State.secondarycoveredlanes   = {}; //!!! surroundingAgent->GetSecondaryCoveredLanes(); Gibts nimma

            properties Properties;
            Properties.lx                       = surroundingAgent->GetLength();
            Properties.ly                       = surroundingAgent->GetWidth();
            Properties.lz                       = surroundingAgent->GetHeight();
            Properties.distanceReftoLeadingEdge = surroundingAgent->GetDistanceReferencePointToLeadingEdge();
            Properties.trackWidth               = vehicleParameters.trackwidth;
            Properties.heightCOG                = vehicleParameters.heightCOG;
            Properties.wheelbase                = vehicleParameters.wheelbase;

            /** @endcode */

            SurroundingMovingObjectsData CurrentMovingObject(VehicleType, State, Properties);
            CurrentMovingObject.SetDistanceToEgo(absdist2ego);

            /** @addtogroup surr_sensing
             * - Add current agent in the iterator to the ouput container "SurroundingMovingObjects"
             */
            SurroundingMovingObjects.push_back(CurrentMovingObject);
        }
    }
}

void Sensor_Modular_Driver_Implementation::GetTrafficRuleInformation()
{

    /** @addtogroup env_sensing
     * - Select neccessary traffic-rule information about every lane
     *  * visibility distance
     *  * traffic signs
     *  * lane markings
     */
    const double visibilityDistance = GetWorld()->GetVisibilityDistance();
    StaticEnvironment.trafficRuleInformation.laneEgo    = GetTrafficRuleLaneInformationEgo();
    StaticEnvironment.trafficRuleInformation.laneLeft   = GetTrafficRuleLaneInformationLeft();
    StaticEnvironment.trafficRuleInformation.laneRight  = GetTrafficRuleLaneInformationRight();
    StaticEnvironment.trafficRuleInformation.laneMarkingsLeft = EgoAgent.GetLaneMarkingsInRange(visibilityDistance, Side::Left, 0);
    StaticEnvironment.trafficRuleInformation.laneMarkingsRight = EgoAgent.GetLaneMarkingsInRange(visibilityDistance, Side::Right, 0);

    const auto laneIntervals = EgoAgent.GetRelativeLanes(0.0);
    if (!laneIntervals.empty())  // TODO This will be done again for lane existence in road geometry stuff. Reorder or make member to reduce redundency?
    {
        // Left side
        int relativeLaneId = 1;
        const auto& relativeLanes = laneIntervals.front().lanes;
        if (std::find_if(relativeLanes.cbegin(),
                         relativeLanes.cend(),
                         [relativeLaneId](const auto& relativeLane) {
                             return relativeLane.relativeId == relativeLaneId;
                         }) != relativeLanes.cend())
        {
            StaticEnvironment.trafficRuleInformation.laneMarkingsLeftOfLeftLane = EgoAgent.GetLaneMarkingsInRange(visibilityDistance, Side::Left, relativeLaneId);
        }

        // Right side
        relativeLaneId = -1;
        if (std::find_if(relativeLanes.cbegin(),
                         relativeLanes.cend(),
                         [relativeLaneId](const auto& relativeLane) {
                             return relativeLane.relativeId == relativeLaneId;
                         }) != relativeLanes.cend())
        {
            StaticEnvironment.trafficRuleInformation.laneMarkingsLeftOfLeftLane = EgoAgent.GetLaneMarkingsInRange(visibilityDistance, Side::Right, relativeLaneId);
        }
    }
}

LaneInformationTrafficRules Sensor_Modular_Driver_Implementation::GetTrafficRuleLaneInformationEgo()
{
    LaneInformationTrafficRules laneInformation;
    const double visibilityDistance = GetWorld()->GetVisibilityDistance();

    laneInformation.trafficSigns = GetAgent()->GetEgoAgent().GetTrafficSignsInRange(visibilityDistance);

    return laneInformation;
}

LaneInformationTrafficRules Sensor_Modular_Driver_Implementation::GetTrafficRuleLaneInformationLeft()
{
    LaneInformationTrafficRules laneInformation;
    const int relativeLaneId = 1;
    const double visibilityDistance = GetWorld()->GetVisibilityDistance();

    laneInformation.trafficSigns = GetAgent()->GetEgoAgent().GetTrafficSignsInRange(visibilityDistance, relativeLaneId);

    return laneInformation;
}

LaneInformationTrafficRules Sensor_Modular_Driver_Implementation::GetTrafficRuleLaneInformationRight()
{
    LaneInformationTrafficRules laneInformation;
    const int relativeLaneId = -1;
    const double visibilityDistance = GetWorld()->GetVisibilityDistance();

    laneInformation.trafficSigns = GetAgent()->GetEgoAgent().GetTrafficSignsInRange(visibilityDistance, relativeLaneId);

    return laneInformation;
}

void Sensor_Modular_Driver_Implementation::GetRoadGeometry()
{
    /** @addtogroup env_sensing
     * - Select preprocessed information about the road like:
     *  * curvature
     *  * width
     *  * distance to the end of the lane
     *  * lane-id
     */
    StaticEnvironment.roadGeometry.visibilityDistance            = GetWorld()->GetVisibilityDistance();
    GetGeometryLaneInformationEgo(StaticEnvironment.roadGeometry.laneEgo);
    GetGeometryLaneInformationLeft(StaticEnvironment.roadGeometry.laneLeft);
    GetGeometryLaneInformationRight(StaticEnvironment.roadGeometry.laneRight);
    StaticEnvironment.numberoflanes = EgoAgent.GetRelativeLanes(0.).at(0).lanes.size();
}

void Sensor_Modular_Driver_Implementation::GetGeometryLaneInformationEgo(LaneInformation &laneInformation)
{
    const double visibilityDistance = GetWorld()->GetVisibilityDistance();
    laneInformation.exists                  = true;     // Ego lane must exist by definition, or else vehicle would have despawned by now. Information not necessary atm!
    if (laneInformation.exists)
    {
        const int mainLaneId {EgoAgent.GetLaneIdFromRelative(0)};
        if (laneInformation.laneid != mainLaneId)
        {
            laneInformation.laneid          = mainLaneId;
            laneInformation.mentalModelLanes= lanes[(laneInformation.laneid)];
        }
        laneInformation.curvature           = EgoAgent.GetLaneCurvature();
        laneInformation.width               = EgoAgent.GetLaneWidth();
        const LaneTypes acceptableLaneTypes {{LaneType::Driving, LaneType::OnRamp, LaneType::OffRamp, LaneType::Entry,
                                              LaneType::Exit}};
        laneInformation.distanceToEndOfLane = EgoAgent.GetDistanceToEndOfLane(visibilityDistance, 0, acceptableLaneTypes);
        laneInformation.laneid              = mainLaneId;
    }
    else
    {
        laneInformation = {};
    }
}

void Sensor_Modular_Driver_Implementation::GetGeometryLaneInformationLeft(LaneInformation &laneInformation)
{
    const int relativeLaneId = 1;
    const double visibilityDistance = GetWorld()->GetVisibilityDistance();

    laneInformation.exists = false;

    const auto laneIntervals = EgoAgent.GetRelativeLanes(0.0);

    if (!laneIntervals.empty())
    {
        const auto& relativeLanes = laneIntervals.front().lanes;

        if (std::find_if(relativeLanes.cbegin(),
                         relativeLanes.cend(),
                         [relativeLaneId](const auto& relativeLane) {
                             return relativeLane.relativeId == relativeLaneId;
                         }) != relativeLanes.cend())
        {
            laneInformation.exists = true;
            const int laneId {EgoAgent.GetLaneIdFromRelative(1)};
            if (laneInformation.laneid != laneId)
            {
                laneInformation.laneid          = laneId;
                laneInformation.mentalModelLanes= lanes[(laneInformation.laneid)];
            }
            laneInformation.curvature               = EgoAgent.GetLaneCurvature(relativeLaneId);
            laneInformation.width                   = EgoAgent.GetLaneWidth(relativeLaneId);
            const LaneTypes acceptableLaneTypes {{LaneType::Driving, LaneType::OnRamp, LaneType::OffRamp, LaneType::Entry,
                                                  LaneType::Exit}};
            laneInformation.distanceToEndOfLane     = EgoAgent.GetDistanceToEndOfLane(visibilityDistance, relativeLaneId,
                                                                                      acceptableLaneTypes);
        }        
    }
    else
    {
        laneInformation = {};
    }
}

void Sensor_Modular_Driver_Implementation::GetGeometryLaneInformationRight(LaneInformation &laneInformation)
{
    const int relativeLaneId = -1;
    const double visibilityDistance = GetWorld()->GetVisibilityDistance();

    laneInformation.exists = false;

    const auto laneIntervals = EgoAgent.GetRelativeLanes(0.0);

    if (!laneIntervals.empty())
    {
        const auto& relativeLanes = laneIntervals.front().lanes;

        if (std::find_if(relativeLanes.cbegin(),
                         relativeLanes.cend(),
                         [relativeLaneId](const auto& relativeLane) {
                             return relativeLane.relativeId == relativeLaneId;
                         }) != relativeLanes.cend())
        {
            laneInformation.exists = true;
            const int laneId {EgoAgent.GetLaneIdFromRelative(-1)};
            if (laneInformation.laneid != laneId)
            {
                laneInformation.laneid          = laneId;
                laneInformation.mentalModelLanes= lanes[(laneInformation.laneid)];
            }
            laneInformation.curvature               = EgoAgent.GetLaneCurvature(relativeLaneId);
            laneInformation.width                   = EgoAgent.GetLaneWidth(relativeLaneId);
            const LaneTypes acceptableLaneTypes {{LaneType::Driving, LaneType::OnRamp, LaneType::OffRamp, LaneType::Entry,
                                                  LaneType::Exit}};
            laneInformation.distanceToEndOfLane     = EgoAgent.GetDistanceToEndOfLane(visibilityDistance, relativeLaneId,
                                                                                      acceptableLaneTypes);
        }      
    }
    else
    {
        laneInformation = {};
    }
}
