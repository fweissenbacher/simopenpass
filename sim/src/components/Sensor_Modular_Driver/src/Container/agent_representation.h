/******************************************************************************
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file    agent_representation.h
//! @author  Christian Siebke
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief provide container structure of agents seen by the driver
//!

# pragma once
 
#include "ContainerStructures.h"
 
class AgentRepresentation
{
public:
 
    AgentRepresentation(SurroundingMovingObjectsData in_perceptionData, int in_cycletime) :
       internal_Data(*in_perceptionData.GetVehicleType(),
                     *in_perceptionData.GetState(),
                     *in_perceptionData.GetProperties()),
                     cycletime{in_cycletime}
    {
        lifetime = 0 ;
    }
 
    AgentRepresentation(const AgentRepresentation&) = delete;
    AgentRepresentation(AgentRepresentation&&) = delete;
    AgentRepresentation& operator=(const AgentRepresentation&) = delete;
    AgentRepresentation& operator=(AgentRepresentation&&) = delete;
    ~AgentRepresentation() = default;
 
    //!
    //! \brief Extrapolate
    //! Extrapolate the internal_Data of an agent if he is not fixated
    //!
    //! \param _roadPerceptionData
    //!
    void Extrapolate(const StaticEnvironmentData & _roadPerceptionData);
 
    //!
    //! \brief PositionAgentOnLanes
    //! Sets the current agent on the mental representation of the lanes while creating a container of mental-model-lanes (one id with each several sections)
    //! \param laneid
    //! \param Lanes map with laneid and the mental-model-lane
    //! \param _roadPerceptionData
    //!
    void PositionAgentOnLanes(int laneid, std::map<int, const std::vector<MentalModelLane>*>*Lanes, const StaticEnvironmentData &_roadPerceptionData);

    //!
    //! \brief ExtrapolateKinematic
    //! Extrapolates the in the last timestep detected agent only kinematic with their last kinematic parameters
    //!
    void ExtrapolateKinematic();

    //!
    //! \brief ExtrapolateAlongLane
    //! Extrapolates the in the last timestep detected agent along his lane
    //!
    //! \param lane
    //!
    void ExtrapolateAlongLane(const std::vector<MentalModelLane> *lane);
    
    //!
    //! \brief LinkPosToRoadPos
    //! links the current intertial position to the road position (s, t)
    //!
    //! \param laneid
    //! \param lanes
    //!
    void LinkPosToRoadPos(int laneid, std::map<int, const std::vector<MentalModelLane*>> lanes);

    //!
    //! \brief LinearInterpolation
    //!
    //! linear interpolation between two values along two s-values at special value s
    //!
    //! \param a value a
    //! \param sa s-position at value a
    //! \param b value b
    //! \param sb s-position at value b
    //! \param s s-position where the output variable is searched
    //! \return value between a and b at position of s
    //!
    double LinearInterpolation(double a, double sa, double b, double sb, double s)
    {
        double div = (b-a)/(sb-sa);
        double c = div*s + (-(div)*sa + a);
        return c;
    }

    //!
    //! \brief MisjudgePosition
    //!
    //! Misjudge the position of the agent by a given value.
    //!
    //!  \param ds
    //!
    void MisjudgePosition(double ds)
    {
        if(lifetime == 0)
        {
            internal_Data.GetState()->pos.xPos += sin(internal_Data.GetState()->pos.yawAngle) * ds;
            internal_Data.GetState()->pos.yPos += cos(internal_Data.GetState()->pos.yawAngle) * ds;
            internal_Data.GetState()->roadPos.s = internal_Data.GetState()->roadPos.s + ds;
        }
    }

    //!
    //! \brief MisjudgeVelocity
    //!
    //! Misjudge the velocity of the agent by a given value.
    //! If the difference of the velocity in the last time-step isn't more than 10% it isn't changed
    //!
    //!  \param dv
    //!
    void MisjudgeVelocity(double dv)
    {
        if(lifetime == 0)
        {
            if(abs(last_v-internal_Data.GetState()->velocity_long)/internal_Data.GetState()->velocity_long >= 0.1)
            {
                last_v = internal_Data.GetState()->velocity_long + dv;
            }
            internal_Data.GetState()->velocity_long = last_v;
            internal_Data.GetState()->velocity_abs = last_v;
            internal_Data.GetState()->velocity_x = (last_v) * sin(internal_Data.GetState()->pos.yawAngle);
            internal_Data.GetState()->velocity_y = (last_v) * cos(internal_Data.GetState()->pos.yawAngle);
        }
    }

    //!
    //! \brief MisjudgeAcceleration
    //!
    //! Misjudge the acceleration of the agent by a given value.
    //! If the difference of the acceleration in the last time-step isn't more than 10% it isn't changed
    //!
    //!  \param da
    //!
    void MisjudgeAcceleration(double da)
    {
        if(lifetime == 0)
        {
            if(abs(last_a-internal_Data.GetState()->acceleration_long)/internal_Data.GetState()->acceleration_long >= 0.1)
            {
                last_a = internal_Data.GetState()->acceleration_long + da;
            }
            internal_Data.GetState()->acceleration_long = last_a;
            internal_Data.GetState()->acceleration_abs_inertial = last_a;
        }
    }

    //!
    //! \brief LifeTimeTicker
    //! increase the life time of the agent representation by one cycletime
    //!
    void LifeTimeTicker() {
       lifetime += Get_cycletime();
    }
 
    //*********Set-functions****//

    void SetRelativeDsError(double ds)
    {
        ds_error = ds;
    }

    void SetRelativeDvError(double dv)
    {
        dv_error = dv;
    }

    void SetRelativeDaError(double da)
    {
        da_error = da;
    }

    //*********Get-functions****//
 
    //!
    //! \brief Get_internal_Data
    //! \return internal data of the mental model
    //!
    SurroundingMovingObjectsData Get_internal_Data() const {
        return internal_Data;
    }
 
    //!
    //! \brief Get_cycletime
    //! \return current cycle-time
    //!
    int Get_cycletime() {
        return cycletime;
    }
 
    //!
    //! \brief Get_LifeTime
    //! \return current life-time of the mental represented agent
    //!
    int Get_LifeTime() {
        return lifetime ;
    }

    double GetRelativeDsError()
    {
        return ds_error;
    }
    double GetRelativeDvError()
    {
        return dv_error;
    }
    double GetRelativeDaError()
    {
        return da_error;
    }

 
private:  
    //! the internal information of the agent representation
    SurroundingMovingObjectsData internal_Data;
 
    //!  represents the time since the agent representation is in the MentalModel (since the agent_representation exists)
    int lifetime;
 
    //! cycletime of the component MentalModel
    int cycletime;

    double ds_error = 0; /***< last misjudgement of the s-Position */
    double dv_error = 0; /***< last misjudgement of the velocity */
    double da_error = 0; /***< last misjudgement of the acceleration */
    double last_v = 0; /***< last velocity */
    double last_a = 0; /***< last acceleration */
 
};
