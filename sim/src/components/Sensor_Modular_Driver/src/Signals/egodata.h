/******************************************************************************
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  egodata.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief provide Container Structures for the driver's cognition of ego-informations
//!
//-----------------------------------------------------------------------------

//! \brief This file contains the container descriptions of the environment data for the sensor
//!
//! \ingroup Sensor_Modular_Driver
//! \defgroup Ego_Data
//! @{
//!

#pragma once

#include <vector>
#include <string>
#include <set>
#include <list>
#include "common/globalDefinitions.h"
#include "common/worldDefinitions.h"


//!
//! \brief The state struct
//! absolute kinematic values in the global coordinate system
//!
struct state
{

    bool hornSwitch = false; /**< Activation of HornSwitch [-] */
    bool headLightSwitch = false; /**< Activation of Headlight [-]. */
    bool highBeamLightSwitch = false; /**< Activation of Highbeam Light [-]. */
    bool flasherSwitch = false; /**< Activation of Flasher [-]. */
    bool brakeLight = false; /**< Activation of Break Light [-]. */

    int id {-999}; /**< Id of the agent */
    int roadid {-999}; /**< Id of the road the agent is on */
    int laneid {-999}; /**< Id of the lane the agent is on*/
    int drivingdirection {-999}; /**< Direction the agent is driving on */
    int spawntime {-999}; /**< timestep [ms] the agent was spawn [ms] */

    double velocity_x {-999}; /**< velocity in inertial x-direction [m/s] */
    double velocity_y {-999}; /**< velocity in inertial y-direction [m/s] */
    double velocity_abs {-999}; /**< absolute velocity [m/s] */
    double acceleration_abs_inertial {-999}; /**< absolute acceleration [m/s²] */
    double yaw_velocity {-999}; /**< yaw velocity [rad/s] */
    double yaw_acceleration {-999}; /**< yaw acceleration [rad/s²] */

    IndicatorState indicatorstate = IndicatorState::IndicatorState_Off; /**< Current indicator state [-] */
    std::set<int> secondarycoveredlanes = {}; /**< Lane-Ids of secondary covered lanes [-] */
    Position pos; /**< Current Position: x, y, yaw and curvature*/
    RoadPosition roadPos; /**< Current road-position: s, t and hdg */

    std::vector<std::pair<ObjectTypeOSI, int>> collisionpartners{}; /**< Current collision partners */
};

//!
//! \brief The state_Ego struct
//! relative kinematic values in the agent coordinate system
//!
struct state_Ego
{
    int currentGear {-999}; /**< Current gear */
    double velocity_long {-999}; /**< velocity in longitudinal agent-direction [m/s] */
    double velocity_lat {-999}; /**< velocity in lateral agent-direction [m/s] */
    double acceleration_long {-999}; /**< acceleration in longitudinal agent-direction [m/s²] */
    double acceleration_lat {-999}; /**< acceleration in lateral agent-direction [m/s²] */
    double steeringWheelAngle {-999}; /**< steering wheel angle */
    double accelPedal {-999}; /**< acceleration pedal value [-] */
    double brakePedal {-999}; /**< brake pedal value [-] */
    double engineSpeed {-999}; /**< engine speed */
    double distance_to_end_of_lane {-999}; /**< current distance to end of the lane */
};

//!
//! \brief The Color struct
//! color specified in the RGB color space
//!
struct Color
{
    double r {-999}; /**< red value */
    double g {-999}; /**< green value */
    double b {-999}; /**< blue value */
};

//!
//! \brief The driverInformation struct
//! Specifies the inner wishes of the current driver
//!
struct driverInformation
{
    // Ego-Vehicle Limits
    double *v_y_Max; /**< maximum y-velcity in agent-direction */

    // Ego-Driver Information
    double *MinGap; /**< minimum accepted gap to another agent */
    double *v_Wish; /**< velocity wish of the agent */
    double *thw_Wish; /**< time-headway wish of the agent */
    double *commonSpeedLimit_Violation; /**< accepted violation of the speed-limit */
    double *ttc_threshold; /**< current threshold of the time-to-collision under which an emergency brake can be initialized */
};

//!
//! \brief The egoData class
//! This class combines all the relevant containers with ego-information
//!
class  egoData
{
public:
    egoData()
    {
    }

    std::string GetName() const
    {
        return  "egoData*";
    }

    AgentVehicleType *GetVehicleType()
    {
        return &VehicleType;
    }
    state_Ego *GetState_Ego()
    {
        return &State_Ego;
    }
    state *GetState()
    {
        return &State;
    }
    void SetVehicleType(AgentVehicleType VehicleType)
    {
        this->VehicleType = VehicleType;
    }
    void SetState_Ego(state_Ego State_Ego)
    {
        this->State_Ego = State_Ego;
    }
    void SetState(state State)
    {
        this->State = State;
    }
    void SetDriverInformation(driverInformation DriverInformation)
    {
        this->DriverInformation = DriverInformation;
    }
    driverInformation *GetDriverInformation()
    {
        return &DriverInformation;
    }

private:

    AgentVehicleType VehicleType; /**< Vehicle type of the agent */
    state_Ego State_Ego; /**< Container of the relative kinematic values in the agent coordinate system */
    state State; /**< Container of the absolute kinematic values in the global coordinate system */

    driverInformation DriverInformation {}; /**< Container of the inner wishes of the current driver */
};

/** @} */
