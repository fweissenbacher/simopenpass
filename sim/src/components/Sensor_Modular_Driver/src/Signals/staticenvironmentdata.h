/******************************************************************************
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
* Copyright (c) 2021 HLRS, Universitaet Stuttgart
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  staticenvironmentdata.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief provide Container Structures for the driver's cognition of the static-environment
//!
//-----------------------------------------------------------------------------

//! \brief This file contains the container descriptions of the environment data for the sensor
//!
//! \ingroup Sensor_Modular_Driver
//! \defgroup Static_Environment_Data
//! @{
//!

#pragma once

#include <vector>
#include <string>
#include "common/globalDefinitions.h"
#include <include/signalInterface.h>
#include "egodata.h"
#include "../../../Sensor_Driver/src/Signals/sensor_driverDefinitions.h"
#include "../Container/MentalModelLane.h"

class MentalModelLane;

/**
 * @brief The ObjectTypes enum
 *
 * Sensor Informationen related to object of type ...
 */
enum ObjectTypes
{
    Unknown,        /**< Type is unknown */
    TrafficSign,    /**< Object is a traffic sign*/
    RoadMarking,    /**< Object is a road marking*/
    Building,       /**< Object is a building*/
    Plant           /**< Object is a plant*/
};

/**
 * @brief The Dimension struct
 *
 * Describes the geometrical dimensions of an object
 */
struct Dimension //dimension of the object related to the sensor coordinate system
{
    double lx; /**< length in x direction */
    double ly; /**< length in y direction */
    double lz; /**< length in z direction */
};

/**
 * @brief The TrafficSignInformation struct
 */
struct TrafficSignInformation
{
    std::string GetName() const
    {
      return  "TrafficSignInformation";
    }
};

/**
 * @brief The PrecipitationType enum
 *
 * Describes the types of precipitation
 */
enum class PrecipitationType
{    
    None,       /**< None */
    Raindrops,  /**< Raindrops */
    IcePellets, /**< IcePellets */
    Hail,       /**< Hail */
    Snowflakes, /**< Snowflakes */
    DiamondDust /**< DiamondDust */
};

/**
 * @brief The Weather struct
 *
 * Describes the current weather situation
 */
struct Weather
{
    std::string GetName() const
    {
      return  "Weather";
    }

    int temperature;                        /**< Temp. in °C */
    PrecipitationType precipitationType;    /**< Type of the precipitation */
    int precipitation;                      /**< amount of precipitation in l/m² */
};

/**
 * @brief The Environment struct
 *
 * Describes the current environmental conditions
 */
struct  Environment
{
    std::string GetName() const
    {
      return  "Environment";
    }

    Weather weather;        /**< Weather object */
    Weekday weekday;        /**< Weekday object */
    double frictionCoeff;   /**< friction coefficient */
};

/**
 * @brief The StaticObject struct
 *
 * Describes the characteristics of a static object
 */
struct StaticObject
{
    std::string GetName() const
    {
      return  "StaticObject";
    }

    ObjectType type;        /**< ObjectType object */
    ObjectPosition pos_abs; /**< ObjectPosition object */
    Dimension dimension;    /**< Dimension object */
};

/**
 * @brief The LaneInformation struct
 *
 * Describes the characteristics of a lane (inherited by LaneInformationGeometry)
 * with additional information for the modular driver
 *
 */
struct LaneInformation:LaneInformationGeometry
{

    int laneid;                                     /**< Lane-id of regarded lane */
    std::vector<MentalModelLane> mentalModelLanes;  /**< representation of a lane in the cognitive processes */
};

/**
 * @brief The RoadGeometry struct
 *
 * Describes the characteristics of a road
 *
 */
struct RoadGeometry
{
    //! Current maximum visibility distance as specified by the world
    double visibilityDistance {-999.0};
    //! Data about the lane to left (in driving direction) of the mainLane
    LaneInformation laneLeft;
    //! Data about the lane the where the middle of the front of the agent is (i.e. mainLane)
    LaneInformation laneEgo;
    //! Data about the lane to right (in driving direction) of the mainLane
    LaneInformation laneRight;
};

/**
 * @brief The StaticEnvironmentData struct
 *
 * Combines the information about the static environment
 *
 */
struct  StaticEnvironmentData
{
    std::string GetName() const
    {
      return  "StaticEnvironmentData";
    }

    int numberoflanes;                              /**< number of lanes */
    std::vector<StaticObject> staticObjects;        /**< vector of staticObjects containers */
    RoadGeometry roadGeometry;                      /**< RoadGeometry Container */
    TrafficRuleInformation trafficRuleInformation;  /**< Traffic rule information */
    Environment environment;                        /**< Environment container */
};

/** @} */
