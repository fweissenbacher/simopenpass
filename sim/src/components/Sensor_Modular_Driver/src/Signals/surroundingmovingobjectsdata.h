/******************************************************************************
* Copyright (c) 2019 TU Dresden
* Copyright (c) 2019 AMFD GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*****************************************************************************/

//-----------------------------------------------------------------------------
//! @file  staticenvironmentdata.h
//! @author  Konstantin Blenz
//! @date    Tue, 03.12.2019
//! @brief provide Container Structures for the driver's cognition of the moving-environment
//!
//-----------------------------------------------------------------------------

#pragma once

#include <vector>
#include <string>
#include <math.h>
#include "common/globalDefinitions.h"
#include "egodata.h"

//! \brief This file provides Container Structures for the driver's cognition of the moving-environment
//!
//! \ingroup Sensor_Modular_Driver
//! \defgroup Surrounding_Moving_Objects
//! @{
//!

//!
//! \brief The properties struct
//! dimension of the object related to the sensor coordinate system
//!
struct properties
{
    double lx {-999}; /**< length in x-direction [m] */
    double ly {-999}; /**< length in y-direction [m] */
    double lz {-999}; /**< length in z-direction [m] */
    double weight {-999}; /**< weight of the object [kg] */
    double heightCOG {-999}; /**< heigth of the center of gravity [m] */
    double wheelbase {-999}; /**< wheelbase if there is one [m] */
    double momentInertiaRoll {-999}; /**< moment of inertia in roll direction */
    double momentInertiaPitch {-999}; /**< moment of inertia in pitch direction */
    double momentInertiaYaw {-999}; /**< moment of inertia in yaw direction */
    double frictionCoeff {-999}; /**< friction coefficient */
    double trackWidth {-999}; /**< track width [m] */
    double distanceReftoLeadingEdge {-999}; /**< distance of the reference-point to the leading edge [m] */
};

//!
//! \brief The RelationType enum
//! Relation type of the considered agent in relation to the current agent
//!
enum RelationType
{
    NONE = 0, /**< no relation known */
    Leader = 1, /**< Leader on the agent-lane*/
    Follower = 2, /**< Follower on the agent-lane */
    LeaderRight = 3, /**< Leader on the right-lane */
    LeaderLeft = 4, /**< Leader on the left-lane */
    FollowerRight = 5, /**< Follower on the right-lane */
    FollowerLeft = 6 /**< Follower on the left-lane */
};

//!
//! \brief The State_Moving struct
//! Additional values to the current kinematic state of the viewed agent
//!
struct State_Moving : state
{
    double velocity_long {-999}; /**< velocity in longitudinal agent-direction [m/s] */
    double velocity_lat {-999}; /**< velocity in lateral agent-direction [m/s] */
    double acceleration_long {-999}; /**< acceleration in longitudinal agent-direction [m/s²] */
    double acceleration_lat = 0; /**< acceleration in lateral agent-direction [m/s²] */
};

//!
//! \brief The SurroundingMovingObjectsData class
//! This class combines all the relevant containers with information about surrounding moving objects
//!
class SurroundingMovingObjectsData
{
public:
    SurroundingMovingObjectsData(AgentVehicleType VehicleType,
                                 State_Moving State,
                                 properties Properties):
        VehicleType(VehicleType),
        State(State),
        Properties(Properties)
    {
    }

    std::string GetName() const
    {
      return  "SurroundingMovingObjectsData";
    }
    AgentVehicleType *GetVehicleType()
    {
        return &VehicleType;
    }
    State_Moving *GetState()
    {
        return &State;
    }
    properties *GetProperties()
    {
        return &Properties;
    }
    void SetVehicleType(AgentVehicleType VehicleType)
    {
        this->VehicleType = VehicleType;
    }
    void SetState(State_Moving State)
    {       
        this->State = State;
    }
    void SetProperties(properties Properties)
    {
        this->Properties = Properties;
    }
    void SetDistanceToEgo(double DistanceToEgo)
    {
        this->DistanceToEgo = DistanceToEgo;
    }
    double const *GetDistanceToEgo()
    {
        return &DistanceToEgo;
    }

private:

    AgentVehicleType VehicleType; /**< Vehicle type of the current moving object */
    State_Moving State; /**< Container of the absolute kinematic values in the global coordinate system of the current moving object */
    properties Properties; /***< Container of the properties of the current moving object */

    double DistanceToEgo; /***< absolute distance of the current moving object to the current agent */
};

/** @} */
