/*********************************************************************
* Copyright (c) 2016 ITK Engineering GmbH
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/


//-----------------------------------------------------------------------------
//! @file  sensor_Modular_Driver.h
//! @brief Interface-file for class Sensor_Modular_Driver.
//!
//! This class is responsible for the creation and deletion of an
//! sensor module.
//-----------------------------------------------------------------------------

#ifndef SENSOR_MODULAR_DRIVER_H
#define SENSOR_MODULAR_DRIVER_H

#include "src/sensor_Modular_Driver_global.h"
#include "include/modelInterface.h"

#endif // SENSOR_MODULAR_DRIVER_H
