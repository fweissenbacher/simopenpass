@echo off


if defined OPENPASS_HOME goto OPENPASS_HOME_IS_OK
REM to avoid problems with closing rounded bracket in IF-clause
REM work with GOTOs to set missing OPENPASS_HOME

REM current working path looks like OPENPASS_HOME
if exist "%CD%"\scripts\windowsEnv.bat echo setting OPENPASS_HOME to %CD%
if exist "%CD%"\scripts\windowsEnv.bat set OPENPASS_HOME=%CD%
if exist "%CD%"\scripts\windowsEnv.bat goto OPENPASS_HOME_IS_OK

REM see, if path where this script is called looks like OPENPASS_HOME
REM accept path if common-base.bat seems to be at the right place
if exist "%~dp0"\scripts\windowsEnv.bat echo setting OPENPASS_HOME to %~dp0
if exist "%~dp0"\scripts\windowsEnv.bat set OPENPASS_HOME=%~dp0
if exist "%~dp0"\scripts\windowsEnv.bat goto OPENPASS_HOME_IS_OK

REM else no suitable OPENPASS_HOME found; abort
echo OPENPASS_HOME is not set
pause
goto END

:OPENPASS_HOME_IS_OK


set "QT_HOME=%OPENPASS_HOME%\lib"
set "QT_SHAREDHOME=%OPENPASS_HOME%\lib"
set "QTDIR=%OPENPASS_HOME%\lib"
set "PATH=%OPENPASS_HOME%\lib;%OPENPASS_HOME%\bin;%OPENPASS_HOME%;%PATH%"
set "QT_QPA_PLATFORM_PLUGIN_PATH=%OPENPASS_HOME%\lib\plugins\platforms"   rem tested for qt5 on win7, visual studio 2010


:END
