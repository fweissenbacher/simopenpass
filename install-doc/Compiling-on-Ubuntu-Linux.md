# Compiling openPASS on Linux (Ubuntu)
This is a guide on how to compile the sim@openPASS platform on a native Linux/Ubuntu system. It will also give insight into some potential pitfalls on the way to a running openPASS installation.

## Troubleshooting

### Runtime Error "Cannot load library 'World_OSI'..."
This error appears when `QLibrary` is unable to dynamically load the `World_OSI` shared library at runtime of the program (i.e. OpenPassSlave). The corresponding funtion, `QLibrary.load()`, looks for the library (`libWorld_OSI.so`) in folders on the system path (`PATH`) or listed in `LD_LIBRARY_PATH`. Unless the user specifically includes the location of `libWorld_OSI.so` in said paths, QLibrary cannot find the library and throws the above mentioned error message.

An elegant fix to this problem exists: Hard-coding the library paths into the `RPATH` (short for *runpath*) of the executable. We can enable this feature in CMake by setting `CMAKE_INSTALL_RPATH_USE_LINK_PATH` to `TRUE`. This should trigger the compiler to write the paths to all necessary libraries into the ELF header (wikipedia)[https://en.wikipedia.org/wiki/Executable_and_Linkable_Format] of the executable, where it should be found by dynamic loaders.
  
Unfortunately, there is a catch/glitch: Newer c++/g++ compilers (at least since gcc-9.3 onwards) store the paths to the libraries in `RUNPATH` instead of the desired `RPATH`! This is caused by the compiler flag `--enable-new-dtags` which is active by default on newer Linux/Ubuntu platforms. Usually, this is not much of an issue, as both `RPATH` and `RUNPATH` should searched by dynamic linkers, but it appears that QLibrary only checks `RPATH` and hence fails to locate the library it was looking for. 

Until Qt changes its policy with regard to `RUNPATH`, there are, to our knowledge, only two solutions to this problem:
* A) Set `LD_LIBRARY_PATH` whenever you run OpenPass (or any of the unit- or integration tests)  or
* B) Force the linker to write to `RPATH` by setting the linker flag `-\Wl,--disable-new-dtypes`.

For more information on the difference between `RPATH` and `RUNPATH`, see https://wiki.debian.org/RpathIssue and  http://blog.tremily.us/posts/rpath/.
