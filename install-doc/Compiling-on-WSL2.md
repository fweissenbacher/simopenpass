# Compiling openPASS on WSL2
This is a guide on how to compile the sim@openPASS platform on Windows Subsystem Linux version 2.

## Prerequisites
(WSL = Linux Subsystem on windows)
WSL-Version: 2 (but also works on WSL v1)
Linux Distro: Ubuntu 20.04 LTS

Required are:
* anaconda or miniconda
* cmake
* doxygen
* make
* A C++ compiler (f.i. g++)
* Text editor (nano, vim,...)
* Qt5

These requirements can be met by running

`
sudo apt install miniconda doxygen cmake make g++ nano qtbase5-dev
`

## 1) Create the *openpass* conda environment

### From scratch
Create a conda environment with name *openpass* and support for Python-3.8 via the following command

 `conda create -n openpass python=3.8`
 
Don't forget to activate the new environment via

`conda activate openpass`

The command prompt in BASH should now be proceeded by `'(openpass)'`, which indicates that *openpass* is the active conda environment.

Next, install the following python packages
* protobuf
* boost

via the command

`conda install <package-name>`

In order to install a specific version of a package, instead of the latest, use

`conda install <package-name>=X.Y.Z`

To show a list of available versions, run
`conda search <package-name>`

Install the FMI library from conda-forge
`conda install -c conda-forge fmilib`

### Using the pre-configured yml file
Instead of creating the `openpass` environment step by step, you can use a preconfigured .yml file that contains a list of all needed conda packages. The corresponding file for `openpass` is named `openpass-conda-env.yml` and can be found in the root folder of the simopenpass-fork repo.

Running the command below will create a fully configured `openpass` environment in the default location (either `$HOME/miniconda3/envs/openpass` or `$HOME/anaconda3/envs/openpass`) 
`
conda env create --file openpass-conda-env.yml
`


## 2) GoogleTest (gtest & gmock)
Clone the googletest repo:
`git clone https://github.com/google/googletest.git`

Enter the 'googletest' repo, create a folder `build/` and enter into it. Then, configure CMake via
```
cmake -DBUILD_GMOCK=TRUE -DCMAKE_INSTALL_PREFIX=$HOME/local ..
```
Finally, run
```
make
make install
```
to build the libraries and install them to the include folder of the `openpass` conda environment.

## 3) Building Open Simulation Interface v3 (OSI3) 
This section is based on the OSI documentation found here: [OSI Documentation](https://opensimulationinterface.github.io/osi-documentation/open-simulation-interface/doc/linux.html)

1. Download source code
`git clone https://github.com/OpenSimulationInterface/open-simulation-interface.git`
	
2. Download proto2cpp (clone repo https://github.com/OpenSimulationInterface/proto2cpp) and save proto2cpp.py in: `open-simulation-interface/proto2cpp/proto2cpp.py`
	
4. Change into `open-simulation-interface` and create a folder named `build`. Move into `build`
5. Run cmake to configure and create the necessary build files
`cmake -DCMAKE_PREFIX_PATH=$HOME/miniconda3/envs/openpass -DFILTER_PROTO2CPP_PY_PATH=../proto2cpp/ -DCMAKE_INSTALL_PREFIX=$HOME/local ..`

If an error occurs or if you suspect that something did not go right, you can simply start over by deleting all contents of `build`, (via calling `rm -r *` from **inside** `build`) and rerunning cmake. The same should be done if you wish to change any of the `-DVAR=var` arguments supplied to cmake.

6. From inside `build`, run **make** to compile the OSI library
`make`
Optionally, you can turn on multithreaded compilation by calling make with an extra argument:
`make -j <number-of-threads>`

7. Install the libraries to their destination folder (= value of CMAKE_INSTALL_PREFIX) with
`make install`


## 4) Compiling FMI
Clone repository from https://github.com/modelon-community/fmi-library
Documentation https://github.com/modelon-community/fmi-library/releases
```
cd fmi-library
(Latest) Release version 2.2.3
git checkout tags/2.2.3
```

Excerpt from FMIlib documentation:
```
To build from a terminal command line on Linux or Mac with default settings use:
$ mkdir build-fmil; cd build-fmil
$ cmake -DFMILIB_INSTALL_PREFIX=<prefix> <path to FMIL source>
$ make install test
```



## 5) Compiling simOpenPASS

### 5.1) Adapt template for CMake initial cache file
OpenPASS uses the CMake build system to set up/search the files and libraries that are needed for the compile process. While most of this configuration/generation process is carried out automatically (via the files in `<simopenpass-fork>/cmake` and the CMakeLists.txt files), a certain amount of user input is still required. For instance, CMake has to be told where to search for certain external libraries and files (unless they are placed in some default directory) or where the compiled files should be installed to. 

These, and more settings can be supplied to CMake by setting the corresponding `CMAKE_<>` variables in the call to cmake. Their values are stored in the *CMakeCache* (file `build/CMakeCache.txt`) which is used in later steps of the build process.

The - in our opinion - easiest way to manage the different variables that control the build setup, is to define these variables via a a list of cmake `set()` commands which are then collected in a *initial CMake cache file*. This information in that file is then passed onto `cmake` via the `-C` option (see section below).

The simopenpass-fork repo does provide templates of initial cache files for different installation environments (Ubuntu/WSL/Windows), which can be found in `<simopenpass-fork>/cmake-initCache-templates`. While the details of each template differ depending on the target platform, they all contain brackets `<what-to-insert>` that need to be substituted with the correct path/parameters. 

A common set of such placeholders is

| placeholder    |  description   |  example (WSL2)   |
| --- | --- | --- |
|  `<path-to-openpass-conda-env>`   |  Path to the root folder of the `openpass` conda environment   |  `/home/<user>/miniconda3/openpass`   |
| `<openpass-install-prefix>`    |  Where to install OpenPass to. Subfolders `bin`, `lib`, `share`, etc. will be created in this folder.    |  `/home/<user>/OpenPASS`   |
|  `<FMIlib-install-prefix>`   | Folder that contains the `lib` folder of FMIlib  | `/home/<user>/.local`  |
|  `<OSI-install-prefix>`   | Folder that contains OSI  | `/home/<user>/.local`  |

The template for WSL v2 is called `ubuntu-20.04-WSL2-initCache.cmake`.



### 5.2) CMake: Configure and generate build files

Change into `open-simulation-interface` and create a folder named `build`. Move into `build`. Then, run cmake to configure and create the necessary build files
`cmake -C ../<initial-cache-file>`

If an error occurs or if you suspect that something did not go right, you can simply start over by deleting all contents of `build`, (via calling `rm -r *` from **inside** `build`) and rerunning cmake. The same should be done if you wish to change any of the `-DVAR=var` arguments supplied to cmake.

### 5.3) Build and install openPASS
From inside `build`, run **make** to compile simopenpass
`make`
Optionally, you can turn on multithreaded compilation by calling make with an extra argument:
`make -j <number-of-threads>`

Install the libraries to `<openpass-install-dir>` with
`make install`






