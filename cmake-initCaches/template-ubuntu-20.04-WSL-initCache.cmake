set(CMAKE_PREFIX_PATH "<path-to-openpass-conda-env>;<FMIlib-install-prefix>;<OSI-install-prefix>" CACHE STRING "Path where cmake looks for packages and libraries") 
set(Qt5_ROOT "/usr/lib/x86_64-linux-gnu/cmake/Qt5" CACHE STRING "Path to cmake info for Qt5. Depends on Qt5 installation")
#set(Qt5_ROOT "/opt/Qt/5.15.2/gcc_64/lib/cmake/" CACHE STRING "Path to cmake info for Qt5")
set(CMAKE_INSTALL_PREFIX <openpass-install-prefix> CACHE STRING "Root installation directory used when calling 'make install' (Linux default: '/usr/local/'")
set(WITH_TESTS TRUE CACHE BOOL "Switches on compilation of UnitTests")
set(WITH_GUI FALSE CACHE BOOL "Enable OpenPASS GUI")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE CACHE BOOL "add automatically determinded parts of RPATH to install-RPATH")
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE CACHE BOOL "use the install RPATH for installing, but not for building")
set(CMAKE_EXE_LINKER_FLAGS "-Wl,--disable-new-dtags" CACHE STRING "") # write runpath of libraries into RPATH instead of RUNPATH 
set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--disable-new-dtags" CACHE STRING "") # write runpath of libraries into RPATH instead of RUNPATH 
set(WITH_EXTENDED_OSI TRUE CACHE BOOL "")
set(CMAKE_FIND_USE_CMAKE_SYSTEM_PATH FALSE CACHE BOOL "True: Exclude default system paths (f.i. /usr/local/) from CMake's search path")
set(CMAKE_BUILD_RPATH_USE_ORIGIN TRUE CACHE BOOL "Adds the ORIGIN location to the rpath")
set(CMAKE_SKIP_BUILD_RPATH FALSE CACHE BOOL "don't skip the full RPATH for the build tree")
# DEBUG Settings:
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -ggdb3 -fno-inline-functions" CACHE STRING "C++ compiler flags for Debug builds")
