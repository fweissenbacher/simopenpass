The patch file `fmilib_world_OSI.patch` is needed for Linux installations of OpenPASS and is supposed to be applied to the Modelon FMI-Library code, tag 2.2.3, downloadable from (github)[https://github.com/modelon-community/fmi-library/tree/2.2.3]. 

It deletes/fixes Linux incompatabile code in the implementation of fmuChecker, as explained in the (setup guide for World_OSI)[https://gitlab.tugraz.at/ffg-interact/wp4/simulation_env/simopenpass-fork/-/blob/vsi/sim/doc/OSI_World_Setup_Guide.pdf] 
